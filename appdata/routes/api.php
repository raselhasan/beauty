<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(["namespace" => "API"], function () {
	Route::post('login', 'UserController@login');
	Route::post('register', 'UserController@register');
	Route::group(['prefix' => 'services'], function () {
		Route::get('/{service}/types', 'ServiceTypeController@types');
		Route::get('/{service}/types/{type}/getworkers', 'ServiceTypeController@typeGetWorkers');
		Route::post('/{service}/types/{type}/booking', 'ServiceTypeController@storeBooking');
		Route::get('/{service}/types/{type}/getbooking', 'ServiceTypeController@getBooking');
		Route::post('/{service}/types/{type}/get-single-booking', 'ServiceTypeController@getSingleBooking');
		Route::get('/{service}/types/{type}/bookings/{booking}/cancle', 'ServiceTypeController@cancledBooking');
		Route::get('/{service}/types/{type}/bookings/{booking}/confirm', 'ServiceTypeController@confirmedBooking');
	});
	Route::group(['middleware' => 'auth:api'], function () {
		Route::group(['prefix' => 'users'], function () {
			Route::get('/{user}', 'UserController@details');
			Route::get('/{user}/galleries', 'UserGalleryController@galleries');
			Route::get('/{user}/galleries/{gallery}', 'UserGalleryController@galleriesById');

			Route::get('/{user}/workers', 'UserWorkerController@workers');
			Route::post('/{user}/workers', 'UserWorkerController@addWorker');
			Route::get('/{user}/workers/{worker}', 'UserWorkerController@workerById');
			Route::put('/{user}/workers/{worker}', 'UserWorkerController@workerUpdate');
			Route::delete('/{user}/workers/{worker}', 'UserWorkerController@workerDelete');

			Route::post('/{user}/workers/{worker}/daysget', 'UserWorkerController@workerDays');
			Route::post('/{user}/workers/all/daysall', 'UserWorkerController@workerDaysAll');
			Route::post('/{user}/workers/{worker}/weeks', 'UserWorkerController@workerDaysWeek');
			Route::post('/{user}/workers/{worker}/addday', 'UserWorkerController@addWorkerWorkingDay');
			Route::post('/{user}/workers/{worker}/days', 'UserWorkerController@workerDaysStore');
			Route::get('/{user}/workers/{worker}/days/{day}', 'UserWorkerController@workerDaysById');
			Route::delete('/{user}/workers/{worker}/days/{day}', 'UserWorkerController@workerDaysdelete');
			Route::put('/{user}/workers/{worker}/days/{day}', 'UserWorkerController@workerDaysUpdate');

			Route::get('/{user}/workers/{worker}/leaves', 'UserWorkerController@leaveDays');
			Route::post('/{user}/workers/{worker}/leaves', 'UserWorkerController@leaveDaysStroe');

			Route::get('/{user}/workers/{worker}/leaves/{leave}', 'UserWorkerController@leaveDaysById');
			Route::put('/{user}/workers/{worker}/leaves/{leave}', 'UserWorkerController@leaveDaysUpdate');
			Route::delete('/{user}/workers/{worker}/leaves/{leave}', 'UserWorkerController@leaveDaysDelete');
		});
		Route::group(['prefix' => 'services'], function () {
			Route::get('/{user}', 'ServiceController@index');
			Route::post('/', 'ServiceController@store');
			Route::get('/{service}', 'ServiceController@show');

			Route::get('/{service}/category', 'ServiceCategoryController@category');
			Route::get('/{service}/subcategory', 'ServiceCategoryController@subcategory');
			Route::get('/{service}/subcategory/{sub}', 'ServiceCategoryController@subcategoryById');

			Route::get('/{service}/schedules', 'ServiceScheduleController@schedules');
			Route::post('/{service}/schedules', 'ServiceScheduleController@schedulesStore');
			Route::put('/{service}/schedules/{schedule}', 'ServiceScheduleController@schedulesUpdate');
			Route::get('/{service}/schedules/{schedule}', 'ServiceScheduleController@schedulesById');
			Route::delete('/{service}/schedules/{schedule}', 'ServiceScheduleController@schedulesDelete');

			Route::post('/{service}/types', 'ServiceTypeController@typeStore');
			Route::get('/{service}/types/{type}', 'ServiceTypeController@type');
			Route::delete('/{service}/types/{type}', 'ServiceTypeController@typeDelete');
			Route::put('/{service}/types/{type}', 'ServiceTypeController@typeUpdate');

			Route::get('/{service}/types/{type}/workers', 'ServiceTypeController@typeWorkers');
			Route::post('/{service}/types/{type}/workers', 'ServiceTypeController@typeWorkersStore');
			Route::post('/{service}/types/{type}/workers_add', 'ServiceTypeController@typeWorkersAdd');
			Route::get('/{service}/types/{type}/workers/{worker}', 'ServiceTypeController@typeWorkersById');
			Route::get('/{service}/types/{type}/workers_detach/{worker}', 'ServiceTypeController@typeWorkersDetach');
			Route::put('/{service}/types/{type}/workers/{worker}', 'ServiceTypeController@typeWorkersUpdate');
			Route::delete('/{service}/types/{type}/workers/{worker}', 'ServiceTypeController@typeWorkersDelete');

			Route::get('/{service}/tabs', 'ServiceGalleryController@serviceTabs');
			Route::post('/{service}/tabs', 'ServiceGalleryController@serviceTabStore');
			Route::get('/{service}/tabs/{tab}', 'ServiceGalleryController@serviceTabById');
			Route::get('/{service}/tabs/{tab}/galleries', 'ServiceGalleryController@serviceTabGallery');
			Route::get('/{service}/tabs/{tab}/galleries/{gallery}', 'ServiceGalleryController@serviceTabGalleryById');

			Route::post('/{service}/custom-package', 'CustomPackageController@create');
			Route::get('/{service}/custom-package', 'CustomPackageController@index');
			Route::delete('/{service}/custom-package/{custom}', 'CustomPackageController@delete');
			Route::put('/{service}/custom-package/{custom}', 'CustomPackageController@update');
			Route::get('/{service}/custom-package/{custom}', 'CustomPackageController@show');
			Route::delete('/{service}/custom-package/{custom}/delete/{type?}', 'CustomPackageController@typedelete');
		});
	});
});
