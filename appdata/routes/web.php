<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');
Route::get('localization/{locale}', 'LocalizationController@index');


Route::get('/about-model/{id}', 'IndexController@aboutM')->name('aboutM');

Route::get('/user-profile', 'IndexController@userProfile')->name('userProfile');

//Rasel Route
Route::get('/select-plan/{id}', 'IndexController@selectPlan')->name('selectPlan');
Route::post('/select-plan/amount-calculation', 'PaymentController@amountCalculation');
Route::post('/select-plan/amount-with-star', 'PaymentController@starAmount');
Route::post('/select-plan/payment', 'PaymentController@payment');
Route::get('/stripe/success', 'PaymentController@paymentSuccess');
Route::get('/stripe/cancel', 'PaymentController@stripeCancel');
Route::get('/paysera/success', 'PaymentController@paymentSuccess');
Route::get('/paysera/cancel', 'PaymentController@payseraCancel');
Route::get('/paysera/callback', 'PaymentController@payseraCallback');
Route::get('payment-list', 'PaymentListController@index');
Route::get('payment-history', 'PaymentListController@paymentHistory');
Route::get('payment-download', 'PaymentListController@paymentDownload');

Route::get('add-control-payment', 'PaymentController@addControlPayment');
Route::post('add-control-payment/get-price', 'PaymentController@addControlPaymentPrice');

Route::post('add-control-payment/make', 'PaymentController@addControlMakePayment');
Route::get('add-control-payment/success', 'PaymentController@addControllPaymentSuccess');
Route::get('add-control-payment/cancel', 'PaymentController@addControllPaymentCancel');

Route::get('/services', 'SearchServiceController@index')->name('blog');
Route::post('/services/subcategory', 'SearchServiceController@getSubCategory');
Route::get('/services/index-data', 'SearchServiceController@indexData');
Route::get('/services/pagination-data', 'SearchServiceController@paginationData');
Route::post('/vip-profile-amount', 'PaymentController@getProfileAmount')->name('getProfileAmount');
Route::post('/vip-profile-payment', 'PaymentController@profilePayment')->name('profilePayment');

Route::get('profile-payment/success', 'PaymentController@profilePaymentSuccess');
Route::get('profile-payment/cancel', 'PaymentController@profilePaymentCancel');
Route::get('index-vipdata', 'SearchServiceController@indexVipData');
Route::get('public-profile/{id}', 'IndexController@publicProfile');
Route::get('public-profile/{id}/service/{title}/{s_id}', 'IndexController@publicService')->name('publicService');
Route::get('get-user-city', 'SearchServiceController@getUserCity');
Route::get('profile-list', 'SearchServiceController@profileList');
Route::post('get-profile-and-service', 'SearchServiceController@profileAndService');
Route::post('active-service', 'PaymentController@activeService');



//end rasel route


Route::get('test', 'SearchServiceController@test');

Route::namespace('Auth')->group(function () {
	Route::post('registration', 'RegisterController@registration')->name('registration');
});
Route::group(['prefix' => 'user'], function () {

	Route::name('user.')->group(function () {

		Route::post('get-service-gallery-tab', 'IndexController@getServiceGlryTab')->name('getServiceGlryTab');
		Route::namespace('Auth')->group(function () {

			// Route::get('login', 'LoginController@showLoginForm')->name('login');	
			Route::post('login', 'LoginController@login')->name('login.submit');
			Route::post('logout', 'LoginController@logout')->name('logout');
		});
		Route::namespace('User')->group(function () {
			// profile
			Route::post('add-live-zone', 'ProfileController@addLiveZone');
			Route::post('edit-live-zone', 'ProfileController@editLiveZone');
			Route::post('update-live-zone', 'ProfileController@liveZoneUpdate');
			Route::post('delete-live-zone', 'ProfileController@liveZoneDelete');
			Route::post('add-comment', 'CommentController@addComment');
			Route::post('main-comment-replay', 'CommentController@replayMainComment');

			Route::get('profile', 'ProfileController@index')->name('profile');
			Route::get('booking-list', 'ProfileController@bookingList')->name('bookingList');
			Route::get('services/{service}/types/{type}/bookings/{booking}/cancle', 'ProfileController@cancledBooking');
			Route::get('calendar/create', 'CalendarController@index')->name('calendar');
			Route::post('calendar/getsession', 'CalendarController@getSession')->name('getSession');
			Route::post('gallery-file-upload', 'ProfileController@galleryFileUp')->name('galleryFileUp');
			Route::post('gallery-file-remove', 'ProfileController@galleryRemover')->name('galleryRemover');
			Route::post('update-user-picture', 'ProfileController@upPrPic');
			Route::post('process-single-tmp-image', 'ProfileController@setProfilePic');
			Route::post('single-tmp-img-remove', 'ProfileController@removeProfilePic');

			Route::post('cover-image-upload', 'ProfileController@coverImgUpload');
			Route::post('update-cover-img', 'ProfileController@coverImgUpdate');
			Route::post('user-cover-remove', 'ProfileController@userCoverRemover')->name('userCoverRemover');

			Route::post('get-profile-information', 'ProfileController@getProfileInfo');
			Route::post('insert-profile-information', 'ProfileController@insertProfileInfo');
			Route::post('service-address', 'ServicesController@saveServiceAddress')->name('saveServiceAddress');
			Route::get('my-service/{title}/{id}', 'ServicesController@index')->name('myService');
			Route::get('add-my-service', 'ServicesController@add')->name('addMyService');
			Route::post('change-service-details', 'ServicesController@changeServiceDetails')->name('changeServiceDetails');
			Route::post('service-tmp-img', 'ServicesController@serviceTmpImg')->name('serviceTmpImg');
			Route::post('update-service-image', 'ServicesController@updateServiceImg')->name('updateServiceImg');
			Route::post('service-tmp-img-remove', 'ServicesController@serviceTmpImgRemove')->name('serviceTmpImgRemove');
			Route::post('change-service-sub-category', 'ServicesController@changeServiceSubCategory')->name('changeServiceSubCategory');
			Route::post('change-service-inner-category', 'ServicesController@changeServiceInnerCategory')->name('changeServiceInnerCategory');
			Route::post('save-service-category', 'ServicesController@saveServiceCategory')->name('saveServiceCategory');
			Route::post('save-service-gallery-tab', 'ServicesController@saveServiceGlryTab')->name('saveServiceGlryTab');
			Route::post('add-service-gallery-image', 'ServicesController@addServiceGalleryImg')->name('addServiceGalleryImg');
			Route::post('service-gallery-image-upload', 'ServicesController@serviceGalleryImgUpload')->name('serviceGalleryImgUpload');
			Route::post('service-gallery-tmp-image-remove', 'ServicesController@serviceGalleryTmpImgRemove')->name('serviceGalleryTmpImgRemove');

			// calendar
			// Route::get('calendar-list', 'CalendarController@calnedarList')->name('calnedarList');
		});
	});
});
Route::get('/user/calendar/{any}', 'User\CalendarController@index')->where('any', '.*');
