<div class="comment" id="main-cmt-{{$comment['id']}}">
	<div class="row">
		<div class="user-img-c">
			<img src="{{asset('userImage/fixPic/'.$comment['user']['profile_image'])}}" alt="user-img">
		</div>
		<div class="col-12 pad-0">
			<div class="comment-cnt">
				<h2>{{ $comment['user']['surname']}}</h2>
				<h6>
				{{date('d-m-Y',strtotime($comment['created_at']))}} at {{date('H:m a',strtotime($comment['created_at']))}}
				</h6>
				<p>{{ $comment['comment']}}</p>
				<span class="edit-com" onclick="replayModelShow('main-cmt-{{$comment["id"]}}')">@lang('lang.replay')Replay</span>
			</div>
			<div class="cmt-replay">
				
			</div>
		</div>
	</div>
</div>