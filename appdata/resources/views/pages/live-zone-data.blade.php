           <div class="single-content" id="del-live-zone-{{$liveZone->id}}">
              <div class="live-tlt">
                <h1 id="lz-title-{{$liveZone->id}}">{{$liveZone->title}}</h1>
               <div class="edit-info-btn">
                <i title="edit image" class="fa fa-edit" onclick="openLiveZone({{$liveZone->id}})"></i>
                <i title="edit image" class="fa fa-trash" onclick="deleteLiveZone({{$liveZone->id}})"></i>
              </div>
              </div>
              <div class="live-des">
                <p id="lz-des-{{$liveZone->id}}">{{$liveZone->description}}</p>
              </div>
              
              <div class="content-img-p lz-img-{{$liveZone->id}} @if($liveZone->status == 1) imgVideoBlock @else imgVideo @endif">
                <div class="content-img">
                  <img id="lz-img-{{$liveZone->id}}" src="{{asset('liveZone/'.$liveZone->image)}}">
                </div>
              </div>
              
              <div class="content-img-p video lz-video-{{$liveZone->id}}  @if($liveZone->status == 2) imgVideoBlock @else imgVideo @endif">
                  <div class="content-img">
                      <iframe id="lz-video-{{$liveZone->id}}" src="{{$liveZone->image}}">
                      </iframe>
                  </div>
              </div>
             
            </div>