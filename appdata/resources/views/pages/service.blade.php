@extends('layout.app')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fancybox.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/cropper.css') }}">
<style type="text/css">
	.gm-style-iw {
		max-width: 300px !important;
		max-height: 300px !important;
	}

	.gm-style-iw-d {
		max-width: 300px !important;
		max-height: 300px !important;
	}

	.add-tab {
		display: none !important;
	}

	.gallery .item-column:nth-child(1) .single-content {
		padding-bottom: 0 !important;
	}
</style>
@endsection
@section('content')
@include('inc.header-filter')
<section class="about-section">
	<div class="link-menu">
		<span class="arrow-l"><i class="fas fa-arrow-left"></i></span>
		<span><a href="#">Cetagory</a></span>
		<span><i class="fas fa-chevron-right"></i></span>
		<span>Sub Cetagory</span>
	</div>
	<div class="row mar-0">
		<div class="col-12 col-lg-8">
			<div class="about-cnt mar-b-30">
				<div class="about-tlt">
					<input type="hidden" name="service_id_h" value="{{ $service['id'] }}">
					<h1 class="service-title">{{ $service['title'] }}</h1>
					<!-- <div class="edit-info-btn btn-ctm sty-black" onclick="serviceTD(event)">
						<i class="fa fa-edit" title="edit image"></i>
					</div> -->
				</div>
				<div class="about-txt service-description">
					{!! $service['description'] !!}
				</div>
			</div>
			<div class="category-area">
				<div class="cat-slt">
					<div class="row">
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="name">Category:</label>
								<span class="category">
									@foreach($categories as $cat)
									{{ ($cat->id == $service['category_id']) ? $cat->name : '' }}
									@endforeach
								</span>
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="form-group">
								<label for="name">Sub Category: </label>
								<span class="category">
									@php
									$sub_catA = [];
									@endphp
									@foreach($sub_categories as $sub_cat)
									@if(in_array($sub_cat->id, array_column($service['takeSubCategory'], 'id')))
									@php
									$sub_catA[] = $sub_cat->name;
									@endphp
									@endif
									@endforeach
									{{ implode(', ', $sub_catA) }}
								</span>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="about-nav">
				<div class="gallery" style="display:none;">
					<!-- <div class="single-content gallery-item wh-100"  onclick="changeServiceGalleryImage(event)">
						<div class="plus-c"><i class="fas fa-plus"></i></div>
					</div> -->
					@foreach($service['service_gallery_tabs'] as $val)
					@foreach($val['service_galleries'] as $val1)
					<div class="single-content gallery-item" data-gallery-tag="{{ $val['tab'] }}">
						<a href="{{ asset('assets/img/services/galleries/' . $val1['img']) }}" data-fancybox="gallery">
							<div class="content-img-p">
								<div class="content-img">
									<img src="{{ asset('assets/img/services/galleries/' . $val1['img']) }}" alt="img" />
								</div>
							</div>
						</a>
					</div>
					@endforeach
					@endforeach
				</div>
				<div class="more"><button class="btn more-btn">More show</button></div>
			</div>
			<div class="apointment-area">
				<view-calendar></view-calendar>
			</div>
			<div class="comment-s">
				<div class="com-tlt">
					<h1><span>5</span> Comments</h1>
				</div>
				<div class="com-text-ar">
					<div class="form-group">
						<textarea class="form-control"></textarea>
					</div>
					<div class="form-group">
						<button class="p-btn btn send">Send</button>
						<button class="p-btn btn">Clear</button>
					</div>
				</div>
				<div class="com-reply">
					<div class="comment">
						<div class="row">
							<div class="user-img-c">
								<img src="{{asset('assets/img/man.jpg')}}" alt="user-img">
							</div>
							<div class="col-12 pad-0">
								<div class="comment-cnt">
									<h2>Jhon</h2>
									<h6>03-11-2019 at 10:00am</h6>
									<p>Lorem Ipsum is simply dummy text of the printing</p>
									<span class="edit-com">Edit</span>
								</div>

								<div class="reply">
									<div class="row">
										<div class="user-img-c">
											<img src="{{asset('assets/img/man.jpg')}}" alt="user-img">
										</div>
										<div class="col-12 pad-0">
											<div class="comment-cnt">
												<h2>Jhon</h2>
												<h6>03-11-2019 at 10:00am</h6>
												<p>Lorem Ipsum is simply dummy text of the printing</p>
												<span class="edit-com">Edit</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="comment">
						<div class="row">
							<div class="user-img-c">
								<img src="{{asset('assets/img/man.jpg')}}" alt="user-img">
							</div>
							<div class="col-12 pad-0">
								<div class="comment-cnt">
									<h2>Jhon</h2>
									<h6>03-11-2019 at 10:00am</h6>
									<p>Lorem Ipsum is simply dummy text of the printing</p>
									<span class="edit-com">Edit</span>
								</div>

								<div class="reply">
									<div class="row">
										<div class="user-img-c">
											<img src="{{asset('assets/img/man.jpg')}}" alt="user-img">
										</div>
										<div class="col-12 pad-0">
											<div class="comment-cnt">
												<h2>Jhon</h2>
												<h6>03-11-2019 at 10:00am</h6>
												<p>Lorem Ipsum is simply dummy text of the printing</p>
												<span class="edit-com">Edit</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-lg-4">
			<div class="model-bg-img" onclick="changeServiceImage(event)">
				@if($service['image'])
				<img src="{{asset('assets/img/services/'. $service['image'])}}" class="wth-100-p" alt="service-img">
				@else
				<img src="{{asset('assets/img/services/no_service.png')}}" class="wth-100-p" alt="service-img">
				@endif
			</div>
			<div class="social-icon">
				<ul>
					<li class="{{ ($service['user']['facebook']) ? '' : 'd-none' }}"><a class="fb" href="{{ $service['user']['facebook'] }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
					<li class="{{ ($service['user']['twitter']) ? '' : 'd-none' }}"><a class="tw" href="{{ $service['user']['twitter'] }}" target="_blank"><i class="fab fa-twitter"></i></a></li>
					<li class="{{ ($service['user']['gmail']) ? '' : 'd-none' }}"><a class="go" href="{{ $service['user']['gmail'] }}" target="_blank"><i class="fab fa-goodreads-g"></i></a></li>
					<li class="{{ ($service['user']['linkedin']) ? '' : 'd-none' }}"><a class="lin" href="{{ $service['user']['linkedin'] }}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
					<li class="{{ ($service['user']['pinterest']) ? '' : 'd-none' }}"><a class="pin" href="{{ $service['user']['pinterest'] }}" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
				</ul>
			</div>
			<div class="pro-box">
				<div class="row">
					<div class="col-12">
						<div class="pro-cnt">
							<div class="pro-name">{{ $service['user']['name'] }} {{ $service['user']['surname'] }} {{ ($service['user']['nickname']) ? '(' . $service['user']['nickname'] . ')' : '' }}</div>
							<div class="pro-email"><i class="fa fa-envelope"></i> {{ $service['user']['email'] }}</div>
							<div class="pro-phn"><i class="fas fa-mobile-alt"></i>{{ $service['user']['phone'] }}</div>
							<div class="pro-cn"><i class="fas fa-map-marker-alt"></i><span class="set-address">{{ $service['address']}}</span></div>
							<div class="pro-btn">
								<a href="{{ url('public-profile', [$id]) }}"><button class="p-btn btn">Profle</button></a>
							</div>
						</div>
					</div>
					<div class="pro-img">
						@if($service['image'])
						<img src="{{asset('userImage/fixPic/'. $service['image'])}}" alt="user-img">
						@else
						<img src="{{asset('assets/img/profile_pic.png')}}" alt="user-img">
						@endif
					</div>
				</div>
			</div>
			<div class="pro-table">
				<table class="table table-striped">
					<tbody>
						<tr class="head-tr">
							<td>Type</td>
							<td>Price</td>
						</tr>
						@if(@count($service['types'])>0)
						@foreach ($service['types'] as $type)
						<tr>
							<td>{{ $type['name']}}</td>
							<td>{{ $type['price']}}</td>
						</tr>
						@endforeach
						@endif

					</tbody>
				</table>
			</div>
			<div class="g-map">
				<div class="mapouter">
					<div class="gmap_canvas" id="map">

					</div>
				</div>
			</div>

			<div class="side-blog">
				<section class="blog-section">
					<div class="pop-master">
						<h1>Populer Masters</h1>
					</div>
					<div class="blog">
						<div class="blog-img">
							<img src="{{asset('assets/img/man.jpg')}}">
						</div>
						<div class="blog-des">
							<div class="blog-txt">
								Lorem Ipsum is simply dummy text of the printing to make
							</div>
							<div class="blog-dta star">
								<span class="date"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></span>
							</div>
						</div>
					</div>
					<div class="blog">
						<div class="blog-img">
							<img src="{{asset('assets/img/man.jpg')}}">
						</div>
						<div class="blog-des">
							<div class="blog-txt">
								Lorem Ipsum is simply dummy text of the printing to make
							</div>
							<div class="blog-dta star">
								<span class="date"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></span>
							</div>
						</div>
					</div>
				</section>
			</div>

		</div>
	</div>
</section>
<section class="slider-section bg-dark-sec">
	<div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="{{asset('assets/img/slider1.jpg')}}">
				<div class="carousel-caption">
					<h1>Third slide label</h1>
					<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
				</div>
			</div>
			<div class="carousel-item">
				<img src="{{asset('assets/img/slider2.jpg')}}">
				<div class="carousel-caption">
					<h1>Third slide label</h1>
					<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
				</div>
			</div>
			<div class="carousel-item">
				<img src="{{asset('assets/img/slider3.jpg')}}">
				<div class="carousel-caption">
					<h1>Third slide label</h1>
					<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
				</div>
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</section>
<div class="modal fade pro-i-m" id="serviceTD" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body edit-box-area">
				<div class="edit-box">
					<span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
					<h3 class="text-center">Update your profile info</h3>
					<form autocomplete="off" method="post" action="" id="change_service_details" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="id" value="{{ $service['id'] }}">
						<div class="col-12 pad-0">
							<div class="form-group">
								<label for="serviceTitle">Title <span class="c-red">*</span></label>
								<input type="text" id="serviceTitle" class="form-control" placeholder="Service Title" value="{{ $service['title'] }}" aria-label="service_title" name="title">
							</div>
						</div>
						<div class="col-12 pad-0">
							<div class="form-group">
								<label for="serviceDescription">Description</label>
								<textarea id="serviceDescription" class="form-control" placeholder="Service Description" name="description">{!! $service['description'] !!}</textarea>
							</div>
						</div>
						<button class="btn btn-success float-right">Save</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade pro-i-m" id="serviceGlryTab" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body edit-box-area">
				<div class="edit-box">
					<span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
					<h3 class="text-center">Add Gallery Tab</h3>
					<form autocomplete="off" method="post" action="" id="change_service_glry_tab" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="service_id" value="{{ $service['id'] }}">
						<div class="col-12 pad-0">
							<div class="form-group">
								<label for="glryTabName">Tab Name <span class="c-red">*</span></label>
								<input type="text" id="glryTabName" class="form-control" placeholder="Tab Name" name="tab">
							</div>
						</div>
						<button class="btn btn-success float-right">Add</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="serviceImg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body edit-box-area">
				<div class="edit-box text-center">
					<span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
					<h3 class="">Update service image</h3>
					<div class="col-md-12" style="padding-bottom:13px">
						<div id="service_files">
							<?php
							if (Session::has('service_img')) {
								$image = Session::get('service_img');
								$html = '';
								$html .= '<span class="pip">';
								$html .= '<img class="imageThumb" src="' . asset('assets/img/services/tmpImg/' . $service['user']['id'] . '/' . $image) . '">';
								$html .= '<br/>';
								$html .= '<span class="service-remove" id="' . $image . '">✖</span>';
								$html .= '</span>';
								echo $html;
							}
							?>
						</div>
					</div>

					<label class="form-label text-dark"><b>Service Image</b></label>
					<div class="controls m-b-10">
						<label class="btn btn-primary btn-file">
							<i class="fa fa-upload"></i> Upload Service Image
							<input type="file" style="display: none;" name="server_img_u" id="server_img_u">
						</label>
					</div>


					<form method="post" action="{{url('user/update-service-image')}}" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="id" value="{{ $service['id'] }}">
						<button class="btn btn-success">Update</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="serviceGalleryImg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body edit-box-area">
				<div class="edit-box text-center">
					<span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
					<h3 class="">Add Gallery image</h3>
					<div class="col-md-12" style="padding-bottom:13px">
						<div class="input-group">
							<div class="ctm-select" ctm-slt-n="service_gallery_tab">
								<div class="ctm-select-txt pad-l-10">
									<span class="select-txt noselect" id="service_gallery_tab_a">chose...</span>
									<span class="select-arr"><i class="fas fa-caret-down"></i></span>
								</div>
								<div class="ctm-option-box noselect">
									<div class="ctm-option noselect">chose...</div>
									@foreach($service_glry_tab as $tab)
									<div class="ctm-option noselect" ctm-otn-v="{{ $tab->id }}">{{ $tab->tab }}</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12" style="padding-bottom:13px">
						<div id="service_gallery">

						</div>
					</div>

					<label class="form-label text-dark"><b>Gallery Image</b></label>
					<div class="controls m-b-10">
						<label class="btn btn-primary btn-file">
							<i class="fa fa-upload"></i> Add Gallery Image
							<input type="file" style="display: none;" name="gallery_img_u" id="gallery_img_u">
						</label>
					</div>


					<form method="post" action="" id="upload_service_gallery_info" enctype="multipart/form-data">
						@csrf
						<button class="btn btn-success">Add</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade cr-modal" id="serverUploadImgM" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Crop the image</h4>
			</div>
			<div class="modal-body" style="width: 100%; overflow: hidden;">
				<div class="img-container">
					<img id="server_image" src="https://avatars0.githubusercontent.com/u/3456749" style="width: 100%">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn" id="crop_service_img">Crop</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade cr-modal" id="serviceGalleryImgU" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Crop the image</h4>
			</div>
			<div class="modal-body" style="width: 100%; overflow: hidden;">
				<div class="img-container">
					<img id="service_gallery_image" src="https://avatars0.githubusercontent.com/u/3456749" style="width: 100%">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn" id="crop_service_gallery_img">Crop</button>
			</div>
		</div>
	</div>
</div>

@endsection
@section('script')
<script src="{{ asset('assets/js/jquery.fancybox.js')}}"></script>
<script src="{{ asset('assets/js/cropper.js')}}"></script>
<script src="{{ asset('assets/js/maugallery.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script>
<script>
	// $(function() {
	// 	$('.calendar').pignoseCalendar({
	// 		theme: 'dark'
	// 	});
	// });

	$(function() {
		$('.gallery').mauGallery({
			columns: {
				xs: 1,
				sm: 2,
				md: 3,
				lg: 4,
				xl: 6
			},
			lightBox: true,
			lightboxId: 'myAwesomeLightbox',
			showTags: true,
			tagsPosition: 'top'
		});
	});
	window.get_service_glry_tab = "{{ route('user.getServiceGlryTab') }}";
</script>
<script>
	function goActive() {
		// add filtering code here
		var url = '{{url("select-plan/".$service["id"])}}';
		window.location.href = url;
	}
	$(function() {
		var autocomplete;
		autocomplete = new google.maps.places.Autocomplete((document.getElementById('autoAddress')), {
			types: ['geocode'],
		});
		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var near_place = autocomplete.getPlace();
			var data = new FormData();
			data.append('address', $('#autoAddress').val());
			data.append('lat', near_place.geometry.location.lat());
			data.append('lng', near_place.geometry.location.lng());
			data.append('service_id', "{{$service['id']}}");
			var city = '';
			for (var i = 0; i < near_place.address_components.length; i++) {
				for (var j = 0; j < near_place.address_components[i].types.length; j++) {
					if (near_place.address_components[i].types[j] == 'administrative_area_level_2') {
						city = near_place.address_components[i].long_name;
					}
				}
			}
			data.append('city', city);
			$.ajax({
				processData: false,
				contentType: false,
				data: data,
				type: 'POST',
				url: '{{url("user/service-address")}}',
				success: function(response) {
					serviceMap(near_place.geometry.location.lat(), near_place.geometry.location.lng());
					$('.set-address').text($('#autoAddress').val());
					//console.log(response);
				}

			});
		});

		var lat = '{{$service["lat"]}}';
		var lng = '{{$service["lng"]}}';
		serviceMap(lat, lng);

	});

	function serviceMap(lat, lng) {
		lat = parseFloat(lat);
		lng = parseFloat(lng);
		var map = new google.maps.Map(document.getElementById('map'), {
			center: {
				lat: lat,
				lng: lng
			},
			zoom: 8
		});
		var marker = new google.maps.Marker({
			position: {
				lat: lat,
				lng: lng
			},
			map: map,
			title: 'My Location'
		});

		var mainContent = '';
		mainContent += '<div style="color:#1014a0;font-size:17px; text-align:center;margin-bottom:5px"><?= $service['title'] ?></div>';
		mainContent += '<div style="color:#1014a0;font-size:17px; text-align:center;margin-bottom:5px"><?= $service['user']['name'] ?> <?= $service['user']['surname'] ?> <?= ($service['user']['nickname']) ? '(' . $service['user']['nickname'] . ')' : '' ?></div>';
		mainContent += '<div style="color:red; text-align:left;margin-bottom:5px"><i class="fa fa-envelope" style="margin-right:6px;"></i> <?= $service['user']['email'] ?></div>';
		mainContent += '<div style="color:red; text-align:left;margin-bottom:5px"><i class="fas fa-mobile-alt" style="margin-right:6px;"></i><?= $service['user']['phone'] ?></div>';
		mainContent += '<div style="color:red; text-align:left;margin-bottom:5px"><i class="fas fa-map-marker-alt" style="margin-right:6px;"></i>' + $('#autoAddress').val() + '</div>';

		var infowindow = new google.maps.InfoWindow();

		marker.addListener('click', function() {
			infowindow.setContent(mainContent);
			infowindow.open(map, marker);
		});
	}
</script>
@endsection
@section('wondowv')
<script>
	window.base_url = "<?= url('/') ?>";
	window.c_user_id = <?= (@Auth::user()->id) ? @Auth::user()->id : 0 ?>;
	window.c_user = <?= (@Auth::user()) ? @Auth::user() : 0 ?>;
	window.service = <?= json_encode($service) ?>;
	window.window.s_id = <?= $s_id ?>;
</script>
@endsection