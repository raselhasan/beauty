<div class="col-12 col-md-9">
@if(count($services)>0)
	@foreach ($services as $service)
		<div class="blog">
			<div class="blog-img">
				<img src="{{asset('assets/img/services/'.$service->image)}}">
			</div>
			<div class="blog-des">
				<div class="blog-tlt"><a href="{{url('about-model/'.$service->id)}}">{{$service->title}}</a></div>
				<div class="blog-txt">
					{!! substr($service->description, 0, 500); !!}
					<br/>
					<a href="{{url('about-model/'.$service->id)}}" class="seeMore">@lang('lang.see_more')</a>
				</div>
				<div class="blog-dta">
					<span class="date"><i class="fa fa-calendar"></i> 
					{{ date('M d Y',strtotime($service->created_at))}}
					</span>
					<span class="auth"><i class="fa fa-user"></i>
					{{$service->name}} {{$service->surname}}
					</span>
				</div>
			</div>
		</div>
	@endforeach
@endif
<div>
	{{$services->render()}}
</div>
</div>
<div class="col-12 col-md-3">
 	<div class="blog-sidbar">
 		@if(count($allVip)>0)
			@foreach ($allVip as $vip)
				@if(property_exists($vip,'title'))
					<div class="single-content">
                        <div class="content-img-p">
                            <div class="content-img">
                                <img src="{{asset('assets/img/services/'.$vip->image)}}">
                            </div>
                        </div>
                        <div class="content-des">
                            {{ substr($vip->description,0,65)}} 
                        </div>
                        <div class="content-tlt">
                            <a href="{{url('about-model/'.$vip->main_id)}}">{{ $vip->title }}</a>
                        </div>
                    </div>
				@else 
					<div class="single-content single-content1">
                        <div class="content-img-p">
                            <div class="content-img">
                                <img src="{{asset('assets/img/services/'.$vip->profile_image)}}">
                            </div>
                        </div>
                        <div class="content-s-tlt">
                             <a href="{{url('public-profile/'.$vip->id)}}">{{ $vip->surname }}</a>
                        </div>
                        <div class="content-s-txt">
                            {{ substr($vip->bio,0,65)}} 
                        </div>
                    </div>
				@endif
			@endforeach
 		@endif
 	</div>
</div> 	