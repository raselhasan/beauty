@extends('layout.app')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fancybox.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/cropper.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app.css') }}"> --}}
<style type="text/css">
  .add-live-zone {
    margin-bottom: 10px;
    cursor: pointer;
  }

  .add_a_img {
    color: #2a9e63;
    text-decoration: underline;
    margin-right: 10px;
  }

  .embade_video {
    color: #E25B37;
    text-decoration: underline;
    margin-right: 10px;
  }

  #link-area,
  #img-area {
    display: none;
  }

  .imgVideo {
    display: none;
  }

  .imgVideoBlock {
    display: block;
  }
  .gm-style-iw {
    max-width: 300px !important;
    max-height: 300px !important;
  }
  .pac-container {
      z-index: 10000 !important;
  }
  .gm-style-iw-d {
    max-width: 300px !important;
    max-height: 300px !important;
  }
  .date a{
    color:#E25B37;
  }
  .url-color{
    color:#E25B37;
  }
  .fa-trash{
    margin-left: 10px;
  }
</style>
@endsection
@section('content')
@include('inc.header-filter')
<section class="about-section userPro-section">
  <div class="row mar-0">
    <div class="col-12 col-lg-7">
      <div class="userPro-cnt">
        <div class="user-slider" onclick="changeCoverImage(event)">
          <div class="swiper-container ctm-container-4">
            <div class="swiper-wrapper">
              @if($UserCover->count())
              @foreach ($UserCover as $uc)
              <div class="swiper-slide">
                <div class="single-content wh-100">
                  <div class="content-img-p">
                    <div class="content-img">
                      <img src="{{ asset('userImage/coverPic/' . $uc->img) }}" alt="cover-pic">
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
              @else
              <div class="swiper-slide">
                <div class="single-content wh-100">
                  <div class="content-img-p">
                    <div class="content-img">
                      <img src="{{asset('assets/img/default_cover.png')}}" alt="user-img">
                    </div>
                  </div>
                </div>
              </div>
              @endif
            </div>
          </div>
        </div>
        <div class="userPro-bio">
          <div class="row mar-0">
            <div class="userPro-imgM">
              @if(Auth()->user()->profile_image)
              <img src="{{asset('userImage/fixPic/'. Auth()->user()->profile_image)}}" alt="user-img">
              @else
              <img src="{{asset('assets/img/profile_pic.png')}}" alt="user-img">
              @endif
              <div class="overShade" onclick="showPicEditBox()">
                <i class="fa fa-edit" title="edit image"></i>
              </div>
            </div>
            <div class="col-12">
              <div class="userPro-txt">
                <div class="edit-info-btn" onclick="proInfoC(event)">
                  <i class="fa fa-edit" title="edit image"></i>
                </div>
                <div class="userPro-tlt">
                  <h3 class="setName">{{ $makeName }}</h3>
                </div>
                <div class="userPro-tel">
                  <div class="userPro-tel-t">
                    <div class="pro-email"><i class="fa fa-envelope"></i>{{ Auth::user()->email }}</div>
                    <div class="pro-phn"><i class="fas fa-mobile-alt"></i>{{ $userInfo['phone'] }}</div>
                    <div class="pro-cn"><i class="fas fa-map-marker-alt"></i>{{ $userInfo['address'] }}</div>
                  </div>
                </div>
                <div class="social">
                  <span>@lang('lang.share')</span>
                  <span class="{{ (Auth::user()->facebook) ? '' : 'd-none' }}"><a class="fb" href="{{ (Auth::user()->facebook) ? Auth::user()->facebook : 'javascript:void(0)' }}" target="_blank"><i class="fab fa-facebook-f"></i></a></span>
                  <span class="{{ (Auth::user()->twitter) ? '' : 'd-none' }}"><a class="tw" href="{{ (Auth::user()->twitter) ? Auth::user()->twitter : 'javascript:void(0)' }}" target="_blank"><i class="fab fa-twitter"></i></a></span>
                  <span class="{{ (Auth::user()->gmail) ? '' : 'd-none' }}"><a class="go" href="{{ (Auth::user()->gmail) ? Auth::user()->gmail : 'javascript:void(0)' }}" target="_blank"><i class="fab fa-goodreads-g"></i></a></span>
                  <span class="{{ (Auth::user()->linkedin) ? '' : 'd-none' }}"><a class="lin" href="{{ (Auth::user()->linkedin) ? Auth::user()->linkedin : 'javascript:void(0)' }}" target="_blank"><i class="fab fa-linkedin-in"></i></a></span>
                  <span class="{{ (Auth::user()->pinterest) ? '' : 'd-none' }}"><a class="pin" href="{{ (Auth::user()->pinterest) ? Auth::user()->pinterest : 'javascript:void(0)' }}" target="_blank"><i class="fab fa-pinterest-p"></i></a></span>
                  @if(@count($userInfo['profile_payment']) < 1) <a href="javascript:;" data-toggle="modal" data-target="#profile-payment-modal"><span>Make vip </span></a>
                    @else 
                    <a href="javascript:;" data-toggle="modal" data-target="#profile-payment-modal"><span>Upgrade VIP </span></a>
                    @endif
                </div>
              </div>
              {{-- <div class="chat-icon"><i class="fas fa-comments"></i></div> --}}
            </div>
          </div>
          <div class="bio-txt">
            <h4 class="bio-txt-tlt">@lang('lang.bio')</h4>
            <p class="bio-txtP">
              {!! Auth::user()->bio !!}
            </p>
          </div>
        </div>
      </div>
      <div class="live-zona">
        <div class="section-title">Live Zona</div>
        <div class="add-live-zone text-right">
          <span onclick="openLiveZone1()"><i class="fa fa-plus"></i> @lang('lang.add_live_zone')</span>

        </div>
        <div class="live-zona-cnt mylz-content">
          @if(count($liveZones)>0)
          @foreach ($liveZones as $liveZone)
          <div class="single-content" id="del-live-zone-{{$liveZone->id}}">
            <div class="live-tlt">
              <h1 id="lz-title-{{$liveZone->id}}">{{$liveZone->title}}</h1>

              <div class="edit-info-btn">
                <i title="edit image" class="fa fa-edit" onclick="openLiveZone({{$liveZone->id}})"></i>
                <i title="edit image" class="fa fa-trash" onclick="deleteLiveZone({{$liveZone->id}})"></i>
              </div>

              

            </div>
            <div class="live-des">
              <p id="lz-des-{{$liveZone->id}}">{{$liveZone->description}}</p>
            </div>

            <div class="content-img-p lz-img-{{$liveZone->id}} @if($liveZone->status == 1) imgVideoBlock @else imgVideo @endif">
              <div class="content-img">
                <img id="lz-img-{{$liveZone->id}}" src="{{asset('liveZone/'.$liveZone->image)}}">
              </div>
            </div>

            <div class="content-img-p video lz-video-{{$liveZone->id}}  @if($liveZone->status == 2) imgVideoBlock @else imgVideo @endif">
              <div class="content-img">
                <iframe id="lz-video-{{$liveZone->id}}" src="{{$liveZone->image}}">
                </iframe>
              </div>
            </div>

          </div>
          @endforeach
          @endif
        </div>
      </div>
    </div>
    <div class="col-12 col-lg-5">
      <div class="add-pic">
        <div class="row mar-0" id="productGalleryV">
          @foreach($gallery as $gallery)
          <div class="col-6 col-sm-4 col-md-3 pad-0">
            <span class="fa fa-times remveImg" onclick="removeImg(event, '<?= $gallery->id ?>')"></span>
            <a href="{{ asset('gallery/'.$gallery->picture) }}" data-fancybox="gallery">
              <div class="single-content">
                <div class="content-img-p">
                  <div class="content-img">
                    <img src="{{ asset('gallery/re_'.$gallery->picture) }}" alt="img">
                  </div>
                </div>
              </div>
            </a>
          </div>
          @endforeach
        </div>
        <div class="add-p-inp">
          <span class="g-txt">@lang('lang.gallery')</span>
          <div class="g-input">
            <label for="file-upload" class="custom-file-upload">
              <i class="fa fa-plus"></i> @lang('lang.add_picture')
            </label>
            <input id="file-upload" multiple onchange="uploadGallery()" type="file" />
          </div>
        </div>
      </div>

      <div class="live-zona styling-one">
        <div class="section-title">@lang('lang.')@lang('lang.my_services')</div>
        @foreach($services as $service)
        <div class="live-zona-cnt">
          <div class="single-content">
            <div class="live-tlt">
              <h1>{{ $service->title }}</h1>
              <a href="{{ route('user.myService', [$service->title, $service->id]) }}">
                <div class="edit-info-btn">
                  <i class="fa fa-edit" title="edit image"></i>
                </div>
              </a>
            </div>
            <div class="content-img-p">
              <div class="content-img">
                @if($service->img)
                <img src="{{asset('assets/img/services/'. $service->img)}}" class="wth-100-p" alt="service-img">
                @else
                <img src="{{asset('assets/img/services/no_service.png')}}" class="wth-100-p" alt="service-img">
                @endif
              </div>
            </div>
          </div>
        </div>
        @endforeach
        <a class="btn-area d-block" onclick="Confirm('Create New Service', 'Are you sure you want to create new service', 'Yes', 'Cancel', '{{ route('user.addMyService') }}');">
          <label class="add-my-services btn-ctm-1">
            <i class="fa fa-plus"></i>@lang('lang.add_my_services')
          </label>
        </a>
      </div>
      <div class="live-zona styling-more">
        
        <div class="side-blog">
          <section class="blog-section">
            <div class="pop-master">
              <h1>@lang('lang.vip_profile')</h1>
            </div>
            <div id="vip-profile">
              
            </div>
            
          </section>
        </div>
        <div id="vip-services">
          
        </div>
        
      </div>

    </div>
  </div>
</section>
<section class="slider-section">
  <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{asset('assets/img/slider1.jpg')}}">
        <div class="carousel-caption">
          <h1>@lang('lang.third_slider_lavel')</h1>
          <p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('assets/img/slider2.jpg')}}">
        <div class="carousel-caption">
          <h1>@lang('lang.third_slider_lavel')</h1>
          <p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('assets/img/slider3.jpg')}}">
        <div class="carousel-caption">
          <h1>@lang('lang.third_slider_lavel')</h1>
          <p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">@lang('lang.previous')</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">@lang('lang.next')</span>
    </a>
  </div>
</section>


<!-- modal -->

<div class="modal fade cr-modal" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">@lang('lang.crop_the_image')</h4>
      </div>
      <div class="modal-body" style="width: 100%; overflow: hidden;">
        <div class="img-container">
          <img id="image" src="https://avatars0.githubusercontent.com/u/3456749" style="width: 100%">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">@lang('lang.cancel')</button>
        <button type="button" class="btn" id="crop">@lang('lang.crop')</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="proImgC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body edit-box-area">
        <div class="edit-box text-center">
          <span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
          <h3 class="">@lang('lang.update_your_picture')</h3>
          <div class="col-md-12" style="padding-bottom:13px">
            <div id="files">
              <?php
              if (Session::has('user_profile_img')) {
                $image = Session::get('user_profile_img');
                $html = '';
                $html .= '<span class="pip">';
                $html .= '<img class="imageThumb" src="' . asset('userImage/tmpPic/' . Auth()->user()->id . '/' . $image) . '">';
                $html .= '<br/>';
                $html .= '<span class="remove" id="' . $image . '">✖</span>';
                $html .= '</span>';
                echo $html;
              }
              ?>
            </div>
          </div>

          <label class="form-label text-dark"><b>@lang('lang.profile_picture')
              <div class="controls m-b-10">
                <label class="btn btn-primary btn-file">
                  <i class="fa fa-upload"></i>@lang('lang.upload_profile_picture')
                  <input type="file" style="display: none;" name="main_img" id="main_img">
                </label>
              </div>


              <form method="post" action="{{url('user/update-user-picture')}}" enctype="multipart/form-data">
                @csrf
                {{-- <input type="file" name="profile_image" class="form-control"> --}}
                <button class="btn btn-success">@lang('lang.update')</button>

              </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- modal -->

<div class="modal fade cr-modal" id="modal_cover" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">@lang('lang.crop_the_image')</h4>
      </div>
      <div class="modal-body" style="width: 100%; overflow: hidden;">
        <div class="img-container">
          <img id="c_image" src="https://avatars0.githubusercontent.com/u/3456749" style="width: 100%">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">@lang('lang.cancel')</button>
        <button type="button" class="btn" id="c_crop">@lang('lang.crop')</button>
      </div>
    </div>
  </div>
</div>

{{-- live zone modal --}}
<form action="{{url('user/add-live-zone')}}" id="live_zone_form" method="POST" enctype="multipart/form-data" onsubmit="return addUpdateLiveZone()">
  @csrf
  <div class="modal fade cr-modal" id="liveZoneModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">@lang('lang.add_live_zone')</h4>
        </div>
        <div class="modal-body" style="width: 100%; overflow: hidden;">
          <div class="col-12 pad-0">
            <div class="form-group">
              <label for="name">@lang('lang.title')</label>
              <input type="text" id="live_zone_title" class="form-control" placeholder="Title" aria-label="name" name="live_zone_title" required>
            </div>
          </div>
          <input type="hidden" name="live_zone_status" id="live_zone_status" value="">
          <input type="hidden" name="live_zone_id" id="live_zone_id" value="">
          <div class="col-12 pad-0">
            <div class="form-group">
              <label for="bio">@lang('lang.description')</label>
              <textarea id="live_zone_des" class="form-control" placeholder="Live zone description" name="live_zone_des"></textarea>
            </div>
          </div>
          <div class="col-12 pad-0">
            <div class="form-group">
              <label for="bio"><a href="javascript:;" class="add_a_img" onclick="imgEmbadeHideShow(1)">@lang('lang.update')</a></label>
              <label for="bio"><a href="javascript:;" class="embade_video" onclick="imgEmbadeHideShow(2)">@lang('lang.embade_a_video')</a></label>
            </div>
          </div>
          <div class="col-12 pad-0" id="link-area">
            <div class="form-group">
              <label for="name">@lang('lang.embade_link')</label>
              <input type="text" id="live_zone_vido_link" class="form-control" placeholder="Embade Video Link" aria-label="name" name="live_zone_vido_link">
            </div>
          </div>
          <div class="col-12 pad-0" id="img-area">
            <div class="form-group">
              <label for="name">@lang('lang.upload_image')</label>
              <input type="file" class="form-control-file" id="live_zone_img" name="live_zone_img">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">@lang('lang.cancel')</button>
          <button type="submit" class="btn" id="c_crop">@lang('lang.save')</button>
        </div>
      </div>
    </div>
  </div>
</form>
{{-- end live zone modal --}}


<div class="modal fade pro-i-m" id="proInfoC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body edit-box-area">
        <div class="edit-box">
          <span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
          <h3 class="text-center">@lang('lang.update_your_profile_info')</h3>
          <form autocomplete="off" method="post" action="" id="profile_info_form" enctype="multipart/form-data">
            @csrf
            <div class="col-12 pad-0 acc-type">
              <div class="form-group">
                <label for="RName">@lang('lang.account_type')<span class="c-red">*</span></label>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-check">
                      <input class="form-check-input sls" type="radio" name="account_type" id="exampleRadios1" value="1">
                      <label class="form-check-label" for="exampleRadios1">
                        @lang('lang.salons')
                      </label>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-check">
                      <input class="form-check-input cmp" type="radio" name="account_type" id="exampleRadios2" value="2">
                      <label class="form-check-label" for="exampleRadios2">
                        @lang('lang.personal')
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="name">@lang('lang.name')<span class="c-red">*</span></label>
                <input type="text" id="name" class="form-control" placeholder="Name" aria-label="name" name="name">
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="surname">@lang('lang.usurname')<span class="c-red">*</span></label>
                <input type="text" id="surname" class="form-control" placeholder="Surame" aria-label="surname" name="surname">
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="nickname">@lang('lang.nickname') @lang('lang.or') @lang('lang.company_name')<span class="c-red">*</span></label>
                <input type="text" id="nickname" class="form-control" placeholder="@lang('lang.nickname') @lang('lang.or') @lang('lang.company_name')" aria-label="nickname" name="nickname">
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="email">@lang('lang.email')</label>
                <input type="email" id="email" class="form-control" readonly placeholder="Email" aria-label="Email" name="email">
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="phone">@lang('lang.phone')</label>
                <input type="text" id="phone" class="form-control" placeholder="Phone" aria-label="Phone" name="phone">
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="address">@lang('lang.address')</label>
                <input type="text" id="autoAddress" class="form-control" placeholder="Address" aria-label="address" name="address">
              </div>
            </div>
            <input type="hidden" name="lat" id="lat" value="">
            <input type="hidden" name="long" id="long" value="">
            <input type="hidden" name="city" id="city" value="">
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="address">@lang('lang.show_on_profile')</label>
                <div class="row">
                  <div class="col-md-2">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input sh1" name="show_name[]" type="checkbox" id="inlineCheckbox1" value="1">
                      <label class="form-check-label" for="inlineCheckbox1">@lang('lang.name')</label>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input sh2" name="show_name[]" type="checkbox" id="inlineCheckbox1" value="2">
                      <label class="form-check-label" for="inlineCheckbox1">@lang('lang.usurname')</label>
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="form-check form-check-inline">
                      <input class="form-check-input sh3" name="show_name[]" type="checkbox" id="inlineCheckbox1" value="3">
                      <label class="form-check-label" for="inlineCheckbox1">@lang('lang.nickname') / @lang('lang.company_name')</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <h5>@lang('lang.social_info')</h5>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="facebook">@lang('lang.facebook_link')</label>
                <input type="text" id="facebook" class="form-control" placeholder="Facebook Link" aria-label="facebook" name="facebook">
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="twitter">@lang('lang.twitter_link')</label>
                <input type="text" id="twitter" class="form-control" placeholder="Twitter Link" aria-label="twitter" name="twitter">
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="gmail">@lang('lang.gmail_link')</label>
                <input type="text" id="gmail" class="form-control" placeholder="Gmail Link" aria-label="gmail" name="gmail">
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="linkedin">@lang('lang.linkedin_link')</label>
                <input type="text" id="linkedin" class="form-control" placeholder="Linkedin Link" aria-label="linkedin" name="linkedin">
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="pinterest">@lang('lang.pinterest_link')</label>
                <input type="text" id="pinterest" class="form-control" placeholder="Pinterest Link" aria-label="pinterest" name="pinterest">
              </div>
            </div>
            <div class="col-12 pad-0">
              <div class="form-group">
                <label for="bio">@lang('lang.short_bio')</label>
                <textarea id="bio" class="form-control" placeholder="Short Bio" name="bio"></textarea>
              </div>
            </div>
            <button class="btn btn-success float-right">@lang('lang.update')</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="coverImgC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body edit-box-area">
        <div class="edit-box text-center">
          <form method="post" action="{{url('user/update-cover-img')}}" enctype="multipart/form-data">
            @csrf
            <span onclick="hidePicEditBox()" title="colse" class="removeBox float-right p-2">&#10006;</span>
            <h3 class="">@lang('lang.update_your_cover_photos')</h3>
            <div class="col-md-12" style="padding-bottom:13px">
              <div class="row mar-0" id="cover_all_area">
                @foreach ($UserCoverA as $uc)
                <div class="col-6 col-sm-3 pad-l-0">
                  <span class="rmv-p" onclick="deleteCoverPic(event, {{ $uc->id }})">&#10006;</span>
                  <label class="image-checkbox {{ ($uc->status) ? 'image-checkbox-checked prv-has' : '' }}">
                    <img class="cover" src="{{ asset('userImage/coverPic/' . $uc->img) }}" />
                    <input type="checkbox" name="cover_img_d[]" value="{{ $uc->id }}" {{ ($uc->status) ? 'checked' : '' }} />
                    <i class="fa fa-check d-none"></i>
                  </label>
                </div>
                @endforeach
              </div>
            </div>

            <label class="form-label text-dark"><b>@lang('lang.cover_photos')</b></label>
            <div class="controls m-b-10">
              <label class="btn btn-primary btn-file">
                <i class="fa fa-upload"></i>@lang('lang.upload_cover_photos')
                <input type="file" style="display: none;" id="cover_img">
              </label>
            </div>


            {{-- <input type="file" name="profile_image" class="form-control"> --}}
            <button class="btn btn-success">@lang('lang.update')</button>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- progile payment --}}
<div class="modal fade pro-i-m" id="profile-payment-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body edit-box-area">
        <div class="edit-box">
          <span title="colse" class="removeBox float-right p-2">&#10006;</span>
          <h3 class="text-center">@lang('lang.make_your_profile_vip')</h3>
          <form autocomplete="off" method="post" action="" id="profile-payment-form">
            @csrf
            <input type="hidden" name="pkg_id" class="pkg_id">
            <div class="col-md-12 text-center" style="padding:20px; color:#45bd26">
              &#128;<span id="put-price">0.00</span>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <div class="ctm-select">
                  <div class="ctm-select-txt pad-l-10">
                    <span class="select-txt noselect" id="category">@lang('lang.choose_duration')</span>
                    <span class="select-arr"><i class="fas fa-caret-down"></i></span>
                  </div>
                  <div class="ctm-option-box put-sub-category">
                    @if(count($profilePackages) >0)
                    @foreach ($profilePackages as $pkg)
                    <div onclick="getPrice( {{$pkg->id }} )" class="ctm-option noselect"> {{ $pkg->duration }}</div>
                    @endforeach
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="RName">@lang('lang.payment_method')</label>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-check">
                      <input class="form-check-input paysera" type="radio" name="payment_type" id="exampleRadios1" value="1">
                      <label class="form-check-label" for="exampleRadios1">
                        @lang('lang.paysera')
                      </label>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-check">
                      <input class="form-check-input stripe" type="radio" name="payment_type" id="exampleRadios2" value="2" checked>
                      <label class="form-check-label" for="exampleRadios2">
                        @lang('lang.card_payment')
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <button class="btn btn-success float-right">@lang('lang.pay_now')</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

{{-- end prfile payment --}}



@endsection
@section('script')
<script src="{{ asset('assets/js/jquery.fancybox.js')}}"></script>
<script src="{{ asset('assets/js/cropper.js')}}"></script>
<script src="https://js.stripe.com/v3/"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script> 

<script>
  window.gallery_up = '<?= route('user.galleryFileUp') ?>';
  window.gallery_remove = '<?= route('user.galleryRemover') ?>';
  window.cover_pic_remove = '<?= route('user.userCoverRemover') ?>';
  window.profile_single_tmp_img = '<?= url("user/process-single-tmp-image") ?>';
  window.profile_single_tmp_remove = '<?= url("user/single-tmp-img-remove") ?>';
  window.cover_img_upload = '<?= url("user/cover-image-upload") ?>';
  window.get_profile_info = '<?= url("user/get-profile-information") ?>';
  window.insert_profile_info = '<?= url("user/insert-profile-information") ?>';

  // autocomplete address
  $(function() {
    var autocomplete;
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('autoAddress')), {
      types: ['geocode'],
    });
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var near_place = autocomplete.getPlace();
      
      var address = $('#autoAddress').val();
      var lat = near_place.geometry.location.lat();
      var lng = near_place.geometry.location.lng();
      var city = '';
      for (var i = 0; i < near_place.address_components.length; i++) {
        for (var j = 0; j < near_place.address_components[i].types.length; j++) {
          if (near_place.address_components[i].types[j] == 'administrative_area_level_2') {
            city = near_place.address_components[i].long_name;
          }
        }
      }
      if(city.indexOf("District")){
        city = city.replace("District", "");
      }
      $('#lat').val(lat);
      $('#long').val(lng);
      $('#city').val($.trim(city));
      console.log('city='+city+' address='+address+' lat='+lat+' lng='+lng);
    });

  });





  $(function() {
    $('[data-fancybox="gallery"]').fancybox({
      thumbs: {
        autoStart: true
      }
    });
  });
</script>
<script>
  function getPrice(id) {
    var data = new FormData();
    data.append('id', id);
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("vip-profile-amount")}}',
      success: function(response) {
        $('#put-price').text(response.price.toFixed(2));
        $('.pkg_id').val(id);
      }
    });
  }
</script>
<script>
  $('#profile-payment-form').on('submit', function() {
    var data = new FormData($('form#profile-payment-form')[0]);
    if ($('.pkg_id').val() < 1) {
      return false;
    }
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("vip-profile-payment")}}',
      success: function(response) {
        if ($(".stripe").is(":checked")) {
          var stripe = Stripe('pk_test_SzoBR6NC8mp5FvJEwkOld0nF00UlcgpKHq');
          console.log(response.url);
          stripe.redirectToCheckout({
            sessionId: response.url.id
          }).then(function(result) {

          });
        } else {
          window.location.href = response.url;
        }

        console.log(response);
      }
    });
    return false;

  });
</script>
@if(Session::has('success'))
<script>
  var x = document.getElementById("snackbar");
  x.style.background = '#7d0000';
  x.innerHTML = '{{ Session::get("success")}}';
  x.className = "show";
  setTimeout(function() {
    x.className = x.className.replace("show", "");
  }, 7000);
</script>
@endif
<script>
  function openLiveZone1() {
    $('#live_zone_status').val('');
    $('#live_zone_title').val('');
    $('#live_zone_des').val('');
    $('#live_zone_id').val('');
    $('#live_zone_vido_link').val('');
    $('#live_zone_img').val('');
    $('#live_zone_form').attr('action', "{{url('user/add-live-zone')}}");
    $('#link-area').hide();
    $('#img-area').hide();
    $('#live_zone_status').val('');
    $('#liveZoneModal').modal('show');

  }
</script>
<script>
  function imgEmbadeHideShow(number) {
    if (number == 1) {
      $('#link-area').hide();
      $('#img-area').show();
      $('#live_zone_status').val(1);
    } else if (number == 2) {
      $('#link-area').show();
      $('#img-area').hide();
      $('#live_zone_status').val(2);
    }
  }
</script>
<script>
  function addUpdateLiveZone() {
    var url = $('form#live_zone_form').attr('action');
    var pieces = url.split('/');
    var item = pieces[pieces.length - 1];
    if (item == 'add-live-zone') {
      AddLiveZone();
    } else if (item == 'update-live-zone') {
      updateLiveZone();
    }
    return false;
  }
</script>


<script>
  function openLiveZone(id) {
    var data = new FormData();
    data.append('id', id);
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: '{{url("user/edit-live-zone")}}',
      success: function(response) {
        $('#liveZoneModal').modal('show');
        $('#live_zone_title').val(response.title);
        $('#live_zone_des').val(response.description);
        $('#live_zone_id').val(response.id);
        $('#live_zone_form').attr('action', "{{url('user/update-live-zone')}}");
      }
    });
  }
</script>

<script>
  function AddLiveZone() {
    var data = new FormData($('form#live_zone_form')[0]);
    var url = $('form#live_zone_form').attr('action');
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: url,
      success: function(response) {
        console.log(response);
        $('.mylz-content').prepend(response);
        $('#liveZoneModal').modal('hide');
      }
    });

  }
</script>

<script>
  function updateLiveZone() {
    var data = new FormData($('form#live_zone_form')[0]);
    var url = $('form#live_zone_form').attr('action');
    $.ajax({
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url: url,
      success: function(response) {
        $('#lz-title-' + response.id).text(response.title);
        $('#lz-des-' + response.id).text(response.description);
        if (response.status == 1) {
          $('.lz-img-' + response.id).removeClass('imgVideo');
          $('.lz-img-' + response.id).removeClass('imgVideoBlock');
          $('.lz-img-' + response.id).addClass('imgVideoBlock');

          $('.lz-video-' + response.id).addClass('imgVideo');
          $('.lz-video-' + response.id).removeClass('imgVideoBlock');
          var i_url = "{{asset('liveZone')}}";
          var img_url = i_url + '/' + response.image;
          $('#lz-img-' + response.id).attr('src', img_url);
        }
        if (response.status == 2) {
          $('.lz-img-' + response.id).removeClass('imgVideoBlock');
          $('.lz-img-' + response.id).addClass('imgVideo');
          $('.lz-video-' + response.id).removeClass('imgVideo');
          $('.lz-video-' + response.id).addClass('imgVideoBlock');
          var video_url = response.image;
          $('#lz-video-' + response.id).attr('src', video_url);
        }
        $('#live_zone_status').val('');
        $('#live_zone_title').val('');
        $('#live_zone_des').val('');
        $('#live_zone_id').val('');
        $('#live_zone_vido_link').val('');
        $('#live_zone_img').val('');
        $('#liveZoneModal').modal('hide');
      }
    });

  }

</script>
<script>
  $(document).ready(function(){
    if ("geolocation" in navigator){
      navigator.geolocation.getCurrentPosition(function(position){
          lat = position.coords.latitude;
          lng = position.coords.longitude;
          getServiceAndProfile(lat, lng);
      },function() {
         
      });
    }
  });

  function getServiceAndProfile(lat, lng){
    var data = new FormData();
    data.append('lat', lat);
    data.append('lng', lng);
    data.append('_token', "{{csrf_token()}}");
    $.ajax( {
        processData: false,
        contentType: false,
        data: data,
        type: 'POST',
        url:'{{url("get-profile-and-service")}}',
        success: function( response ){
            $('#vip-profile').html(response.profiles);
            $('#vip-services').html(response.services);
        }
            
    });
  }
  function deleteLiveZone(id)
  {
    var data = new FormData();
    data.append('id', id);
    data.append('_token', "{{csrf_token()}}");
    $.ajax( {
        processData: false,
        contentType: false,
        data: data,
        type: 'POST',
        url:'{{url("user/delete-live-zone")}}',
        beforeSend: function() {
           return confirm('Are you sure to delete this item?');
        },
        success: function( response ){
            $('#del-live-zone-'+id).remove();
        }
            
    });
  }
</script>
@endsection