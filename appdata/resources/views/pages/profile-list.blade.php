 @extends('layout.app')
 @section('style')
 <style type="text/css">
 	.pagination {justify-content: center;}
 	.pagination > li > a{ color: #e2dede !important;}
	.page-link{background-color: #1a1a1a !important; border: 1px solid #1a1a1afc !important;}
	.page-item.active .page-link {
	    z-index: 3;
	    color: #fff;
	    background-color: #007bff !important;
	    border-color: #007bff !important;
	}
    .seeMore{
        color:#e25b37;
    }
    .card{
    	background-color: #1a1a1a;
    	margin-bottom: 15px;
    }
 </style>
 @endsection
 @section('content')
 <section class="blog-section">
 	<div class="link-menu">
 		{{-- <span class="arrow-l"><i class="fas fa-arrow-left"></i></span> 
 		<span><a href="#">Cetagory</a></span>
 		<span><i class="fas fa-chevron-right"></i></span>
 		<span>Sub Cetagory</span> --}}
 	</div>
 	<form method="get" action="{{url('profile-list')}}">
	 	<div class="row mar-0">
	 		<div class="col-md-10">
	 			<div class="form-group">
				    <input type="text" class="form-control profile_name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Name" name="profile_name" value="{{$data['profile_name']}}">
				</div>
	 		</div>
	 		<div class="col-md-2">
	 			<button type="submit" class="btn btn-primary">Search</button>
	 		</div>
	 	</div>
 	</form>
 	<div class="row mar-0">
		<div class="col-md-9">
			<div class="row">
				@if(count($data['users']) >0)
				@foreach($data['users'] as $user)
				<div class="col-md-4">
					<div class="card" style="width: 18rem;">
					  	<img class="card-img-top" src="{{asset('assets/img/services/'.$user->profile_image)}}" alt="Card image cap">
					  	<div class="card-body">
					    	<a href="{{url('public-profile/'.$user->id)}}"><h5 class="card-title"> {{$user->name}} {{$user->surname}} @if($user->nickname) ({{$user->nickname}}) @endif</h5></a>
					  	</div>
					</div>
				</div>
				@endforeach
				@else 
					<div class="col-md-12">
						<div class="alert alert-primary" role="alert">
						  Sorry, We not found any data with the name of {{$data['profile_name']}}
						</div>
					</div>
				@endif
			</div>
			<div>
				{{$data['users']->render()}}
			</div>
		</div>
		<div class="col-md-3">
			<div class="row">
				@if(count($data['vip_item'])>0)
				@foreach ($data['vip_item'] as $vip)
				<div class="col-md-12">
					@if(isset($vip->title))
					<div class="single-content">
                        <div class="content-img-p">
                            <div class="content-img">
                                <img src="{{asset('assets/img/services/'.$vip->image)}}">
                            </div>
                        </div>
                        <div class="content-des">
                            {{ substr($vip->description,0,65)}} 
                        </div>
                        <div class="content-tlt">
                            <a href="{{url('about-model/'.$vip->main_id)}}">{{ $vip->title }}</a>
                        </div>
                    </div>
					@else
					<div class="single-content single-content1">
                        <div class="content-img-p">
                            <div class="content-img">
                                <img src="{{asset('assets/img/services/'.$vip->profile_image)}}">
                            </div>
                        </div>
                        <div class="content-s-tlt">
                             <a href="{{url('public-profile/'.$vip->id)}}">{{ $vip->surname }}</a>
                        </div>
                        <div class="content-s-txt">
                            {{ substr($vip->bio,0,65)}} 
                        </div>
                    </div>
					@endif 
				</div>
				@endforeach
				@endif

			</div>
		</div>

 	</div>
 </section>

 <section class="bg-dark-sec">
 <section class="slider-section">
 	<div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
 		<div class="carousel-inner">
 			<div class="carousel-item active">
 				<img src="{{asset('assets/img/slider1.jpg')}}">
 				<div class="carousel-caption">
 					<h1>Third slide label</h1>
 					<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/slider2.jpg')}}">
 				<div class="carousel-caption">
 					<h1>Third slide label</h1>
 					<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/slider3.jpg')}}">
 				<div class="carousel-caption">
 					<h1>Third slide label</h1>
 					<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
 				</div>
 			</div>
 		</div>
 		<a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
 			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
 			<span class="sr-only">Previous</span>
 		</a>
 		<a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
 			<span class="carousel-control-next-icon" aria-hidden="true"></span>
 			<span class="sr-only">Next</span>
 		</a>
 	</div>
 </section>
</section>
 @endsection
 @section('script')
<script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        
        var current_url = window.location.href.split('?')[0];
        var page_number = $(this).attr('href').split('page=')[1];
        var link = current_url+'?page='+page_number;
        var profile_name = $('.profile_name').val();
        if($.trim(profile_name) !=''){
        	link = link+'&profile_name='+profile_name;
        }
        window.location = link;
        
        
    });
 </script>

    

 @endsection