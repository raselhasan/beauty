@extends('layout.app')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fancybox.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/cropper.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/app.css') }}"> --}}
<style type="text/css">
  .date a{
    color:#E25B37;
  }
  .url-color{
    color:#E25B37;
  }
</style>
@endsection
@section('content')
@include('inc.header-filter')
<section class="about-section userPro-section">
  <div class="row mar-0">
    <div class="col-12 col-lg-7">
      <div class="userPro-cnt">
        <div class="user-slider" onclick="changeCoverImage(event)">
          <div class="swiper-container ctm-container-4">
            <div class="swiper-wrapper">
              @if($UserCover->count())
              @foreach ($UserCover as $uc)
              <div class="swiper-slide">
                <div class="single-content wh-100">
                  <div class="content-img-p">
                    <div class="content-img">
                      <img src="{{ asset('userImage/coverPic/' . $uc->img) }}" alt="cover-pic">
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
              @else
              <div class="swiper-slide">
                <div class="single-content wh-100">
                  <div class="content-img-p">
                    <div class="content-img">
                      <img src="{{asset('assets/img/default_cover.png')}}" alt="user-img">
                    </div>
                  </div>
                </div>
              </div>
              @endif
            </div>
          </div>
        </div>
        <div class="userPro-bio">
          <div class="row mar-0">
            <div class="userPro-imgM">
              @if($userInfo['profile_image'])
              <img src="{{asset('userImage/fixPic/'. $userInfo['profile_image'])}}" alt="user-img">
              @else
              <img src="{{asset('assets/img/profile_pic.png')}}" alt="user-img">
              @endif
            </div>
            <div class="col-12">
              <div class="userPro-txt">
              <div class="userPro-tlt"><h3 class="setName">{{ $makeName }}</h3>
              </div>
              <div class="userPro-tel">
                <div class="userPro-tel-t">
                  <div class="pro-email"><i class="fa fa-envelope"></i>{{ $userInfo['email'] }}</div>
                  <div class="pro-phn"><i class="fas fa-mobile-alt"></i>{{ $userInfo['phone'] }}</div>
                  <div class="pro-cn"><i class="fas fa-map-marker-alt"></i>{{ $userInfo['address'] }}</div>
                </div>
              </div>
              <div class="social">
                <span>@lang('lang.share')</span>
                <span class="{{ ($userInfo['facebook']) ? '' : 'd-none' }}"><a class="fb" href="{{ ($userInfo['facebook']) ? $userInfo['facebook'] : 'javascript:void(0)' }}" target="_blank"><i class="fab fa-facebook-f"></i></a></span>
                <span class="{{ ($userInfo['twitter']) ? '' : 'd-none' }}"><a class="tw" href="{{ ($userInfo['twitter']) ? $userInfo['twitter'] : 'javascript:void(0)' }}" target="_blank"><i class="fab fa-twitter"></i></a></span>
                <span class="{{ ($userInfo['gmail']) ? '' : 'd-none' }}"><a class="go" href="{{ ($userInfo['gmail']) ? $userInfo['gmail'] : 'javascript:void(0)' }}" target="_blank"><i class="fab fa-goodreads-g"></i></a></span>
                <span class="{{ ($userInfo['linkedin']) ? '' : 'd-none' }}"><a class="lin" href="{{ ($userInfo['linkedin']) ? $userInfo['linkedin'] : 'javascript:void(0)' }}" target="_blank"><i class="fab fa-linkedin-in"></i></a></span>
                <span class="{{ ($userInfo['pinterest']) ? '' : 'd-none' }}"><a class="pin" href="{{ ($userInfo['pinterest']) ? $userInfo['pinterest'] : 'javascript:void(0)' }}" target="_blank"><i class="fab fa-pinterest-p"></i></a></span>

              </div>
            </div>
            <div class="chat-icon"><i class="fas fa-comments"></i></div>
          </div>
        </div>
        <div class="bio-txt">
          <h4 class="bio-txt-tlt">@lang('lang.bio')</h4>
          <p class="bio-txtP">
            {!! $userInfo['bio'] !!}
          </p>
        </div>
      </div>
    </div>
    <div class="live-zona">
      <div class="section-title">@lang('lang.live_zona')</div>
      <div class="live-zona-cnt">
        @if(count($liveZones)>0)
          @foreach ($liveZones as $liveZone)
            <div class="single-content">
              <div class="live-tlt">
                <h1>{{$liveZone->title}}</h1>
              </div>
              <div class="live-des">
                <p>{{$liveZone->description}}</p>
              </div>
              @if($liveZone->status == 1)
              <div class="content-img-p">
                <div class="content-img">
                  <img src="{{asset('liveZone/'.$liveZone->image)}}">
                </div>
              </div>
              @endif
              @if($liveZone->status == 2)
                <div class="content-img-p video">
                  <div class="content-img">
                    <iframe src="{{$liveZone->image}}">
                    </iframe>
                  </div>
                </div>
              @endif
            </div>
          @endforeach
        @endif  
        
      </div>
    </div>
  </div>
  <div class="col-12 col-lg-5">
    <div class="add-pic">
      <div class="row mar-0" id="productGalleryV">
        @foreach($gallery as $gallery)
        <div class="col-6 col-sm-4 col-md-3 pad-0">
          <a href="{{ asset('gallery/'.$gallery->picture) }}" data-fancybox="gallery">
            <div class="single-content">
              <div class="content-img-p">
                <div class="content-img">
                  <img src="{{ asset('gallery/re_'.$gallery->picture) }}" alt="img">
                </div>
              </div>
            </div>
          </a>
        </div>
        @endforeach
      </div>
      <div class="add-p-inp">
        <span class="g-txt">@lang('lang.gallery')</span>
      </div>
    </div>

    <div class="live-zona styling-one">
      <div class="section-title">@lang('lang.my_services')</div>
      @foreach($services as $service)
      <div class="live-zona-cnt">
        <div class="single-content">
          <div class="live-tlt">
            <h1><a href="{{ route('publicService', [$id, $service->title, $service->id]) }}">{{ $service->title }}</a></h1>
          </div>
          <div class="content-img-p">
            <div class="content-img">
              @if($service->img)
              <img src="{{asset('assets/img/services/'. $service->img)}}" class="wth-100-p" alt="service-img">
              @else
              <img src="{{asset('assets/img/services/no_service.png')}}" class="wth-100-p" alt="service-img">
              @endif
            </div>
          </div>
        </div>
      </div>
      @endforeach
      
    </div>
    <div class="side-blog">
      <section class="blog-section">
        <div class="pop-master"><h1>@lang('lang.vip_profile')</h1> </div>
        <div id="vip-profile"></div>
        
      </section>
    </div>

    <div id="vip-services"></div>
          
        

  </div>
</div>
</section>
<section class="slider-section">
  <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{asset('assets/img/slider1.jpg')}}">
        <div class="carousel-caption">
          <h1>@lang('lang.third_slider_lavel')</h1>
          <p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('assets/img/slider2.jpg')}}">
        <div class="carousel-caption">
          <h1>@lang('lang.third_slider_lavel')</h1>
          <p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('assets/img/slider3.jpg')}}">
        <div class="carousel-caption">
          <h1>@lang('lang.third_slider_lavel')</h1>
          <p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">@lang('lang.previous')</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">@lang('lang.next')</span>
    </a>
  </div>
</section>



@endsection
@section('script')
<script src="{{ asset('assets/js/jquery.fancybox.js')}}"></script>
<script src="{{ asset('assets/js/cropper.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script> 
<script>
  $(document).ready(function(){
    if ("geolocation" in navigator){
      navigator.geolocation.getCurrentPosition(function(position){
          lat = position.coords.latitude;
          lng = position.coords.longitude;
          getServiceAndProfile(lat, lng);
      },function() {
         
      });
    }
  });

  function getServiceAndProfile(lat, lng){
    var data = new FormData();
    data.append('lat', lat);
    data.append('lng', lng);
    data.append('_token', "{{csrf_token()}}");
    $.ajax( {
        processData: false,
        contentType: false,
        data: data,
        type: 'POST',
        url:'{{url("get-profile-and-service")}}',
        success: function( response ){
            $('#vip-profile').html(response.profiles);
            $('#vip-services').html(response.services);
        }
            
    });
  }
  </script>

<script>
  window.gallery_up =  '<?= route('user.galleryFileUp') ?>';
  window.gallery_remove =  '<?= route('user.galleryRemover') ?>';
  window.cover_pic_remove =  '<?= route('user.userCoverRemover') ?>';
  window.profile_single_tmp_img =  '<?= url("user/process-single-tmp-image") ?>';
  window.profile_single_tmp_remove =  '<?= url("user/single-tmp-img-remove") ?>';
  window.cover_img_upload =  '<?= url("user/cover-image-upload") ?>';
  window.get_profile_info =  '<?= url("user/get-profile-information") ?>';
  window.insert_profile_info =  '<?= url("user/insert-profile-information") ?>';


  $(function() {
    $('[data-fancybox="gallery"]').fancybox({
      thumbs : {
        autoStart : true
      }
    });
  });


</script>

@endsection