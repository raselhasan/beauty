@extends('layout.app')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/DataTables/css/jquery.dataTables.min.css') }}">
<style type="text/css">
    .gm-style-iw {
        max-width: 300px !important;
        max-height: 300px !important;
    }

    .gm-style-iw-d {
        max-width: 300px !important;
        max-height: 300px !important;
    }
</style>
@endsection
@section('content')
@include('inc.header-filter')
<section class="about-section">
    <div class="link-menu">
        <span class="arrow-l"><i class="fas fa-arrow-left"></i></span>
        <span><a href="#">Booking List</a></span>
    </div>
    <div class="row mar-0">
        <div class="col-12 col-lg-8">
            <div class="booking-list-area">
                <div class="table-responsive">
                    <table id="bookingListTable" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>Service</th>
                                <th>Type</th>
                                <th>Worker</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($booking as $book)
                            <tr>
                                <td>{{ $book->service->title }}</td>
                                <td>{{ $book->type->name }}</td>
                                <td>{{ $book->worker->name }}</td>
                                <td>{{ $book->date }}</td>
                                <td>{{ $book->start . ' - ' . $book->end }}</td>
                                <td>
                                    @if( $book->cancle == 1 )
                                    <button class="p-btn btn btn-danger" disabled>Cancle</button>
                                    @elseif( $book->confirm == 1 )
                                    @if( \Carbon\Carbon::now()->diffInMinutes( $book->new_date, false ) <= 0 ) <button class="p-btn btn btn-success" disabled>Complete </button>
                                        @else
                                        <button class="p-btn btn btn-info" disabled>Confirm</button>
                                        @endif
                                        @else
                                        @if( \Carbon\Carbon::now()->diffInMinutes( $book->new_date, false ) <= 0 ) <button class="p-btn btn btn-danger" disabled>Cancle</button>
                                            @else
                                            <button class="p-btn btn btn-danger" data-toggle="modal" data-target="#cancleBooking" onclick="$('.cancle-booking-url').attr('href', '<?= $book->cancle_url; ?>')">Cancle</button>
                                            <button class="p-btn btn btn-warning" disabled>Pending</button>
                                            @endif
                                            @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="social-icon">
                <ul>
                    <li class="{{ (Auth::user()->facebook) ? '' : 'd-none' }}"><a class="fb" href="{{ Auth::user()->facebook }}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="{{ (Auth::user()->twitter) ? '' : 'd-none' }}"><a class="tw" href="{{ Auth::user()->twitter }}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    <li class="{{ (Auth::user()->gmail) ? '' : 'd-none' }}"><a class="go" href="{{ Auth::user()->gmail }}" target="_blank"><i class="fab fa-goodreads-g"></i></a></li>
                    <li class="{{ (Auth::user()->linkedin) ? '' : 'd-none' }}"><a class="lin" href="{{ Auth::user()->linkedin }}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                    <li class="{{ (Auth::user()->pinterest) ? '' : 'd-none' }}"><a class="pin" href="{{ Auth::user()->pinterest }}" target="_blank"><i class="fab fa-pinterest-p"></i></a></li>
                </ul>
            </div>
            <div class="pro-box">
                <div class="row">
                    <div class="col-12">
                        <div class="pro-cnt">
                            <div class="pro-name">{{ Auth::user()->name }} {{ Auth::user()->surname }} {{ (Auth::user()->nickname) ? '(' . Auth::user()->nickname . ')' : '' }}</div>
                            <div class="pro-email"><i class="fa fa-envelope"></i> {{ Auth::user()->email }}</div>
                            <div class="pro-phn"><i class="fas fa-mobile-alt"></i>{{ Auth::user()->phone}}</div>
                            <div class="pro-btn">
                                <a href="{{ route('user.profile') }}"><button class="p-btn btn">Profle</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="side-blog">
                <section class="blog-section">
                    <div class="pop-master">
                        <h1>Populer Masters</h1>
                    </div>
                    <div class="blog">
                        <div class="blog-img">
                            <img src="{{asset('assets/img/man.jpg')}}">
                        </div>
                        <div class="blog-des">
                            <div class="blog-txt">
                                Lorem Ipsum is simply dummy text of the printing to make
                            </div>
                            <div class="blog-dta star">
                                <span class="date"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="blog">
                        <div class="blog-img">
                            <img src="{{asset('assets/img/man.jpg')}}">
                        </div>
                        <div class="blog-des">
                            <div class="blog-txt">
                                Lorem Ipsum is simply dummy text of the printing to make
                            </div>
                            <div class="blog-dta star">
                                <span class="date"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></span>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div>
</section>
<section class="slider-section bg-dark-sec">
    <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{asset('assets/img/slider1.jpg')}}">
                <div class="carousel-caption">
                    <h1>Third slide label</h1>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{asset('assets/img/slider2.jpg')}}">
                <div class="carousel-caption">
                    <h1>Third slide label</h1>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img src="{{asset('assets/img/slider3.jpg')}}">
                <div class="carousel-caption">
                    <h1>Third slide label</h1>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="cancleBooking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <section class="header-filter">
                    <div class="form-section">
                        <div class="form-all">
                            <div class="row mar-0">
                                <div class="col-12 pad-0">
                                    <h1 class="title">Are you sure?</h1>
                                </div>
                                <div class="col-12 pad-0">
                                    <div class="form-group btn-area">
                                        <button class="p-btn btn btn-info" type="button" onclick="$('#cancleBooking').modal('hide');">
                                            No
                                        </button>
                                        <a href="" class="cancle-booking-url">
                                            <button class="p-btn btn btn-danger" type="button">
                                                Yes
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('assets/DataTables/js/jquery.dataTables.min.js')}}"></script>
<script>
    window.base_url = "<?= url('/') ?>";
</script>
<script>
    $(function() {
        $('#bookingListTable').DataTable();
    });
</script>
@endsection