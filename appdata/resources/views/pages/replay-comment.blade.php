
<div class="reply">
	<div class="row">
		<div class="user-img-c">
			<img src="{{asset('userImage/fixPic/'.$replay['user']['profile_image'])}}" alt="user-img">
		</div>
		<div class="col-12 pad-0">
			<div class="comment-cnt">
				<h2>{{$replay['user']['surname']}}</h2>
				<h6>{{date('d-m-Y',strtotime($replay['created_at']))}} at {{date('H:m a',strtotime($replay['created_at']))}}</h6>
				<p>{{$replay['replay']}}</p>
				{{-- <span class="edit-com">@lang('lang.replay')</span> --}}
			</div>
		</div>
	</div>
</div>
