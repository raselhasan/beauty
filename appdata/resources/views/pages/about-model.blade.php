 @extends('layout.app')
 @section('style')
 <link rel="stylesheet" href="{{ asset('assets/range/css/pignose.calendar.min.css') }}">
 <style type="text/css">
 	.model-s-img img{
 		height: 287px;
 	}
 	.gm-style-iw {
	  max-width: 300px !important;
	  max-height: 300px !important; 
	}
	.gm-style-iw-d{
		max-width: 300px !important;
	  	max-height: 300px !important; 
	}
 </style>
 @endsection
 @section('content')
 @include('inc.header-filter')
 <section class="about-section">
 	<div class="link-menu">
 		<span class="arrow-l"><i class="fas fa-arrow-left"></i></span> 
 		<span><a href="#">@lang('lang.cetagory')</a></span>
 		<span><i class="fas fa-chevron-right"></i></span>
 		<span>@lang('lang.sub_cetagory')</span>
 	</div>
 	<div class="row mar-0">
 		<div class="col-12 col-lg-8">
 			<div class="about-cnt mar-b-30">
 				<div class="about-tlt"><h1>{{$service['title']}}</h1></div>
 				<div class="about-txt">
 					<p>	{!! $service['description'] !!}</p>
 				</div>
 			</div>
 			@if(count($service['service_gallery_tabs'])>0)
	 			<div class="about-nav">
	 				<ul class="nav nav-pills" id="pills-tab" role="tablist">
	 					<li class="nav-item">
	 						<a class="nav-link active" id="pills-all-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-home" aria-selected="true">All</a>
	 					</li>
	 					@if(count($service['service_gallery_tabs'])>0)
							@foreach ($service['service_gallery_tabs'] as $tab)
								<li class="nav-item">
			 						<a class="nav-link" id="pills-shaps-tab" data-toggle="pill" href="#tab-{{$tab['id']}}" role="tab" aria-controls="pills-contact" aria-selected="false">{{ $tab['tab']}}</a>
			 					</li>
							@endforeach
	 					@endif
	 				</ul>
	 				<div class="tab-content" id="pills-tabContent">
	 					<div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
	 						<div class="row mar-0">
	 							@if($service['service_gallery_tabs'])
									@foreach ($service['service_gallery_tabs'] as $tab)
										@if(count($tab['service_galleries'])>0)
			 								@foreach ($tab['service_galleries'] as $tabimg)
					 							<div class="col-6 col-sm-4 col-md-3 col-lg-2 pad-0">
					 								<div class="single-content">
					 									<div class="content-img-p">
					 										<div class="content-img">
					 											<img src="{{asset('assets/img/services/galleries/'.$tabimg['img'])}}">
					 										</div>
					 									</div>
					 								</div>
					 							</div>
					 						@endforeach
					 					@endif
					 				@endforeach
					 			@endif				
	 						</div>
	 					</div>
						@if($service['service_gallery_tabs'])
							@foreach ($service['service_gallery_tabs'] as $tab)
			 					<div class="tab-pane fade" id="tab-{{$tab['id']}}" role="tabpanel" aria-labelledby="pills-test-tab">
			 						<div class="row mar-0">
			 							@if(count($tab['service_galleries'])>0)
			 								@foreach ($tab['service_galleries'] as $tabimg)
					 							<div class="col-12 col-md-3 col-lg-2 pad-0">
					 								<div class="single-content">
					 									<div class="content-img-p">
					 										<div class="content-img">
					 											<img src="{{asset('assets/img/services/galleries/'.$tabimg['img'])}}">
					 										</div>
					 									</div>
					 								</div>
					 							</div>
					 						@endforeach	
			 							@endif

			 						</div>
			 					</div>
			 				@endforeach
	 					@endif				
	 				</div>
	 			</div>
 			@endif
 			<div class="apointment-s">
 				<div class="row mar-0">
 					<div class="col-12 col-md-8 pad-0">
 						<div class="apointment-tbl-all">
 							<div class="api-tlt">@lang('lang.available_apointment')</div>
 							<div class="apointment-tbl">
 								<div class="apointment-t"><i class="far fa-clock"></i> @lang('lang.time1')</div>
 								<div class="apointment-a">@lang('lang.time2')</div>
 								<div class="apointment-btn"> <button class="p-btn btn"> <span class="unavai-txt">@lang('lang.unavailable')</span><span class="avai-txt">@lang('lang.book_available')</span> </button> </div>
 							</div>
 							<div class="apointment-tbl active">
 								<div class="apointment-t"><i class="far fa-clock"></i> @lang('lang.time1')</div>
 								<div class="apointment-a ">@lang('lang.time2')</div>
 								<div class="apointment-btn "> <button class="p-btn btn"> <span class="unavai-txt">@lang('lang.unavailable')</span><span class="avai-txt">@lang('lang.book_available')</span> </button> </div>
 							</div>
 						</div>
 					</div>
 					<div class="col-12 col-md-4 pad-0">
 						<div class="calendar-show">
 							<div class="calendar"></div>
 						</div>
 					</div>
 				</div>
 			</div>
 			<div class="comment-s">
 				<div class="com-tlt"><h1><span id="total_comment">{{$total_comment}}</span>  Comments</h1></div>
 				<div class="com-text-ar">
 					<div class="form-group">
 						<textarea class="form-control" id="user_comment"></textarea>
 					</div>
 					<div class="form-group">
 						<button class="p-btn btn send" onclick="addComment()">@lang('lang.send')</button>
 						<button class="p-btn btn" onclick="textClear()">@lang('lang.clear')</button>
 					</div>
 				</div>
 				<div class="com-reply">
 					@if(count($comments) > 0)
 						@foreach ($comments as $comment)
		 					<div class="comment" id="main-cmt-{{$comment['id']}}">
		 						<div class="row">
		 							<div class="user-img-c">
		 								<img src="{{asset('userImage/fixPic/'.$comment['user']['profile_image'])}}" alt="user-img">
		 							</div>
		 							<div class="col-12 pad-0">
		 								<div class="comment-cnt">
		 									<h2>{{ $comment['user']['surname']}}</h2>
		 									<h6>
												{{date('d-m-Y',strtotime($comment['created_at']))}} at {{date('H:m a',strtotime($comment['created_at']))}}
		 									</h6>
		 									<p>{{ $comment['comment']}}</p>
		 									<span class="edit-com" onclick="replayModelShow('main-cmt-{{$comment["id"]}}')">Replay</span>
		 								</div>
										<div class="cmt-replay">
											@if(count($comment['comment_replay'])>0)
												@foreach ($comment['comment_replay'] as $replay)
					 								<div class="reply">
					 									<div class="row">
					 										<div class="user-img-c">
					 											<img src="{{asset('userImage/fixPic/'.$replay['user']['profile_image'])}}" alt="user-img">
					 										</div>
					 										<div class="col-12 pad-0">
					 											<div class="comment-cnt">
					 												<h2>{{$replay['user']['surname']}}</h2>
					 												<h6>{{date('d-m-Y',strtotime($comment['created_at']))}} at {{date('H:m a',strtotime($replay['created_at']))}}</h6>
					 												<p>{{$replay['replay']}}</p>
					 												{{-- <span class="edit-com">Replay</span> --}}
					 											</div>
					 										</div>
					 									</div>
					 								</div>
					 							@endforeach	
			 								@endif
		 								</div>
		 							</div>
		 						</div>
		 					</div>
 						@endforeach
 					@endif
 				</div>
 			</div>
 		</div>
 		<div class="col-12 col-lg-4">
 			<div class="model-bg-img">
 				<div class="model-s-img">
 					<img src="{{asset('assets/img/services/'.$service['image'])}}" alt="user-img">
 				</div>
 			</div>
 			<div class="social-icon">
 				<ul>
 					<li><a class="fb" href="#"><i class="fab fa-facebook-f"></i></a></li>
 					<li><a class="tw" href="#"><i class="fab fa-twitter"></i></a></li>
 					<li><a class="go" href="#"><i class="fab fa-goodreads-g"></i></a></li>
 					<li><a class="lin" href="#"><i class="fab fa-linkedin-in"></i></a></li>
 					<li><a class="pin" href="#"><i class="fab fa-pinterest-p"></i></a></li>
 				</ul>
 			</div>
 			<div class="pro-box">
 				<div class="row">
 					<div class="col-12">
 						<div class="pro-cnt">
 							<div class="pro-name">{{$service['user']['name']}} {{$service['user']['surname']}}</div>
 							<div class="pro-email"><i class="fa fa-envelope"></i>{{$service['user']['email']}}</div>
 							<div class="pro-phn"><i class="fas fa-mobile-alt"></i>{{$service['user']['phone']}}</div>
 							<div class="pro-cn"><i class="fas fa-map-marker-alt"></i>{{$service['address']}}</div>
 							<div class="pro-btn">
 								<a href="{{url('public-profile/'.$service['user']['id'])}}"><button class="p-btn btn">Profle</button></a>
 							</div>
 						</div>
 					</div>
 					<div class="pro-img">
 						<img src="{{asset('assets/img/services/'.$service['image'])}}" alt="user-img">
 					</div>
 				</div>
 			</div>
 			<div class="pro-table">
 				<table class="table table-striped">
 					<tbody>
 						<tr class="head-tr">
 							<td>Type</td>
 							<td>Price</td>
 						</tr>
 						@if(count($service['types'])>0)
							@foreach ($service['types'] as $type)
								<tr>
									<td>{{ $type['name']}}</td>
									<td>{{ $type['price']}}</td>
								</tr>
							@endforeach
						@endif
 					</tbody>
 				</table>
 			</div>
 			<div class="g-map">
				<div class="mapouter">
					<div class="gmap_canvas" id="map">
						
					</div>
				</div>
			</div>

 			<div class="side-blog">
 				<section class="blog-section">
 					<div class="pop-master"><h1>@lang('lang.populer_masters')</h1> </div>
 					<div class="blog">
 						<div class="blog-img">
 							<img src="{{asset('assets/img/man.jpg')}}">
 						</div>
 						<div class="blog-des">
 							<div class="blog-txt">
 								@lang('lang.demo1')
 							</div>
 							<div class="blog-dta star">
 								<span class="date"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></span>
 							</div>
 						</div>
 					</div>
 					<div class="blog">
 						<div class="blog-img">
 							<img src="{{asset('assets/img/man.jpg')}}">
 						</div>
 						<div class="blog-des">
 							<div class="blog-txt">
 								@lang('lang.demo1')
 							</div>
 							<div class="blog-dta star">
 								<span class="date"><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i><i class="far fa-star"></i></span>
 							</div>
 						</div>
 					</div>
 				</section>
 			</div>

 		</div>
 	</div>
 </section>
 <section class="slider-section bg-dark-sec">
 	<div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
 		<div class="carousel-inner">
 			<div class="carousel-item active">
 				<img src="{{asset('assets/img/slider1.jpg')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/slider2.jpg')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')/h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/slider3.jpg')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 				</div>
 			</div>
 		</div>
 		<a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
 			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
 			<span class="sr-only">@lang('lang.previous')</span>
 		</a>
 		<a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
 			<span class="carousel-control-next-icon" aria-hidden="true"></span>
 			<span class="sr-only">@lang('lang.next')</span>
 		</a>
 	</div>
 </section>
{{-- replay comment modal --}}

<div class="modal fade cr-modal" id="replayModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" style="z-index: 9999999">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">@lang('lang.replay_comment')</h4>
      </div>
      <div class="modal-body" style="width: 100%; overflow: hidden;">
        <div class="com-text-ar">
        	<input type="hidden" name="comment_id" id="comment_id">
			<div class="form-group">
				<textarea class="form-control" id="modal_user_comment"></textarea>
			</div>
			
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">@lang('lang.cancel')</button>
        <button type="button" class="btn" id="c_crop" onclick="mainReplay()">@lang('lang.send')</button>
      </div>
    </div>
  </div>
</div>

{{-- end replay modal --}}



 @endsection
 @section('script')

 <script src="{{ asset('assets/range/js/pignose.calendar.full.min.js') }}"></script>
 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script>
 <script>
 	$(document).ready(function(){
 	});
 	var swiper = new Swiper('.ctm-container3', {
 		slidesPerView: 5,
 		spaceBetween: 15,
 		loop: false,
 		navigation: {
 			nextEl: '.swiper-button-next',
 			prevEl: '.swiper-button-prev',
 		},

 		breakpoints: {
 			1400: {
 				slidesPerView: 4,
 			},
 			1000: {
 				slidesPerView: 3,
 			},
 			700: {
 				slidesPerView: 2,
 			},
 			450: {
 				slidesPerView: 1,
 			}
 		}
 	});
 	$(function() {
 		$('.calendar').pignoseCalendar({
                    theme: 'dark' // light, dark, blue
                });
 	});
 </script>
 <script>
	$(function () {
		var lat  = '{{$service["lat"]}}';
		var lng = '{{$service["lng"]}}';
		serviceMap(lat,lng);

	});
	function serviceMap(lat, lng)
	{
		lat = parseFloat(lat);
		lng = parseFloat(lng);
		var map = new google.maps.Map(document.getElementById('map'), {
		  center: {lat: lat, lng: lng},
		  zoom: 8
		});
		var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            map: map,
            title: 'My Location'
        });
        
        var mainContent = '';
        mainContent +='<div style="color:#1014a0;font-size:17px; text-align:center;margin-bottom:5px">{{$service['title']}}</div>';
        mainContent += '<div style="color:#1014a0;font-size:17px; text-align:center;margin-bottom:5px">{{ $service['user']['name'] }}&nbsp;{{ $service['user']['surname'] }}&nbsp;{{ ($service['user']['nickname']) ? '(' . $service['user']['nickname'] . ')' : '' }}</div>';
        mainContent +='<div style="color:red; text-align:left;margin-bottom:5px"><i class="fa fa-envelope" style="margin-right:6px;"></i> {{ $service['user']['email'] }}</div>';
        mainContent +='<div style="color:red; text-align:left;margin-bottom:5px"><i class="fas fa-mobile-alt" style="margin-right:6px;"></i>{{ $service['user']['phone']}}</div>';
        mainContent +='<div style="color:red; text-align:left;margin-bottom:5px"><i class="fas fa-map-marker-alt" style="margin-right:6px;"></i>'+ $('#autoAddress').val() +'</div>';

		var infowindow = new google.maps.InfoWindow();

        marker.addListener('click', function() {
        	infowindow.setContent(mainContent);
		    infowindow.open(map, marker);
		});
	}
</script>

<script>
	function addComment()
	{
		sendComment();
	}
	
	function sendComment()
	{
		var comment = $('#user_comment').val();
		if($.trim(comment)!=''){
			var data = new FormData();
	        data.append('comment', comment);
	        data.append('service_id','{{$service['id']}}');
	        $.ajax( {
	            processData: false,
	            contentType: false,
	            data: data,
	            type: 'POST',
	            url:'{{url("/user/add-comment")}}',
	            success: function( response ){
	                $('#user_comment').val('');
	                $('.com-reply').prepend(response);
	                var t_comment = $('#total_comment').text();
	                var total_comment = parseInt(t_comment) + 1;
	                $('#total_comment').text(total_comment);
	                console.log(response);
	            }
	                
	        }); 
		}
	}
</script>
<script>
	function replayModelShow(comment_id)
	{
		$('#comment_id').val(comment_id);
		$('#replayModal').modal('show');

	}

	function mainReplay()
	{
		var comment_id = $('#comment_id').val();
		var comment = $('#modal_user_comment').val();
		if($.trim(comment) !=''){
			var data = new FormData();
	        data.append('comment', comment);
	        data.append('comment_id', comment_id);
	        data.append('service_id','{{$service['id']}}');
			$.ajax( {
	            processData: false,
	            contentType: false,
	            data: data,
	            type: 'POST',
	            url:'{{url("/user/main-comment-replay")}}',
	            success: function( response ){
	            	$('#modal_user_comment').val('');
	            	$('#replayModal').modal('hide');
	                $('.cmt-replay').prepend(response);
	                var t_comment = $('#total_comment').text();
	                var total_comment = parseInt(t_comment) + 1;
	                $('#total_comment').text(total_comment);
	            }
	                
	        }); 
		}
	}
	function textClear()
	{
		$('#user_comment').val('');
	}
</script>




 @endsection