<section class="section1">
 	<div class="section-title">@lang('lang.vip_services')</div>
 	<div class="show-content">
 		<div class="row mar-0">
 			@php
 				$s1 = 0;
 				$p1 = 0;
 			@endphp
			@if(count($services)>0)
				@foreach ($services as $service)
					@php
						$s1 = $s1+1;
						if($s1>4){
							break;
						}
					@endphp
		 			<div class="col-12 col-sm-6 col-md-4 col-lg-3 pad-0">
		 				<div class="single-content">
		 					<div class="content-img-p">
		 						<div class="content-img">
		 							<img src="{{asset('assets/img/services/'.$service->image)}}">
		 						</div>
		 					</div>
		 					<div class="content-des">
		 						{!! substr($service->description, 0, 90); !!}...
		 						<a href="{{url('about-model/'.$service->main_id)}}" class="seeMore">@lang('lang.see_more')</a>
		 					</div>
		 					<div class="content-tlt">
		 						{{$service->title}}
		 					</div>

		 				</div>
		 			</div>
	 			@endforeach
			@endif

			@if(count($profiles)>0)
				@foreach ($profiles as $profile)
					@php
						$p1 = $p1+1;
						if($p1==5){
							break;
						}
					@endphp
		 			<div class="col-12 col-sm-6 col-md-4 col-lg-3 pad-0">
		 				<div class="single-content single-content1">
							<div class="content-img-p">
								<div class="content-img">
									<img src="{{asset('assets/img/services/'.$profile->profile_image)}}">
								</div>
							</div>
							<div class="content-s-tlt">
								{{ $profile->surname }}
							</div>
							<div class="content-s-txt">
								{{ substr($profile->bio,0,50)}}...
								<a href="{{url('public-profile/'.$profile->id)}}" class="seeMore">@lang('lang.see_more')</a>
							</div>

						</div>
		 			</div>
 				@endforeach
			@endif
 			
 		</div>
 	</div>
</section>
<section class="section1">
 	<div class="section-title">@lang('lang.vip_profile')</div>
 	<div class="show-content">
 		<div class="row mar-0">
 			@php
 				$p2 = 0;
 				$s2 = 0;
 			@endphp
 			@if(count($profiles)>0)
				@foreach ($profiles as $profile1)
					@php
						$p2 = $p2+1;
					@endphp
					@if($p2>4)
			 			<div class="col-12 col-sm-6 col-md-4 col-lg-3 pad-0">
			 				<div class="single-content single-content1">
								<div class="content-img-p">
									<div class="content-img">
										<img src="{{asset('assets/img/services/'.$profile1->profile_image)}}">
									</div>
								</div>
								<div class="content-s-tlt">
									{{ $profile1->surname }}
								</div>
								<div class="content-s-txt">
									{{ substr($profile1->bio,0,50)}}...
									<a href="{{url('public-profile/'.$profile1->id)}}" class="seeMore">@lang('lang.see_more')</a> 
								</div>

							</div>
			 			</div>
		 			@endif

 				@endforeach
			@endif

			@if(count($services)>0)
				@foreach ($services as $service1)
					@php
						$s2 = $s2+1;
					@endphp
					@if($s2>4)
			 			<div class="col-12 col-sm-6 col-md-4 col-lg-3 pad-0">
			 				<div class="single-content">
			 					<div class="content-img-p">
			 						<div class="content-img">
			 							<img src="{{asset('assets/img/services/'.$service1->image)}}">
			 						</div>
			 					</div>
			 					<div class="content-des">
			 						{!! substr($service1->description, 0, 90); !!}...
			 						<a href="{{url('about-model/'.$service1->main_id)}}" class="seeMore">@lang('lang.see_more')</a>
			 					</div>
			 					<div class="content-tlt">
			 						{{$service1->title}}
			 					</div>

			 				</div>
			 			</div>
		 			@endif
	 			@endforeach
			@endif

 		</div>
 	</div>
 </section>