 @extends('layout.app')
 @section('style')
 <style type="text/css">
 	.pagination {justify-content: center;}
 	.pagination > li > a{ color: #e2dede !important;}
	.page-link{background-color: #1a1a1a !important; border: 1px solid #1a1a1afc !important;}
	.page-item.active .page-link {
	    z-index: 3;
	    color: #fff;
	    background-color: #007bff !important;
	    border-color: #007bff !important;
	}
    .seeMore{
        color:#e25b37;
    }
 </style>
 @endsection
 @section('content')
 @include('inc.service-filter')
 <section class="blog-section">
 	<div class="link-menu">
 		{{-- <span class="arrow-l"><i class="fas fa-arrow-left"></i></span> 
 		<span><a href="#">@lang('lang.cetagory')</a></span>
 		<span><i class="fas fa-chevron-right"></i></span>
 		<span>@lang('lang.sub_cetagory')</span> --}}
 	</div>
 	<div class="row mar-0" id="services">

 	</div>
 </section>

 <section class="bg-dark-sec">
 <section class="section2">
 	<div class="section-title">@lang('lang.palaugos_salia_taves')</div>
 	<div class="row mar-0">
 		<div class="col-12">
 			<div class="swiper-container ctm-container3">
 				<div class="swiper-wrapper">
 					@if(count(@$item['recently_viewed'])>0)
	 					@foreach (@$item['recently_viewed'] as $viewed)
		 					<div class="swiper-slide">
		 						<div class="single-content single-content1">
		 							<div class="content-img-p">
		 								<div class="content-img">
		 									<img src="{{asset('assets/img/services/'.$viewed->image)}}">
		 								</div>
		 							</div>
		 							<div class="content-s-tlt">
		 								<a href="{{url('about-model/'.$viewed->id)}}">{{$viewed->title}}</a>
		 							</div>
		 							<div class="content-s-txt">
		 								{!! substr($viewed->description, 0, 100); !!} <a href="{{url('about-model/'.$viewed->id)}}" class="seeMore">@lang('lang.see_more')</a> 
		 							</div>
		 						</div>
		 					</div>
 						@endforeach
 					@endif
 					
 				</div>
 				<!-- Add Arrows -->
 			</div>
 			<div class="sw-np">
 				<div class="swiper-button-next swiper-button-next1"><img src="{{ asset('assets/img/right.png') }}" alt="Left Arrow Key"></div>
 				<div class="swiper-button-prev swiper-button-prev1"><img src="{{ asset('assets/img/left.png') }}" alt="Right Arrow Key"></div>
 			</div>
 		</div> 
 	</div>
 	<div class="more"><button class="btn more-btn">@lang('lang.visus_ancitos')</button></div>
 </section>
 <section class="slider-section">
 	<div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
 		<div class="carousel-inner">
 			<div class="carousel-item active">
 				<img src="{{asset('assets/img/slider1.jpg')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/slider2.jpg')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/slider3.jpg')}}">
 				<div class="carousel-caption">
 					 <h1>@lang('lang.third_slider_lavel')</h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p> 
 				</div>
 			</div>
 		</div>
 		<a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
 			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
 			<span class="sr-only">@lang('lang.previous')</span>
 		</a>
 		<a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
 			<span class="carousel-control-next-icon" aria-hidden="true"></span>
 			<span class="sr-only">@lang('lang.next')</span>
 		</a>
 	</div>
 </section>
</section>
 @endsection
 @section('script')
 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script>

    @include("service_script.script");

 @endsection