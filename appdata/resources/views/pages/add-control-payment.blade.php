 @extends('layout.app')
 @section('style')
 <style>
 	.all_service{
 		margin-left: 20px;
 	}
 	.card{
 		background-color: #2d2a2aad;
 	}
 </style>
 @endsection
 @section('content')
 <section class="select-panel-section">
 	<form id="add_controll_form">
 	<div class="row">
	 	<div class="col-md-3"></div>
	 	<div class="col-md-6">
	 		<div class="card">
			  <div class="card-header">
			    @lang('lang.add_controll_payment')
			  </div>
			  <div class="card-body">
			  	<div class="col-md-12 text-center" style="padding:20px; color:#45bd26">
	              &#128;<span id="put-price">0.00</span>
	            </div>
	            <input type="hidden" name="pkg_id" class="pkg_id">
			   	<div class="form-group">
	                <div class="ctm-select">
	                  <div class="ctm-select-txt pad-l-10">
	                    <span class="select-txt noselect" id="category">@lang('lang.choose_duration')</span>
	                    <span class="select-arr"><i class="fas fa-caret-down"></i></span>
	                  </div>
	                  <div class="ctm-option-box put-sub-category">
	                    @if(count($AddShowingPackage) >0)
	                      @foreach ($AddShowingPackage as $pkg)
	                        <div onclick="getPrice( {{$pkg->id }} )" class="ctm-option noselect"> {{ $pkg->duration }}</div>
	                      @endforeach
	                    @endif  
	                  </div>
	                </div>
              	</div>
              	<div class="row">
              		<div class="col-md-6">
              			<div class="form-check">
	                      <input class="form-check-input paysera" type="radio" name="payment_type" id="exampleRadios1" value="1">
	                      <label class="form-check-label" for="exampleRadios1">
	                          @lang('lang.paysera')
	                       </label>
	                    </div>
              		</div>
              		<div class="col-md-6">
              			<div class="form-check">
	                        <input class="form-check-input stripe" type="radio" name="payment_type" id="exampleRadios2" value="2" checked>
	                        <label class="form-check-label" for="exampleRadios2">
	                          @lang('lang.card_payment')
	                        </label>
	                    </div>
              		</div>
              	</div>
			  </div>
			  <div class="card-footer text-right">
			    <button class="btn btn-success float-right" type="submit">@lang('lang.pay_now')</button>
			  </div>
			</div>
	 	</div>
	 	<div class="col-md-3"></div>
 	</div>
 	</form>
</section>
@endsection
@section('script')
<script src="https://js.stripe.com/v3/"></script>
<script>
  function getPrice(id)
  {
    var data = new FormData();
    data.append('id',id);
    $.ajax( {
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url:'{{url("add-control-payment/get-price")}}',
      success: function( response ){
        $('#put-price').text(response.price.toFixed(2));
        $('.pkg_id').val(id);
        console.log(response);
      }   
    }); 
  }
</script>
<script>
  $('#add_controll_form').on('submit',function(){
    var data = new FormData( $( 'form#add_controll_form' )[ 0 ] );
    if($('.pkg_id').val() < 1){
      return false;
    }
    $.ajax( {
      processData: false,
      contentType: false,
      data: data,
      type: 'POST',
      url:'{{url("add-control-payment/make")}}',
      success: function( response ){
        if ($(".stripe").is(":checked")) {
          var stripe = Stripe('pk_test_SzoBR6NC8mp5FvJEwkOld0nF00UlcgpKHq');
          console.log(response.url);
          stripe.redirectToCheckout({
              sessionId: response.url.id
          }).then(function (result) {

          });
        }else{
          window.location.href = response.url;
        }

        console.log(response);
      }   
    });
    return false;

  });
</script>
@endsection