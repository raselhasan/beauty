 <script>
    $(document).ready(function(){
        
    });
    var swiper = new Swiper('.ctm-container3', {
        slidesPerView: 5,
        spaceBetween: 15,
        loop: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        breakpoints: {
            1400: {
                slidesPerView: 4,
            },
            1000: {
                slidesPerView: 3,
            },
            700: {
                slidesPerView: 2,
            },
            450: {
                slidesPerView: 1,
            }
        }
    });
 </script>

{{-- when page load --}}
 <script>
    var link = '';
    var Mycity = '';
    $(document).ready(function(){
        var myBack = $('.backk').val();
        if(myBack == 0){
            setDefautlValue();
            setOtherData();
        }else{
            sendInitalData();
        }

        
    });
 </script>
{{-- end page load --}} 



{{-- get user current address--}}  
<script>
    function setDefautlValue(){
        if ("geolocation" in navigator){
            navigator.geolocation.getCurrentPosition(function(position){
                var currentLatitude = position.coords.latitude;
                var currentLongitude = position.coords.longitude;
                var geocoder;
                geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
                geocoder.geocode({'latLng': latlng}, function(results, status) {
                    if(status == google.maps.GeocoderStatus.OK) {
                        if(results[1]) {
                            var address = results[0].formatted_address;
                            var city = '';
                            for(var i = 0; i<results[0].address_components.length; i++){
                                for(var j = 0; j< results[0].address_components[i].types.length; j++){
                                    if(results[0].address_components[i].types[j] == 'administrative_area_level_2'){
                                        Mycity = results[0].address_components[i].long_name;
                                        Mycity = Mycity.split('District').join("");
                                        
                                        setSetWithLocation(Mycity,currentLatitude,currentLongitude);
                                    }
                                }
                            }
                            
                        }
                    }
                }); 
            },function() {
                setSetWithNoLocation();
            });
        }       
    }

    function setSetWithLocation(myCity, lat, lng)
    {
        $('#city').val(myCity);
        $('#lat').val(lat);
        $('#lng').val(lng);
        $("input[name=distance]").val(10);
        $("input[name=price]").val('0,100');
        $("input[name=page]").val(1);            
        $('.salons').prop('checked', true);
        $('.personal').prop('checked', true);
        sendInitalData();
    }
    function setSetWithNoLocation()
    {
        $("input[name=distance]").val(100);
        $("input[name=price]").val('0,100');
        $("input[name=page]").val(1);            
        $('.salons').prop('checked', true);
        $('.personal').prop('checked', true);
        sendInitalData();
    }
</script>
{{-- end user current address --}} 


{{-- get current data --}}
<script>
    function sendInitalData(){
        var lat = $('#lat').val();
        var lng = $('#lng').val();
        var city = $('#city').val();
        var title = $("input[name=service_title]").val(); 
        var category = $('#category').val();
        var subCategory =''; 
        $('.my-checkbox:checkbox:checked').each(function(i){
            subCategory +='sub_category%5B%5D='+$(this).val()+'&';
        });
        if(subCategory){
            subCategory = subCategory.substring(0, subCategory.length - 1);
        }else{
            subCategory = 'sub_category%5B%5D=';
        }
        var salons = '';
        var personal = '';
        if($('.salons').is(':checked')){
         salons = 1;
        };
        if($('.personal').is(':checked')){
         personal = 1;
        };
        var distance = $("input[name=distance]").val();
        var price = $("input[name=price]").val();
        var page = $("input[name=page]").val();

        link = 'lat='+ lat +'&lng='+lng+'&city='+city+'&title='+ title +'&salons='+salons+'&personal='+personal+'&distance='+distance+'&price='+price+'&category='+category+'&'+subCategory+'&page='+page;
        if(page == 1){
            indexData(link);
        }else{
            paginationData(link);
        }
        var current_url = window.location.href;
        if (current_url.indexOf("?") >= 0) {
            var current_url = current_url.split('?')[0];
        }
        var updateAbleUrl = current_url+'?'+link;
        window.history.pushState("object or string", "Title", updateAbleUrl);
    }
</script>    
{{-- end current data --}}   



{{-- pagination data --}}
 <script type="text/javascript">
    $(document).on('click','.pagination a',function(e){
        e.preventDefault();
        
        var current_url = window.location.href.split('page=')[0];
        var page_number = $(this).attr('href').split('page=')[1];
        $("input[name=page]").val(page_number);
        link = current_url+'page='+page_number;
        var cropLink = link.split('?')[1];
        if(page_number == 1){
            indexData(cropLink);
        }else{
            paginationData(cropLink);
        }
        $(window).scrollTop(0);
        window.history.pushState("object or string", "Title", link);
        
    });
 </script>    
{{-- end pagination data --}}

{{-- filter data --}}
<script type="text/javascript">
    function getServices( number)
    {

        if(number == 1)
        {
            var get_distance = $('.wrunner__valueNote').text();
            get_distance = get_distance.slice(0,-4);
            $("input[name=distance]").val(get_distance);
            if($('#lat').val() == ''){
                tryGetLocation();
                return false;
            }

        }
        if(number == 2){
          var min = $("#multi_range .wrunner__valueNote_display_visible").eq(0).text();
          var max = $("#multi_range .wrunner__valueNote_display_visible").eq(1).text();
          $("input[name=price]").val(min+','+max);
        }
        sendInitalData();
    }
</script>
{{-- end filter data --}}

{{-- if no location --}}
<script>
    function tryGetLocation()
    {
        if ("geolocation" in navigator){
            navigator.geolocation.getCurrentPosition(function(position){
                var currentLatitude = position.coords.latitude;
                var currentLongitude = position.coords.longitude;
                var geocoder;
                geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
                geocoder.geocode({'latLng': latlng}, function(results, status) {
                    if(status == google.maps.GeocoderStatus.OK) {
                        if(results[1]) {
                            var address = results[0].formatted_address;
                            var city = '';
                            for(var i = 0; i<results[0].address_components.length; i++){
                                for(var j = 0; j< results[0].address_components[i].types.length; j++){
                                    if(results[0].address_components[i].types[j] == 'administrative_area_level_2'){
                                        Mycity = results[0].address_components[i].long_name;
                                        Mycity = Mycity.split('District').join("");
                                        $('#city').val(Mycity);
                                        $('#lat').val(currentLatitude);
                                        $('#lng').val(currentLongitude);
                                        sendInitalData();
                                    }
                                }
                            }
                            
                        }
                    }
                }); 
            },function() {
                sendInitalData();
            });
        }  
    }
</script>



{{-- end if no location --}}







 <script type="text/javascript">
    function getSubCategory(category_id)
    {
        var data = new FormData();
        $('#category').val(category_id);
        data.append('category_id', category_id);
        $.ajax( {
            processData: false,
            contentType: false,
            data: data,
            type: 'POST',
            url:'{{url("/services/subcategory")}}',
            success: function( response ){
                $('.put-sub-category').html(response);
                getServices(0);
                console.log(response);
            }
                
        }); 
    }
 </script>


 <script>
    // $(document).ready(function(){
 //        if ("geolocation" in navigator){
 //            navigator.geolocation.getCurrentPosition(function(position){
 //                var currentLatitude = position.coords.latitude;
 //                var currentLongitude = position.coords.longitude;
 //                var geocoder;
 //                geocoder = new google.maps.Geocoder();
 //                var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
 //                geocoder.geocode({'latLng': latlng}, function(results, status) {
 //                    if(status == google.maps.GeocoderStatus.OK) {
 //                        if(results[1]) {
 //                            var address = results[0].formatted_address;
 //                            var city = '';
    //                      for(var i = 0; i<results[0].address_components.length; i++){
    //                          for(var j = 0; j< results[0].address_components[i].types.length; j++){
    //                              if(results[0].address_components[i].types[j] == 'administrative_area_level_2'){
    //                                  city = results[0].address_components[i].long_name;
    //                              }
    //                          }
    //                      }
    //                      city = city.split('District').join("");
    //                      var distance = 10;
    //                      var price = '0,100';

    //                      var url = 'lat='+ currentLatitude +'&lng='+currentLongitude+'&city='+city+'&distance='+ distance +'&price='+price+'&page=1';
    //                   //    $('.hir-tracker-thumb').css("width","10%");
    //                      // $('.tooltip').css("left","10%");
    //                      // $('.tooltip').text("10");

    //                      // $('.sl-bar').css({"width": "359px", "left": "0px"});
    //                      // $('.sl-bar2').css({"left": "346px"});
    //                      // $('.sl-bar3').css({"display": "none","left": "286.641px"});
    //                      // $('.sl-bar2').addClass("last-active");
    //                      // $('.price').val('0,100');
    //                      $('.sel-city').text(city);


    //                      var current_url = window.location.href;
    //                      if (current_url.toLowerCase().indexOf("page=") >= 0) {
    //                          var pageNumber = current_url.split('page=')[1];
    //                          if(pageNumber == 1){
    //                              indexData(url);
    //                          }else{
    //                              var url = 'lat='+currentLatitude+'&lng='+currentLongitude+'&city='+city+'&page='+pageNumber;
    //                              paginationData(url);
    //                          }
    //                      } else {
    //                          indexData(url);
    //                      }
                            
 //                            if(current_url.indexOf("?") >= 0){
 //                             var cut_url = current_url.split('?')[0];
 //                             var updateAbleUrl = cut_url+'?'+url;
                                
 //                            }else{
 //                             var updateAbleUrl = current_url+'?'+url;
 //                            }
 //                            $('#lat').val(currentLatitude);
 //                            $('#lng').val(currentLongitude);
 //                            $('#city').val(city);
 //                            window.history.pushState("object or string", "Title", updateAbleUrl);
 //                        }
 //                     } 
 //             });
 //         });
 //     }
    // });
</script>
<script type="text/javascript">
    function indexData(url)
    {
        $.ajax({
            url:'services/index-data?'+url

        }).done(function(response){
            $('#services').html(response);
            console.log(response);
        });
    }

    function paginationData(url)
    {
        $.ajax({
            url:'services/pagination-data?'+url
        }).done(function(data){
            $('#services').html(data)
        });
    }
</script>

<script type="text/javascript">
    function setCity(city){
        $('#city').val(city);
        getCityLatLng(city);
        //getServices();
    }
    function getCityLatLng(city)
    {
        geocoder = new google.maps.Geocoder();
        var address = city+' ,bangladesh';
        geocoder.geocode({'address': address}, function(results, status) {
            if(status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $('#lat').val(latitude);
                $('#lng').val(longitude);
                $('.backk').val(1);
                sendInitalData();
            }
        });
    }

</script>
<script>
    function scrollPosition()
    {
        var position = $(window).scrollTop();
        alert(position);
    }

</script>