<script>
    $(document).ready(function() {
        getUserLocation();
    });
</script>

<script>
    function getUserLocation() {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var currentLatitude = position.coords.latitude;
                var currentLongitude = position.coords.longitude;
                var geocoder;
                geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
                geocoder.geocode({
                    'latLng': latlng
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            var address = results[0].formatted_address;
                            var city = '';
                            for (var i = 0; i < results[0].address_components.length; i++) {
                                for (var j = 0; j < results[0].address_components[i].types.length; j++) {
                                    if (results[0].address_components[i].types[j] == 'administrative_area_level_2') {
                                        Mycity = results[0].address_components[i].long_name;
                                        Mycity = Mycity.split('District').join("");
                                        $('#city').val(Mycity);
                                        $('#lat').val(currentLatitude);
                                        $('#lng').val(currentLongitude);
                                        var link = 'lat=' + currentLatitude + '&lng=' + currentLongitude + '&city=' + Mycity + '&distance=' + 10;
                                        getVIPandProfile(link);
                                    }
                                }
                            }

                        }
                    }
                });
            }, function() {
                var link = 'lat=&lng=&city=&distance=' + 10;
                getVIPandProfile(link);
                setCity();
            });
        }
    }
</script>

<script>
    function getVIPandProfile(link) {
        $.ajax({
            url: 'index-vipdata?' + link

        }).done(function(response) {
            $('.vip-profile-service').html(response);
            // console.log(response);
        });
    }
</script>
<script>
    function setCity() {
        $.ajax({
            url: 'get-user-city'

        }).done(function(response) {
            $('#city').val(response);
        });
    }
</script>

<script type="text/javascript">
    function getSubCategory(category_id) {
        var data = new FormData();
        $('#category').val(category_id);
        data.append('category_id', category_id);
        $.ajax({
            processData: false,
            contentType: false,
            data: data,
            type: 'POST',
            url: '{{url("/services/subcategory")}}',
            success: function(response) {
                $('.put-sub-category').html(response);
            }

        });
    }
</script>
<script>
    function submitForm() {
        var lat = $('#lat').val();
        var lng = $('#lng').val();
        var city = $('#city').val();

        var title = $('.service_title').val();
        var category = $('#category').val();
        var subCategory = '';
        $('.my-checkbox:checkbox:checked').each(function(i) {
            subCategory += 'sub_category%5B%5D=' + $(this).val() + '&';
        });
        if (subCategory) {
            subCategory = subCategory.substring(0, subCategory.length - 1);
        } else {
            subCategory = 'sub_category%5B%5D=';
        }
        var salons = '';
        var personal = '';
        if ($('.salons').is(':checked')) {
            salons = 1;
        };
        if ($('.personal').is(':checked')) {
            personal = 1;
        };
        var page = 1;

        var get_distance = $('.wrunner__valueNote').text();
        var distance = get_distance.slice(0, -4);


        var min = $("#multi_range .wrunner__valueNote_display_visible").eq(0).text();
        var max = $("#multi_range .wrunner__valueNote_display_visible").eq(1).text();
        var price = min + ',' + max;
        var link = '?lat=' + lat + '&lng=' + lng + '&city=' + city + '&title=' + title + '&salons=' + salons + '&personal=' + personal + '&distance=' + distance + '&price=' + price + '&category=' + category + '&' + subCategory + '&page=' + page;
        var url = '{{url("services")}}' + link;
        window.location = url;
        return false;

    }
</script>
<script>
    function genereateLink(category, subCategory) {
        var lat = $('#lat').val();
        var lng = $('#lng').val();
        var city = $('#city').val();
        subCategory = 'sub_category%5B%5D=' + subCategory;
        var link = '?lat=' + lat + '&lng=' + lng + '&city=' + city + '&title=&salons=1&personal=1&distance=10&price=0,100&category=' + category + '&' + subCategory + '&page=1';
        var url = '{{url("services")}}' + link;
        window.location = url;

    }
</script>