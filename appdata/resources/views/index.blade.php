 @extends('layout.app')
 @section('style')
 <style>
 	.seeMore{
        color:#e25b37;
    }
    .dr-section .dr-m .dr {
	    position: relative;
	}
	.dr-section .dr-m .dr .dr-content {
	    
	    position: absolute;
	    z-index: 9;
	    background: black;
	    width: 100%;
	}
 </style>
 @endsection
 @section('content')
 @include('inc.index-filter')
 <section class="slider-section">
 	<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
 		<div class="carousel-inner">
 			<div class="carousel-item active">
 				<img src="{{asset('assets/img/slider1.jpg')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/slider2.jpg')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/slider3.jpg')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 				</div>
 			</div>
 		</div>
 		<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
 			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
 			<span class="sr-only">@lang('lang.previous')</span>
 		</a>
 		<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
 			<span class="carousel-control-next-icon" aria-hidden="true"></span>
 			<span class="sr-only">@lang('lang.next')</span>
 		</a>
 	</div>
 </section>
<div class="vip-profile-service">

</div>

<section class="bg-dark-sec"> {{-- vitor --}}
  <section class="dr-section">
 	<div class="section-title">@lang('lang.category_and_sub_category')</div>
 	<div class="dr-m">
 		<div class="row mar-0">
 			@if(count($categories) > 0)
 				@foreach ($categories as $category)
 					<div class="col-3 col-md-6 col-lg-4">
		 				<div class="dr">
		 					<div class="dr-l"> <span class="dr-ic1"><i class="fa fa-user"></i></span><span class="dr-txt"> {{ $category['name'] }}</span> <span class="dr-ic2"> <i class="fa fa-plus"></i> </span><span class="dr-ic3"> <i class="fa fa-minus"></i> </span></div>
		 					<div class="dr-content">
		 						@if(count($category['sub_categories']) > 0)
		 							@foreach ($category['sub_categories'] as $subCat)
		 								<p><a href="javascript:;" onclick="genereateLink('{{ $category["id"]}}','{{$subCat["id"]}}')">{{$subCat['name']}}</a></p>
		 							@endforeach
		 						@endif
		 					</div>
		 				</div>
		 			</div>
 				@endforeach
 			@endif
 		</div>
 	</div>
 </section>
 <section class="slider-section">
 	<div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
 		<div class="carousel-inner">
 			<div class="carousel-item active">
 				<img src="{{asset('assets/img/slider1.jpg')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/slider2.jpg')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 				</div>
 			</div>
 			<div class="carousel-item">
 				<img src="{{asset('assets/img/slider3.jpg')}}">
 				<div class="carousel-caption">
 					<h1>@lang('lang.third_slider_lavel')</h1>
 					<p>@lang('lang.praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur')</p>
 				</div>
 			</div>
 		</div>
 		<a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
 			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
 			<span class="sr-only">@lang('lang.previous')</span>
 		</a>
 		<a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
 			<span class="carousel-control-next-icon" aria-hidden="true"></span>
 			<span class="sr-only">@lang('lang.next')</span>
 		</a>
 	</div>
 </section>
</section>
 @endsection
 @section('script')
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyAnMJd489Qa_hRJXPon9VFHHFpGchq8Ib4"></script>

    @include("service_script.index-script");

 <script>
 	var swiper = new Swiper('.ctm-container3', {
 		slidesPerView: 5,
 		spaceBetween: 15,
 		loop: false,
 		navigation: {
 			nextEl: '.swiper-button-next',
 			prevEl: '.swiper-button-prev',
 		},

 		breakpoints: {
 			1400: {
 				slidesPerView: 4,
 			},
 			1000: {
 				slidesPerView: 3,
 			},
 			700: {
 				slidesPerView: 2,
 			},
 			450: {
 				slidesPerView: 1,
 			}
 		}
 	});
 </script>
 @endsection