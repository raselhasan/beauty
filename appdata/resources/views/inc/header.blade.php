<div class="header-section header1-section">
	<div class="main-menu">
		<span class="hs-menubar d-block d-lg-none">
			<div class="menu-trigger"> <i class="zmdi zmdi-menu"></i></div>
		</span>
		<nav class="navbar navbar-expand-lg navbar-light">
			<a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('assets/img/logo1.jpg')}}"></a>
			<div class="ss d-none d-md-block">
				<div class="search-show">
					<div class="input-group search-item">
						<input type="text" onclick="searchByTitle(event, this.value)" onkeyup="searchByTitle(event, this.value)" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
						<div class="input-group-append">
							<span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></span>
						</div>
					</div>
					<div class="ss-src-result d-none">
						<div class="src-rslt-con">sgsgsgs</div>
						<div class="src-rslt-con">sgsgsgs</div>
						<div class="src-rslt-con">sgsgsgs</div>
						<div class="src-rslt-con">sgsgsgs</div>
						<div class="src-rslt-con">sgsgsgs</div>
						<div class="src-rslt-con">sgsgsgs</div>
						<div class="src-rslt-con">sgsgsgs</div>
						<div class="src-rslt-con">sgsgsgs</div>
						<div class="src-rslt-con">sgsgsgs</div>
						<div class="src-rslt-con">sgsgsgs</div>
						<div class="src-rslt-con">sgsgsgs</div>
					</div>
				</div>
			</div>
			<!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class=""><i class="fas fa-bars"></i></span>
			</button> -->
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link active" href="{{url('/')}}">@lang('lang.home')</a>
					</li>
					<li class="nav-item">
						<a class="nav-link dot" href="#">@lang('lang.offers')</a>
					</li>
					<li class="nav-item">
						<a class="nav-link dot" href="{{url('profile-list')}}">@lang('lang.users')</a>
					</li>
					<li class="nav-item">
						<a class="nav-link dot" href="{{url('/user/calendar/create')}}">@lang('lang.sell_services')</a>
						<span class="count-l">2</span>
					</li>
					<li class="nav-item">
						<a class="nav-link dot" href="{{url('/user/booking-list')}}">@lang('lang.buy_services')</a>
						<span class="count-l">2</span>
					</li>
					<li class="nav-item">
						<div class="lan-flag">
							<div class="en-flag {{ (config('app.locale')=='en' ) ? '' : 'd-none' }}">
								<img src="{{ asset('assets/lan/en.png') }}" alt="lan flag" />
							</div>
							<div class="lt-flag {{ (config('app.locale')=='lt' ) ? '' : 'd-none' }}">
								<img src="{{ asset('assets/lan/lt.png') }}" alt="lan flag" />
							</div>
							<div class="rs-flag {{ (config('app.locale')=='rs' ) ? '' : 'd-none' }}">
								<img src="{{ asset('assets/lan/rs.png') }}" alt="lan flag" />
							</div>
							<div class="lan-pop-up d-none">
								<div class="lan-name" onclick="changeLanguage('en')">@lang('lang.english')</div>
								<div class="lan-name" onclick="changeLanguage('lt')">@lang('lang.lithuania')</div>
								<div class="lan-name" onclick="changeLanguage('rs')">@lang('lang.rashiya')</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="pro-cnf-area">
				<div class="usr-main-p {{ (Auth::guard('web')->check()) ? '' : 'd-none'}}">
					<a href="{{url('/user/profile')}}"><img class="cover" src="{{asset('assets/img/man.jpg')}}" alt="jglj"></a>
				</div>
				<div class="setting-btn {{ (Auth::guard('web')->check()) ? '' : 'd-none'}}">
					<i class="fas fa-cog"></i>
				</div>
			</div>
			@if(Auth::guard('web')->check())
			<div class="switch-acnt-box">
				<div class="acnt-list">
					<a href="{{url('/user/profile')}}">@lang('lang.profile')</a>
				</div>
				<div class="acnt-list">
					<a href="{{url('/user/calendar/create')}}">@lang('lang.create_calendar')</a>
				</div>
				<div class="acnt-list">
					<a href="{{url('payment-history')}}">@lang('lang.payment_history')</a>
				</div>
				<div class="acnt-list">
					<a href="{{url('/user/booking-list')}}">Booking List</a>
				</div>
				<div class="m-logout">
					<a href="{{ route('user.logout') }}" onclick="event.preventDefault();
					document.getElementById('logout-form').submit();">
						<button class="btn p-btn l-btn">@lang('lang.log_out')</button>
						<form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					</a>
				</div>
			</div>
			@else
			<div class="lg-btn">
				<span><button class="btn p-btn l-btn" data-toggle="modal" data-target="#loginPopUp">@lang('lang.log_in')</button> </span>
				<!-- <span><i class="fas fa-chevron-right"></i> </span> -->
				<!-- <span><button class="btn p-btn r-btn" data-toggle="modal" data-target="#registrationPopUp">@lang('lang.register')</button> </span> -->
			</div>
			@endif
		</nav>
	</div>
	<div class="mb-src-box">
		<div class="ss d-block d-md-none">
			<div class="search-show">
				<div class="input-group search-item">
					<input type="text" onclick="searchByTitle(event, this.value)" onkeyup="searchByTitle(event, this.value)" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
					<div class="input-group-append">
						<span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></span>
					</div>
				</div>
				<div class="ss-src-result d-none">
					<div class="src-rslt-con">sgsgsgs</div>
					<div class="src-rslt-con">sgsgsgs</div>
					<div class="src-rslt-con">sgsgsgs</div>
					<div class="src-rslt-con">sgsgsgs</div>
					<div class="src-rslt-con">sgsgsgs</div>
					<div class="src-rslt-con">sgsgsgs</div>
					<div class="src-rslt-con">sgsgsgs</div>
					<div class="src-rslt-con">sgsgsgs</div>
					<div class="src-rslt-con">sgsgsgs</div>
					<div class="src-rslt-con">sgsgsgs</div>
					<div class="src-rslt-con">sgsgsgs</div>
				</div>
			</div>
		</div>
	</div>

	<div class="multi-menu d-none d-lg-block">
		<div class="main-menu1">
			<nav class="navbar navbar-expand-lg navbar-light ">
				<button class="navbar-toggler" type="button">
					<span class=""><i class="fas fa-bars"></i></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						@php
						$menus = App\Category::get();
						@endphp
						@foreach($menus as $menu)
						<li class="nav-item show-sub-m">
							<a class="nav-link" href="#">{{ $menu->name }}</a>
							<div class="sub-menu sub-menu1">
								@php
								$n = 0;
								$k = $menu->subCategories->count() > 4 ? 4 : $menu->subCategories->count();
								@endphp
								@for($i=0; $i < $k; $i++) <div class="inner-menu">
									@for($j=0; $j < ceil($menu->subCategories->count() / 4); $j++)
										@if($menu->subCategories->count() >= $n + 1) <div class="link-all">
											<div class="inner-tlt"> <a href="">{{ $menu->subCategories[$n]->name }}</a></div>
											<div class="inner-link">
												@php
												$inrCats = $menu->subCategories[$n++]->innerCategories;
												@endphp
												@foreach($inrCats as $inrCat)
												<div><a href="">{{ $inrCat->name }}</a></div>
												@endforeach
											</div>
										</div>
										@endif
										@endfor
							</div>
							@endfor
				</div>
				</li>
				@endforeach
				</ul>
		</div>
		</nav>
	</div>
</div>

<nav class="hs-navigation">
	<ul class="nav-links">
		@php
		$menus = App\Category::get();
		@endphp
		@foreach($menus as $menu)
		<li class="{{ ($menu->subCategories->count()) ? 'has-child' : '' }}">
			<span class="{{ ($menu->subCategories->count()) ? 'its-parent' : '' }}">
				<a href="#">
					<span class="icon">
						<i class="zmdi zmdi-device-hub"></i>
					</span>
					{{ $menu->name }}
				</a>
			</span>
			@if($menu->subCategories->count())
			<ul class="its-children">
				@foreach($menu->subCategories as $subCat)
				<li class="{{ ($subCat->innerCategories->count()) ? 'has-child' : '' }}">
					<span class="{{ ($subCat->innerCategories->count()) ? 'its-parent' : '' }}">
						<a href="#1"> {{ $subCat->name }}</a>
					</span>
					@if($subCat->innerCategories->count())
					<ul class="its-children">
						@foreach($subCat->innerCategories as $innerCat)
						<li> <a href="#1">{{ $innerCat->name }} </a> </li>
						@endforeach
					</ul>
					@endif
				</li>
				@endforeach
			</ul>
			@endif
		</li>
		@endforeach
	</ul>
</nav>
</div>

<div class="search-box header-filter">
	<div class="form-section">
		<div class="form-all">
			<div class="input-group search-in">
				<input type="text" class="form-control" placeholder="Search" aria-label="Recipient's username" aria-describedby="basic-addon2">
				<div class="input-group-append">
					<button class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></button>
				</div>
			</div>
		</div>
	</div>
	<span class="s-close">&#10006;</span>
</div>