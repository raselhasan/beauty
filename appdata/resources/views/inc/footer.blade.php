<section class="footer-section">
	<div class="row mar-0">
		<div class="col-12 col-md-6 col-lg-4">
			<div class="d-con">
				<div class="f-logo"><img src="{{asset('assets/img/logo1.jpg')}}"></div>
				<div class="f-txt">@lang('lang.footer_text')</div>
				<div class="f-card">
					<img src="{{ asset('assets/img/pay-1.png') }}" alt="Left Arrow Key">
					<img src="{{ asset('assets/img/pay-2.png') }}" alt="Left Arrow Key">
					<img src="{{ asset('assets/img/pay-3.png') }}" alt="Left Arrow Key">
					<img src="{{ asset('assets/img/pay-4.png') }}" alt="Left Arrow Key">
					<img src="{{ asset('assets/img/pay-5.png') }}" alt="Left Arrow Key">
				</div>
			</div> 
		</div>
		<div class="col-12 col-md-6 col-lg-5">
			<div class="f-menu">
				<div class="f-menu1">
					<h1>@lang('lang.navigation')</h1>
					<ul>
						<li><a href="#">@lang('lang.home')</a></li>
						<li><a href="#">@lang('lang.pages')</a></li>
						<li><a href="#">@lang('lang.models')</a></li>
						<li><a href="#">@lang('lang.appointment')</a></li>
						<li><a href="#">@lang('lang.faq')</a></li>
						<li><a href="#">@lang('lang.contact')Cont</a></li>
					</ul>
				</div>
				<div class="f-menu1">
					<h1>@lang('lang.navigation')</h1>
					<ul>
						<li><a href="#">@lang('lang.home')</a></li>
						<li><a href="#">@lang('lang.pages')</a></li>
						<li><a href="#">@lang('lang.models')</a></li>
						<li><a href="#">@lang('lang.appointment')</a></li>
						<li><a href="#">@lang('lang.faq')</a></li>
						<li><a href="#">@lang('lang.contact')</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-6 col-lg-3">
			<div class="adv">
				<div class="single-content">
					<div class="content-img-p">
						<div class="content-img">
							<img src="{{asset('assets/img/man.jpg')}}">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>