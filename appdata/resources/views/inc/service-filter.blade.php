<form action="" method="GET" id="searchForm">
<input type="hidden" name="lat" id="lat" value="{{$item['lat']}}">
<input type="hidden" name="lng" id="lng" value="{{$item['lng']}}">
<input type="hidden" name="city" id="city" value="{{$item['city']}}">
<input type="hidden" name="category" id="category" value="{{$item['category']}}">
<input type="hidden" name="price" id="price" value="{{$item['price']}}">
<input type="hidden" name="distance" id="distance" value="{{$item['distance']}}">
<input type="hidden" name="page" id="page" value="{{$item['page']}}">
<input type="hidden" name="plg_assign" class="plg_assign" value="0">
<input type="hidden" name="backk" class="backk" value="{{$item['back']}}">
<section class="header-filter">
	<div class="form-section form-sh">
		<div class="form-all">
			<div class="row mar-0">
				<div class="col-12 col-md-6 col-lg-5 pad-0">
					<div class="input-group search-in">
						<input type="text" class="form-control service_title" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2" name="service_title" onkeyup="getServices(0)" value="{{$item['title']}}">
						<div class="input-group-append">
							<span class="input-group-text" id="basic-addon2"><i class="fas fa-search"></i></span>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-2 pad-0">
					<div class="input-group">
						<div class="ctm-select">
							<div class="ctm-select-txt pad-l-10">
								<span class="select-txt noselect" id="category">
									{{($item['category']) ? $item['category'] : 'choose...'}}
								</span>
								<span class="select-arr"><i class="fas fa-caret-down"></i></span>
							</div>
							<div class="ctm-option-box">
								@if(count($service['categories'])>0)
									@foreach ($service['categories'] as $cat)
										<div class="ctm-option noselect" onclick="getSubCategory({{$cat->id}})">{{ $cat->name }}</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-2 pad-0">
					<div class="form-group">
						<div class="ctm-select">
							<div class="ctm-select-txt pad-l-10">
								<span class="select-txt noselect" id="category">@lang('lang.choose')</span>
								<span class="select-arr"><i class="fas fa-caret-down"></i></span>
							</div>
							<div class="ctm-option-box put-sub-category">
								{!! $item['sub_category'] !!}
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-2 pad-0">
					<div class="input-group">
						<div class="ctm-select">
							<div class="ctm-select-txt pad-l-10">
								<span class="select-txt noselect sel-city" id="category">
									{{($item['city']) ? $item['city'] : 'choose...'}}
								</span>
								<span class="select-arr"><i class="fas fa-caret-down"></i></span>
							</div>
							<div class="ctm-option-box noselect">
								@if(count($service['cities'])>0)
									@foreach ($service['cities'] as $city)
										<div class="ctm-option noselect" onclick="setCity('{{ $city->name }}')">{{ $city->name }}</div>
									@endforeach
								@endif
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-1 pad-0">
					<div class="form-group mar-0">
						<div class="form-check">
							<input class="form-check-input salons" type="checkbox" id="gridCheck" name="salons" value="1" @if($item['salons'] ==1) checked @endif onclick="getServices(0)">
							<label class="form-check-label" for="gridCheck">
								@lang('lang.salons')
							</label>
						</div><div class="form-check">
							<input class="form-check-input personal" type="checkbox" id="gridCheck" name="personal" value="1"  @if($item['personal'] ==1) checked @endif onclick="getServices(0)">
							<label class="form-check-label" for="gridCheck">
								@lang('lang.personal')
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-range">
			<div class="row mar-0">
				<div class="col-12 col-md-6 col-lg-6 r-d">
					<span class="range-tlt">@lang('lang.atstuumas')</span>
					{{-- <div class="range-s">
						<input type="range" id="html-input-range" onchange="getServices(1)">
					</div> --}}
					<div class="w-100" id="single_range" onclick="getServices(1)"></div>
					<!-- <div class="demo-output dm">
						<input class="range-slider r1"  type="hidden" value="0,0"/>
					</div> -->
				</div>
				<div class="col-12 col-md-6 col-lg-4 r-d">
					{{-- <div class="demo-output dm1">
						<input class="range-slider get-price r2" type="hidden" value="0,100" onchange="getServices(2)">
					</div> --}}
					<div class="w-100" id="multi_range" onclick="getServices(2)"></div>
				</div>
				<div class="col-12 col-md-6 col-lg-2 r-d">
					<div class="ran-bar">
						<span class=""> <a href="{{url('services')}}">@lang('lang.reset_filter')</a> </span> 
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="line-h"><span><i class="fas fa-chevron-down"></i></span></div>
</section>
</form>