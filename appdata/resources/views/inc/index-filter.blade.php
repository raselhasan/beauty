<form action="" method="GET" id="searchForm" onsubmit="return submitForm()">
	<input type="hidden" name="lat" id="lat" value="">
	<input type="hidden" name="lng" id="lng" value="">
	<input type="hidden" name="city" id="city" value="">
	<input type="hidden" name="category" id="category" value="">
	<input type="hidden" name="price" id="price" value="0,100">
	<input type="hidden" name="distance" id="distance" value="10">
	<input type="hidden" name="plg_assign" class="plg_assign" value="0">
	<section class="header-filter">
		<div class="form-section form-sh">
			<div class="form-all">
				<div class="row mar-0">
					<div class="col-12 col-md-12 col-lg-9 pad-0">
						<span class="range-tlt d-block d-md-none">@lang('lang.atstuumas')</span>
						<div class="form-range">
							<div class="row mar-0">
								<div class="col-12 col-md-6 col-lg-6 r-d">
									<span class="range-tlt d-none d-md-block">@lang('lang.atstuumas')</span>
									{{-- <div class="range-s">
											<input type="range" id="html-input-range" onchange="getServices(1)">
									</div> --}}
									<div class="w-100" id="single_range" onclick="getServices(1)"></div>
									<!-- <div class="demo-output dm">
										<input class="range-slider r1"  type="hidden" value="0,0"/>
									</div> -->
								</div>
								<div class="col-12 col-md-6 col-lg-4 r-d">
									{{-- <div class="demo-output dm1">
						<input class="range-slider get-price r2" type="hidden" value="0,100" onchange="getServices(2)">
					</div> --}}
									<div class="w-100" id="multi_range" onclick="getServices(2)"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-lg-2 pad-0">
						<div class="input-group">
							<div class="ctm-select">
								<div class="ctm-select-txt pad-l-10">
									<span class="select-txt noselect sel-city" id="category">
										@lang('lang.choose')
									</span>
									<span class="select-arr"><i class="fas fa-caret-down"></i></span>
								</div>
								<div class="ctm-option-box noselect">
									@if(count($cities)>0)
									@foreach ($cities as $city)
									<div class="ctm-option noselect" onclick="setCity('{{ $city['name'] }}')">{{ $city['name'] }}</div>
									@endforeach
									@endif
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-lg-1 pad-0">
						<div class="form-group mar-0">
							<div class="form-check">
								<input class="form-check-input salons" type="checkbox" id="gridCheck" name="salons" value="1" checked>
								<label class="form-check-label" for="gridCheck">
									@lang('lang.salons')
								</label>
							</div>
							<div class="form-check">
								<input class="form-check-input personal" type="checkbox" id="gridCheck" name="personal" value="1" checked>
								<label class="form-check-label" for="gridCheck">
									@lang('lang.personal')
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="line-h"><span><i class="fas fa-chevron-down"></i></span></div>
	</section>
</form>