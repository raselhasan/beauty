<!DOCTYPE html>
<html lang="en">

<head>
	<title>New</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="verify-paysera" content="ff297396eb1864fcd1b755985cd44ac7">
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('assets/fontawesome/css/fontawesome-all.min.css') }}">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('assets/swiper/css/swiper.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/ionicons/css/ionicons.min.css') }}">
	{{-- <link rel="stylesheet" href="{{ asset('assets/range/html-input-range.css') }}"> --}}
	{{-- <link rel="stylesheet" href="{{ asset('assets/range/jquery.range.css') }}"> --}}
	<link rel="stylesheet" href="{{ asset('assets/range/wrunner-default-theme.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/summernote/summernote.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/mobile-menu/css/hs-menu.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/mobile-menu/fonts/css/material-design-iconic-font.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/calendar.css') }}">
	<style>
		.wrunner_theme_default.wrunner_direction_horizontal {
			position: relative;
			top: -8px;
		}

		.wrunner__pathPassed_theme_default.wrunner__pathPassed {
			border-radius: 0;
			background: #E25B37;
		}

		.wrunner__handle_theme_default.wrunner__handle {
			border-radius: 0;
			background-image: linear-gradient(to bottom, #eeeeee, #dddddd);
			background-repeat: repeat-x;
		}

		.wrunner__handle_theme_default.wrunner__handle:hover {
			border-radius: 0;
		}

		.wrunner__path_theme_default.wrunner__path {
			background-color: #fff;
			border-radius: 0;
		}

		.wrunner__valueNote_theme_default.wrunner__valueNote {
			background-color: #E25B37;
		}

		.acc-type {
			margin-top: 21px;
		}

		#language-box {
			margin-top: 5px;
			background: #0c0c0c;
			color: white;
		}
	</style>
	@yield('style')
</head>

<body>
	<div id="sckbr_p">
		<div id="snackbar">Some text some message..</div>
	</div>
	@include('inc.registration')
	@include('inc.login')
	@include('inc.header')

	<div id="calendar">
		@yield('content')
	</div>
	@include('inc.footer')
	@yield('wondowv')
	<script src="{{ asset('assets/js/calendar.js') }}"></script>
	<!-- <script src="{{ asset('assets/js/popper.min.js') }}"></script> -->
	<!-- <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script> -->
	<script src="{{ asset('assets/ionicons/js/ionicons.js') }}"></script>
	<script src="{{ asset('assets/swiper/js/swiper.js') }}"></script>
	{{-- <script src="{{ asset('assets/range/html-input-range.js') }}"></script> --}}
	{{-- <script src="{{ asset('assets/range/jquery.range.js') }}"></script> --}}
	<script src="{{ asset('assets/summernote/summernote.min.js') }}"></script>
	<script src="{{ asset('assets/range/wrunner-jquery.js') }}"></script>
	<script src="{{ asset('assets/mobile-menu/js/jquery.hsmenu.js') }}"></script>
	<script src="{{ asset('assets/js/custom.js') }}"></script>

	<script>
		function changeLanguage(lang) {
			var url = '{{url("localization")}}/' + lang;
			window.location = url;
		}


		// $(function(){
		// 	htmlInputRange.options({
		// 		tooltip: true,
		// 		max: 100,
		// 		labels: true,
		// 		labelsData: {
		// 			one: 'one',
		// 			two: 'two'
		// 		}
		// 	});
		// 	$('.single-slider').jRange({
		// 		from: 0,
		// 		to: 100,
		// 		step: 1,
		// 		format: '%s',
		// 		showLabels: false,
		// 		snap: true
		// 	});

		// 	$('.range-slider').jRange({
		// 		from: 0,
		// 		to: 100,
		// 		step: 1,
		// 		format: '%s',
		// 		showLabels: false,
		// 		isRange : true,
		// 		values: [ 10, 20 ]
		// 	});

		// });
		window.login = "{{ route('user.login.submit') }}";
		window.registration = "{{ route('registration') }}";
		window.profile = "{{ route('user.profile') }}";
	</script>
	@if(session()->has('errorL'))
	<script>
		$(function() {
			$('[data-target="#loginPopUp"]').click();
		});
	</script>
	@endif
	@yield('script')
</body>

</html>