/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");
import Popper from "popper.js";
window.Popper = Popper;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
import VueCal from "vue-cal";
import "vue-cal/dist/i18n/zh-cn.js";
import "vue-cal/dist/vuecal.css";
import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/index.css";
import VueSlideUpDown from "vue-slide-up-down";
import moment from "moment-timezone";

moment.tz.setDefault("Europe/Vilnius");

Vue.prototype.$moment = moment;
Vue.prototype.$user_id = window.c_user_id;
Vue.prototype.$user = window.c_user;
Vue.prototype.$service = window.service;
Vue.prototype.$base_url = window.base_url;
Vue.prototype.$s_id = window.s_id;
Vue.prototype.$getData = async function(type, url, data = null) {
    let rdata = null;
    if (type == "get") {
        try {
            await axios.get(url).then(response => (rdata = response.data));
        } catch (errors) {
            console.log(errors);
        }
    } else if (type == "post") {
        try {
            await axios
                .post(url, data)
                .then(response => (rdata = response.data));
        } catch (errors) {
            console.log(errors);
        }
    } else if (type == "put") {
        try {
            await axios
                .put(url, data)
                .then(response => (rdata = response.data));
        } catch (errors) {
            console.log(errors);
        }
    } else {
        try {
            await axios.delete(url).then(response => (rdata = response.data));
        } catch (errors) {
            console.log(errors);
        }
    }
    return rdata;
};

Vue.use(VueToast, {
    // One of options
    position: "top"
});

Vue.component("vue-slide-up-down", VueSlideUpDown);
Vue.component("vue-cal", VueCal);
Vue.component(
    "view-calendar",
    require("./components/viewCalendar/ViewCalendar.vue").default
);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#calendar"
});
