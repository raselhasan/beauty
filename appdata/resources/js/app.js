/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
// import Vue from "vue";

import { ClientTable } from "vue-tables-2";
import VueSlideUpDown from "vue-slide-up-down";
import VueSession from "vue-session";
import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/index.css";
import VCalendar from "v-calendar";
import moment from "moment-timezone";
import VueCal from "vue-cal";
import "vue-cal/dist/i18n/zh-cn.js";
import "vue-cal/dist/vuecal.css";

moment.tz.setDefault("Europe/Vilnius");

Vue.prototype.$moment = moment;
Vue.prototype.$csrf_token = window.csrf_token;
Vue.prototype.$base_url = window.base_url;
Vue.prototype.$user_id = window.user_id;
Vue.prototype.$user = window.user;
Vue.prototype.$addWorkerDay = function(d) {
    this.$root.$emit("addWorkerDay", d);
};
Vue.prototype.$getData = async function(type, url, data = null) {
    let rdata = null;
    if (type == "get") {
        try {
            await axios.get(url).then(response => (rdata = response.data));
        } catch (errors) {
            console.log(errors);
        }
    } else if (type == "post") {
        try {
            await axios
                .post(url, data)
                .then(response => (rdata = response.data));
        } catch (errors) {
            console.log(errors);
        }
    } else if (type == "put") {
        try {
            await axios
                .put(url, data)
                .then(response => (rdata = response.data));
        } catch (errors) {
            console.log(errors);
        }
    } else {
        try {
            await axios.delete(url).then(response => (rdata = response.data));
        } catch (errors) {
            console.log(errors);
        }
    }
    return rdata;
};

Vue.use(ClientTable);
Vue.use(VueSession);
Vue.use(VueToast, {
    // One of options
    position: "top"
});
Vue.use(VCalendar, {
    componentPrefix: "vc"
});
Vue.component("vue-cal", VueCal);
Vue.component("vue-slide-up-down", VueSlideUpDown);

Vue.component(
    "create-calendar",
    require("./components/createCalendar/CreateCalendar.vue").default
);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import router from "./routes/routes.js";
const app = new Vue({
    el: "#app",
    router
});
