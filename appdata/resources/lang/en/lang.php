<?php
return [
  //header-blade	
  'home' => 'Home',
  'services' => 'Services',
  'sell_services' => 'Sell Services',
  'buy_services' => 'buy Services',
  'offers' => 'Offers',
  'users' => 'Users',
  'models' => 'Models',
  'appointment' => 'Appointment',
  'faq' => 'FAQ',
  'contact' => 'Contact',
  'english' => 'English',
  'lithuania' => 'Lithuania',
  'rashiya' => 'Rashiya',
  'profile' => 'Profile',
  'create_calendar' => 'Create Calendar',
  'payment_history' => 'Payment-history',
  'log_out' => 'Log out',
  'log_in' => 'Log in',
  'register' => 'Register',

  //header-filter
  'volksagen' => 'volksagen',
  'men' => 'men',

  //login
  'login' => 'Login',
  'login_with_facebook' => 'Login with Facebook',
  'login_with_google' => 'Login with Google',
  'password' => 'Password',

  //Registration
  'registration' => 'Registration',
  'nickname' => 'Nickname',
  'company_name' => 'Company name',

  //service-filter
  'reset_filter' => 'Reset filter',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',









  //index
  'third_slider_lavel' => 'Third slide label',
  'praesent_commodo_cursus_magna_vel scelerisque_nisl_consectetur' => 'Praesent commodo cursus magna, vel scelerisque nisl consectetur',
  'previous' => 'Previous',
  'next' => 'Next',
  'category_and_sub_category' => 'CATEGORY AND SUB CATEGORY',
  'pages' => 'Pages',

  //index-filter
  'salons' => 'Salons',
  'personal' => 'Personal',
  'atstuumas' => 'atstuumas',
  'search' => 'Search',












  //footer
  'footer_text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make',
  'navigation' => 'Navigation',


  // about model
  'cetagory' => 'Cetagory',
  'sub_cetagory' => 'Sub Cetagory',
  'inner_cetagory' => 'Inner Cetagory',
  'available_apointment' => 'Available Apointment:',
  'unavailable' => 'Unavailable',
  'book_available' => 'Book available',
  'time1' => '8:00am-8:00pm',
  'time2' => '0 time slot available',
  'send' => 'Send',
  'clear' => 'Clear',
  'populer_masters' => 'Populer Masters',
  'demo1' => 'Lorem Ipsum is simply dummy text of the printing to make',
  'replay_comment' => 'Replay Comment',
  'cancel' => 'Canel',

  //add control
  'add_controll_payment' => 'Add controll payment',
  'choose_duration' => 'choose duration',
  'paysera' => 'Paysera',
  'card_payment' => 'Card Payment',
  'pay_now' => 'Pay now',

  //blog
  'palaugos_salia_taves' => 'palaugos salia taves',
  'see_more' => 'See More',
  'visus_ancitos' => 'Visus ancitos',

  //comment
  'replay' => 'Replay',

  //my-service
  'active_service' => 'Active Service',
  'choose' => 'choose...',
  'address' => 'Address',
  'more_show' => 'More show',
  'comments' => 'Comments',
  'jhon' => 'Jhon',
  'time3' => '03-11-2019 at 10:00am',
  'edit' => 'Edit',
  'type' => 'Type',
  'price' => 'Price',
  'populer_masters' => 'Populer Masters',
  'update_your_profile_info' => 'Update your profile info',
  'title' => 'Title',
  'description' => 'Description',
  'save' => 'Save',
  'add_gallery_tab' => 'Add Gallery Tab',
  'tab_name' => 'Tab Name',
  'add' => 'Add',
  'service_image' => 'Service Image',
  'upload_service_image' => 'Upload Service Image',
  'update' => 'Update',
  'gallery_image' => 'Gallery Image',
  'add_gallery_image' => 'Add Gallery Image',
  'crop' => 'Crop',

  //payment-history
  'service_payment' => 'Service Payment',
  'profile_payment' => 'Profile Payment',
  'add_controll_payment' => 'Add controll paymentAdd controll payment',
  'you_have_no_payment_history' => 'You have no payment history',

  //payment-list
  'paid_services' => 'Paid Services',
  'update_service' => 'Update Service',

  //public-profile
  'share' => 'Share',
  'bio' => 'Bio',
  'live_zona' => 'Live Zona',
  'lorem_ipsum' => 'Lorem Ipsum',
  '' => '',

  //select-plan
  'select_service' => 'Select service',
  'checkout' => 'Checkout',
  'your_order' => 'Your order',
  'select_payment_method' => 'Select payment method',
  'active_without_payment' => 'Active without Payment',

  //services
  'see_more' => 'See more',

  //user-profile
  'add_live_zone' => 'Add Live Zone',
  'gallery' => 'Gallery',
  'add_picture' => 'Add picture',
  'my_services' => 'My Services',
  'add_my_services' => 'Add my services',
  'crop_the_image' => 'Crop the image',
  'profile_picture' => 'Profile Picture',
  'upload_profile_picture' => 'Upload Profile Picture',
  'update_your_picture' => 'update your picture',
  'add_image' => 'Add Image',
  'embade_a_video' => 'Embade a Video',
  'embade_link' => 'Embade Link',
  'upload_image' => 'Upload Image',
  'update_your_profile_info' => 'Update your profile info',
  'account_type' => 'Account Type',
  'personal' => 'Personal',
  'name' => 'Name',
  'usurname' => 'Usurname',
  'nick_name' => 'Nick Name',
  'email' => 'Email',
  'phone' => 'Phone',
  'show_on_profile' => 'Show on profile',
  'social_info' => 'Social Info',
  'or' => 'or',
  'facebook_link' => 'Facebook-link',
  'twitter_link' => 'Twitter-link',
  'gmail_link' => 'Gmail-link',
  'linkedin_link' => 'Linkedin-link',
  'pinterest_link' => 'Pinterest-link',
  'short_bio' => 'Short Bio',
  'update_your_cover_photos' => 'Update your cover photos',
  'cover_photos' => 'Cover Photos',
  'upload_cover_photos' => 'Upload Cover Photos',
  'make_your_profile_vip' => 'Make your profile VIP',
  'payment_method' => 'Payment Method',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',

  //vip-profile
  '' => '',
  'vip_profile' => 'VIP PROFILES',
  'vip_services' => 'VIP SERVICES',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',
  '' => '',







];
