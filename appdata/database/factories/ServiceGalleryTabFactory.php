<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ServiceGalleryTab;
use Faker\Generator as Faker;

$factory->define(ServiceGalleryTab::class, function (Faker $faker) {
    return [
        'tab' => $faker->word,
    ];
});
