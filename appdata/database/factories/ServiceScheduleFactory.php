<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ServiceSchedule;
use Faker\Generator as Faker;

$factory->define(ServiceSchedule::class, function (Faker $faker) {
    return [
        'start' => date('H:i:s', rand(1, 26000)),
        'end' => date('H:i:s', rand(1, 26000)),
    ];
});
