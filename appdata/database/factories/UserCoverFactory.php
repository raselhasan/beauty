<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UserCover;
use Faker\Generator as Faker;

$factory->define(UserCover::class, function (Faker $faker) {
    return [
    	'img' => 'demo.jpg',
    	'status' => $faker->randomElement([1, 0]),
    ];
});
