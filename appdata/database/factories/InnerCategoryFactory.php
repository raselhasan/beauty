<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\InnerCategory;
use Faker\Generator as Faker;

$factory->define(InnerCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->word
    ];
});
