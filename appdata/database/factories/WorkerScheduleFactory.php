<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\WorkerSchedule;
use Faker\Generator as Faker;

$factory->define(WorkerSchedule::class, function (Faker $faker) {
    return [
        'start' => date('H:i:s', rand(1, 26000)),
        'end' => date('H:i:s', rand(26000, 52000)),
    ];
});
