<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $name = $faker->firstname;
    return [
        'name' => $faker->name,
        'surname' => $name,
        'nickname' => $faker->lastname,
        'email' => $faker->unique()->safeEmail,
        'address' => $faker->address,
        'phone' => $faker->e164PhoneNumber,
        'facebook' => 'https://www.facebook.com/' . $name,
        'twitter' => 'https://www.twitter.com/' . $name,
        'gmail' => 'https://www.google.com/' . $name,
        'linkedin' => 'https://www.linkedin.com/' . $name,
        'pinterest' => 'https://www.pinterest.com/' . $name,
        'bio' => $faker->paragraph(rand(2, 5)),
        'profile_image' => 'demo.jpg',
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
