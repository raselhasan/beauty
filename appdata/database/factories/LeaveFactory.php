<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Leave;
use Faker\Generator as Faker;

$factory->define(Leave::class, function (Faker $faker) {
	$start_date = '2020-01-01 00:00:00';
    $end_date = '2022-12-31 00:00:00';

    $min = strtotime($start_date);
    $max = strtotime($end_date);

    $val = rand($min, $max);
    $weeks = rand(1, 52);

    $start = new DateTime(date('Y-m-d', $val));
    return [
        'date' => $start,
        'start' => date('H:i:s', rand(1, 26000)),
        'end' => date('H:i:s', rand(26000, 52000)),
    ];
});
