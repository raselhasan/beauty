<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_galleries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('img', 100);
            $table->unsignedBigInteger('service_gallery_tab_id')->nullable();
            $table->timestamps();

            $table->foreign('service_gallery_tab_id')->references('id')->on('service_gallery_tabs')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_galleries');
    }
}
