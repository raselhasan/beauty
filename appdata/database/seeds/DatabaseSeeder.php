<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $category = factory(App\Category::class, 25)->create()->each(function ($category) {

            $sub_categories = factory(App\SubCategory::class, rand(0, 7))->make();

            $category->subCategories()->saveMany($sub_categories);

            $sub_categories->each(function ($subCat) {

                $inner_categories = factory(App\InnerCategory::class, rand(1, 3));

                $subCat->innerCategories()->saveMany($inner_categories);
            });
        });

        // factory(App\User::class, 20)->create()->each(function($user){

        // 	$user_services = factory(App\Service::class, rand(2,5))->make();

        // 	$user_covers = factory(App\UserCover::class, rand(2,5))->make();

        // 	$user_galleries = factory(App\Gallery::class, rand(3,6))->make();

        // 	$user_workers = factory(App\Worker::class, rand(5,10))->make();

        // 	$user->userCovers()->saveMany($user_covers);

        // 	$user->galleries()->saveMany($user_galleries);

        // 	$user->workers()->saveMany($user_workers);

        // 	$user->services()->saveMany($user_services);

        //     $user_workers->each(function($woker){

        //         $leaves = factory(App\Leave::class, rand(0,2))->make();

        //         $working_days = factory(App\WorkingDay::class, rand(4,6))->make();

        //         $woker->leaves()->saveMany($leaves);

        //         $woker->workingDays()->saveMany($working_days);

        //         $working_days->each(function($days){

        //             $worker_schedules = factory(App\WorkerSchedule::class, rand(1,3));

        //             $days->workerSchedules()->saveMany($worker_schedules);
        //         });
        //     });

        //     $user_services->each(function($service) use($user_workers) {

        //         $faker = \Faker\Factory::create();

        //         $types = factory(App\Type::class, rand(2,6))->make();

        //         $off_days = factory(App\OffDay::class, rand(0,3))->make();

        //         $service_gallery_tabs = factory(App\ServiceGalleryTab::class, 
        //             rand(3,6))->make();

        //         $service->types()->saveMany($types);

        //         $service->types()->saveMany($off_days);

        //         $service->serviceGalleryTabs()->saveMany($service_gallery_tabs);

        //         $service_gallery_tabs->each(function($service_gallery_tab){

        //             $service_galleries = factory(App\ServiceGallery::class, rand(3, 6))->make();

        //             $service_gallery_tab->serviceGalleries()->saveMany($service_galleries);
        //         });

        //         $service_schedules = factory(App\ServiceSchedule::class, rand(1,3))->make([
        //             'start' => date('H:i:s', rand(1, 26000)),
        //             'end' => date('H:i:s', rand(1, 26000)),
        //             'type_id' => $faker->randomElement($service->types->pluck('id')->toArray()),
        //             'worker_id' => $faker->randomElement($service->user->workers->pluck('id')->toArray()),
        //         ]);

        //         $service->serviceSchedules()->saveMany($service_schedules);

        //         $user_workers->each(function ($user_worker) use ($types) {
        //             $user_worker->types()->attach(
        //                 $types->random(rand(1, count($types)))->pluck('id')->toArray()
        //             );
        //         });
        //     });
        // });

    }
}
