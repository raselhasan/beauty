<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceSchedule extends Model
{
	protected $guarded = [];
	
    public function type()
	{
		return $this->belongsTo(Type::class);
	}

	public function worker()
	{
		return $this->belongsTo(Worker::class);
	}

	public function service()
	{
		return $this->belongsTo(Service::class);
	}
}
