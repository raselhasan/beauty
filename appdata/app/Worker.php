<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{
	protected $guarded = [];
	
	protected $hidden = ['pivot'];
	
	public function leaves()
	{
		return $this->hasMany(Leave::class);
	}

	public function serviceSchedules()
	{
		return $this->hasMany(ServiceSchedule::class);
	}

	public function user()
    {
        return $this->belongsTo(User::class);
    }

	public function workingDays()
	{
		return $this->hasMany(WorkingDay::class);
	}

	public function types()
	{
		return $this->belongsToMany(Type::class);
	}

	public function bookings()
	{
		return $this->hasMany(Booking::class);
	}
}
