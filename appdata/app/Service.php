<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $guarded = [];

	public function servicePayment()
	{
		return $this->hasMany(PaymentService::class);
	}
	public function payment()
	{
		return $this->hasMany(Payment::class);
	}

	public function booking()
	{
		return $this->hasMany(Booking::class);
	}

	public function offDays()
	{
		return $this->hasMany(OffDay::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function category()
	{
		return $this->belongsTo(Category::class);
	}
	public function subCategory()
	{
		return $this->belongsTo(SubCategory::class);
	}

	public function serviceGalleryTabs()
	{
		return $this->hasMany(ServiceGalleryTab::class);
	}

	public function types()
	{
		return $this->hasMany(Type::class);
	}

	public function serviceSchedules()
	{
		return $this->hasMany(ServiceSchedule::class);
	}
	public function singleUser()
	{
		return $this->hasMany(User::class);
	}
}
