<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomPackage extends Model
{
    protected $guarded = [];
    
    public function types()
    {
        return $this->belongsToMany(Type::class);
    }
}
