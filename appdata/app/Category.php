<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	public function services()
	{
		return $this->hasMany(Service::class);
	}

	public function subCategories()
	{
		return $this->hasMany(SubCategory::class);
	}

	public function innerCategories()
	{
		return $this->hasMany(InnerCategory::class);
	}

	public function hasSubCategory($id)
	{
		return $this->subCategories->contains($id);;
	}
}
