<?php

namespace App\Http\Controllers\User;

use Auth;
use Session;
use App\Type;
use App\User;
use App\Booking;
use App\Gallery;
use App\Service;
use App\LiveZone;
use App\UserCover;
use Carbon\Carbon;
use App\VipPackage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function index()
    {
        $folder_path = 'userImage/tmpPic/' . Auth::user()->id;
        $files = glob($folder_path . '/*');
        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }
        Session::forget('user_profile_img');

        $data['gallery'] = Gallery::orderBy('id', 'DESC')
            ->where('user_id', Auth::user()->id)
            ->get();
        $data['UserCoverA'] = UserCover::orderBy('id', 'DESC')
            ->where('user_id', Auth::user()->id)
            ->get();
        $data['UserCover'] = UserCover::orderBy('id', 'DESC')
            ->where('status', 1)
            ->where('user_id', Auth::user()->id)
            ->get();
        $data['services'] = Service::orderBy('id', 'DESC')
            ->where('user_id', Auth::user()->id)
            ->get();
        // dd($data['UserCoverA']);
        $data['userInfo'] = User::find(Auth()->user()->id);
        //dd($data['userInfo']->toArray());
        $show_name = json_decode($data['userInfo']->show_name);
        $makeName = '';
        foreach ($show_name as $name) {
            if ($name == 1) {
                $makeName .= $data['userInfo']->name . ' ';
            }
            if ($name == 2) {
                $makeName .= $data['userInfo']->surname . ' ';
            }
            if ($name == 3) {
                if ($makeName == '') {
                    $makeName = $data['userInfo']->nickname;
                } else {
                    $makeName .= '(' . $data['userInfo']->nickname . ')';
                }
            }
        }
        $data['makeName'] = trim($makeName);
        $data['userInfo'] = User::where('id', Auth()->user()->id)->with('ProfilePayment')->first()->toArray();
        $data['profilePackages'] = VipPackage::all();
        $data['liveZones'] = LiveZone::where('user_id', Auth()->user()->id)->orderBy('id','DESC')->get();
        // $data['liveZones'] = [];
        return view('pages.user-profile', $data);
    }

    public function galleryFileUp(Request $request)
    {
        $html = '';
        for ($x = 0; $x < $request->total; $x++) {

            if ($request->hasFile('pic' . $x)) {
                $file = $request->file('pic' . $x);
                $img_i = getimagesize($file);
                if ($img_i[0] > 400) {
                    $width = 400;
                    $height = ($img_i[1] * 400) / $img_i[0];
                } else {
                    $width = $img_i[0];
                    $height = $img_i[1];
                }
                $file_name = time() . '_' . $x . '.' . $file->getClientOriginalExtension();
                $file->move('gallery', $file_name);
                $imager = new \Imager('gallery/' . $file_name);
                $imager->resize($width, $height)->write('gallery/re_' . $file_name);
                $data  = new Gallery;
                $data->picture = $file_name;
                $data->user_id = Auth::user()->id;
                $data->save();

                $html = '<div class="col-6 col-sm-4 col-md-3 pad-0">
                <span class="fa fa-times remveImg" onclick="removeImg(event, \'' . $data->id . '\')"></span>
                <a href="' . asset('gallery/' . $file_name) . '" data-fancybox="gallery">
                <div class="single-content">
                <div class="content-img-p">
                <div class="content-img">
                <img src="' . asset('gallery/re_' . $file_name) . '" alt="img">
                </div>
                </div>
                </div>
                </a>
                </div>' . $html;
            }
        }
        return $html;
    }
    public function galleryRemover(Request $request)
    {
        $gallery = Gallery::find($request->id);
        $path = 'gallery/' . $gallery->picture;
        $path2 = 'gallery/re_' . $gallery->picture;
        if ($gallery->picture) {
            if (file_exists($path)) {
                unlink($path);
            }
            if (file_exists($path2)) {
                unlink($path2);
            }
        }
        $gallery->delete();
    }

    public function setProfilePic(Request $request)
    {
        $file = $request->file('avatar');
        if (Session::has('user_profile_img')) {
            $name = Session::get('user_profile_img');
        } else {
            $name = 'profile_pic' . time() . '.jpg';
        }
        Session::put('user_profile_img', $name);
        $file->move('userImage/tmpPic/' . Auth::user()->id, $name);
        $image = Session::get('user_profile_img');
        $html = '';
        $html .= '<span class="pip">';
        $html .= '<img class="imageThumb" src="' . asset('userImage/tmpPic/' . Auth::user()->id . '/' . $image) . '">';
        $html .= '<br/>';
        $html .= '<span class="remove" id="' . $image . '">✖</span>';
        return $html;
    }

    public function removeProfilePic(Request $request)
    {
        $image_name = $request->img_name;
        Session::forget('user_profile_img');
        $path = $path = 'userImage/tmpPic/' . Auth::user()->id . '/' . $image_name;
        unlink($path);
    }

    public function upPrPic(Request $request)
    {
        $user_pic = User::find(Auth::user()->id)->profile_image;
        if ($user_pic) {
            $path =  'userImage/fixPic/' . $user_pic;
            unlink($path);
        }
        if (Session::has('user_profile_img')) {
            $img = Session::get('user_profile_img');
        } else {
            $img = '';
        }
        Session::forget('user_profile_img');
        if ($img) {
            $path =  'userImage/tmpPic/' . Auth::user()->id . '/' . $img;
            $npath = 'userImage/fixPic/' . $img;
            copy($path, $npath);
            unlink($path);
        }

        User::where('id', Auth::user()->id)->update([
            'profile_image' => $img
        ]);
        return back();
    }
    public function coverImgUpload(Request $req)
    {
        $file = $req->file('avatar');
        $name = 'cover_img' . time() . '.jpg';
        $file->move('userImage/coverPic', $name);

        $user_cover = new UserCover;
        $user_cover->user_id = Auth::user()->id;
        $user_cover->img = $name;
        $user_cover->img = $name;
        $user_cover->save();

        $html = '<div class="col-6 col-sm-3 pad-l-0">
        <span  class="rmv-p" onclick="deleteCoverPic(event, ' . $user_cover->id . ')">&#10006;</span>
        <label class="image-checkbox">
        <img class="cover" src="' . asset('userImage/coverPic/' . $name) . '" />
        <input type="checkbox" name="cover_img_d[]" value="' . $user_cover->id . '" />
        <i class="fa fa-check d-none"></i>
        </label>
        </div>';
        return $html;
    }
    public function coverImgUpdate(Request $req)
    {
        // dd($req->all());
        $user_cover = UserCover::where('status', 1)
            ->update([
                'status' => 0
            ]);
        $user_cover_u = UserCover::whereIn('id', $req->cover_img_d)
            ->update([
                'status' => 1
            ]);
        return back();
    }
    public function userCoverRemover(Request $request)
    {
        $user_cover = UserCover::find($request->id);
        $path = 'userImage/coverPic/' . $user_cover->img;
        if ($user_cover->img) {
            if (file_exists($path)) {
                unlink($path);
            }
        }
        $user_cover->delete();
    }
    public function getProfileInfo(Request $req)
    {
        $userD = User::find(Auth::user()->id);
        return $userD;
    }
    public function insertProfileInfo(Request $req)
    {
        $user = User::find(Auth::user()->id);
        $user->name = $req->name;
        $user->surname = $req->surname;
        $user->nickname = $req->nickname;
        $user->email = $req->email;
        $user->phone = $req->phone;
        $user->address = $req->address;
        $user->facebook = $req->facebook;
        $user->twitter = $req->twitter;
        $user->gmail = $req->gmail;
        $user->linkedin = $req->linkedin;
        $user->pinterest = $req->pinterest;
        $user->user_lat = $req->lat;
        $user->user_lng = $req->long;
        $user->user_city = trim($req->city);
        $user->bio = $req->bio;
        $user->show_name = json_encode($req->show_name, true);
        $user->account_type = $req->account_type;
        $user->save();
        return $user;
    }

    public function addLiveZone(Request $request)
    {
        $liveZone = new LiveZone();
        $liveZone->user_id = Auth()->user()->id;
        $liveZone->title = $request->live_zone_title;
        $liveZone->description = $request->live_zone_des;
        $status = $request->live_zone_status;

        if ($status == 1) {
            if ($request->hasFile('live_zone_img')) {
                $file = $request->file('live_zone_img');
                $file_name = time() . '.' . $file->getClientOriginalExtension();
                $file->move('liveZone', $file_name);
                $liveZone->image = $file_name;
                $liveZone->status = 1;
            }
        } else if ($status == 2) {
            $liveZone->image = $request->live_zone_vido_link;
            $liveZone->status = 2;
        } else {
            $liveZone->status = 0;
        }
        $liveZone->save();
        $liveZone = LiveZone::find($liveZone->id);
        return view('pages.live-zone-data', compact('liveZone'));
    }

    public function editLiveZone(Request $request)
    {
        $liveZone = LiveZone::find($request->id);
        return $liveZone;
    }
    public function liveZoneDelete(Request $request)
    {
        $live_zone = LiveZone::find($request->id);
        if($live_zone->status == 1){
            \File::delete('liveZone/'.$live_zone->image);
        }
        $live_zone->delete();
        return 'success';
    }

    public function liveZoneUpdate(Request $request)
    {
        $liveZone = LiveZone::find($request->live_zone_id);
        if ($request->live_zone_title) {
            $liveZone->title = $request->live_zone_title;
        }
        if ($request->live_zone_des) {
            $liveZone->description = $request->live_zone_des;
        }
        $status = $request->live_zone_status;

        if ($status == 1) {
            if ($request->hasFile('live_zone_img')) {
                $file = $request->file('live_zone_img');
                $file_name = time() . '.' . $file->getClientOriginalExtension();
                $file->move('liveZone', $file_name);
                $liveZone->image = $file_name;
                $liveZone->status = 1;
            }
        } else if ($status == 2) {
            if ($request->live_zone_vido_link) {
                $liveZone->image = $request->live_zone_vido_link;
                $liveZone->status = 2;
            }
        }
        $liveZone->save();
        $liveZone = LiveZone::find($request->live_zone_id);
        return $liveZone;
    }
    public function bookingList()
    {
        $data['booking'] = Booking::where('user_id', Auth::user()->id)->orderBy('id',  'DESC')->get();
        // dd($data['booking']->toArray());
        return view('pages.booking-list', $data);
    }
    public function cancledBooking(Service $service, Type $type, Booking $booking)
    {
        $booking->cancle = 1;
        $booking->confirm = 0;
        $booking->save();
        $bookingAll = $type->booking()->with(['user', 'worker'])->get();
        return redirect()->back();
    }
}
