<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class CalendarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    public function index()
    {
        $data['api_token'] = Session::get('api_token');
        return view('calendar.create', $data);
    }
    public function getSession()
    {
        $api_token = Session::get('api_token');
        return response()->json(['api_token'=>$api_token]);
    }
}
