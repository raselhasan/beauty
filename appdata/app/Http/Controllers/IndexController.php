<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use App\Service;
use App\Category;
use App\SubCategory;
use App\ServiceGalleryTab;
use App\District;
use App\Gallery;
use App\UserCover;
use App\User;
use App\VipPackage;
use App\Comment;
use App\CommentReplay;
use Session;
use App\LiveZone;

class IndexController extends Controller
{
    public function index()
    {
        $item['categories'] = Category::with('subCategories')->get()->toArray();
        $item['cities'] = District::all()->toArray();
        //dd($item['categories']);
        return view('index', $item);
    }
    public function blog()
    {
        $services = Service::with('user')->Paginate(5);
        //dd($services);
        return view('pages.blog', compact('services'));
    }
    public function aboutM($id)
    {

        $comments = Comment::where('service_id', $id)->with('CommentReplay.user', 'user')->orderBy('id', 'DESC')->get()->toArray();
        //dd($comments);

        $service = Service::where('id', $id)->with('user', 'types', 'serviceGalleryTabs.serviceGalleries')->first()->toArray();

        $mainComment = Comment::where('service_id', $id)->withCount('CommentReplay')->get()->toArray();
        $replayComment = array_sum(array_column($mainComment, 'comment_replay_count'));
        $total_comment = count($mainComment) + $replayComment;

        $this->saveRecentlyViewed($id);
        return view('pages.about-model', compact('service', 'comments', 'total_comment'));
    }

    public function ServiceComments($id)
    {
    }

    public function saveRecentlyViewed($id)
    {
        if (Session::has('recently_viewed')) {
            $viewed = Session::get('recently_viewed');
            if (!in_array($id, $viewed)) {
                array_push($viewed, $id);
                session()->put('recently_viewed', $viewed);
                session()->save();
            }
        } else {
            $viewed = array($id);
            session()->put('recently_viewed', $viewed);
            session()->save();
        }
    }
    public function userProfile()
    {
        $userInfo = User::find(Auth::user()->id);
        dd(userInfo);
        return view('pages.user-profile');
    }
    public function selectPlan($id)
    {
        $package['package1'] = Package::where('type', 1)->with('duration')->first();
        $package['package2'] = Package::where('type', 2)->with('duration', 'star')->first();
        $package['package3'] = Package::where('type', 3)->with('duration')->first();
        //dd($package['package2']->toArray());
        $package['service'] = Service::find($id);
        return view('pages.select-plan', compact('package', 'id'));
    }

    public function publicProfile($id)
    {
        $data['id'] = $id;
        $data['gallery'] = Gallery::orderBy('id', 'DESC')
            ->where('user_id', $id)
            ->get();
        $data['UserCoverA'] = UserCover::orderBy('id', 'DESC')
            ->where('user_id', $id)
            ->get();
        $data['UserCover'] = UserCover::orderBy('id', 'DESC')
            ->where('status', 1)
            ->where('user_id', $id)
            ->get();
        $data['services'] = Service::orderBy('id', 'DESC')
            ->where('user_id', $id)
            ->get();
        // dd($data['UserCoverA']);
        $data['userInfo'] = User::find($id);
        //dd($data['userInfo']->toArray());
        $show_name = json_decode($data['userInfo']->show_name);
        $makeName = '';
        foreach ($show_name as $name) {
            if ($name == 1) {
                $makeName .= $data['userInfo']->name . ' ';
            }
            if ($name == 2) {
                $makeName .= $data['userInfo']->surname . ' ';
            }
            if ($name == 3) {
                if ($makeName == '') {
                    $makeName = $data['userInfo']->nickname;
                } else {
                    $makeName .= '(' . $data['userInfo']->nickname . ')';
                }
            }
        }
        $data['makeName'] = trim($makeName);
        $data['userInfo'] = User::where('id', $id)->with('ProfilePayment')->first()->toArray();
        $data['profilePackages'] = VipPackage::all();

        $data['liveZones'] = LiveZone::where('user_id',$id)->orderBy('id','DESC')->get();
        

        return view('pages.public-profile', $data);
    }
    public function getSubCategory($id)
    {
        return SubCategory::find($id);
    }
    public function publicService($id, $title, $s_id)
    {
        $data['id'] = $id;
        $data['s_id'] = $s_id;
        $data['service'] = Service::where('id', $s_id)
            ->with('category', 'serviceGalleryTabs.serviceGalleries', 'types', 'user')
            ->get();
        $data['service'] = $data['service']->map(function ($service) {
            $arr = [];
            $sub_cat_id = json_decode($service->sub_category);
            if ($sub_cat_id) {
                foreach ($sub_cat_id as $key => $id) {
                    $arr[$key] = $this->getSubCategory($id);
                }
            }
            $service['takeSubCategory'] = $arr;
            return $service;
        })->first()->toArray();
        $data['categories'] = Category::all();
        $data['sub_categories'] = SubCategory::where('category_id', $data['service']['category'])->get();
        $data['service_glry_tab'] = ServiceGalleryTab::where('service_id', $s_id)->get();
        // dd($data['service']['user']['facebook']);
        return view('pages.service', $data);
    }
    public function getServiceGlryTab(Request $req)
    {
        $srv_glry_tab = ServiceGalleryTab::where('service_id', $req->service_id)->get();
        return $srv_glry_tab;
    }
    public function test()
    {
        $earthRadiusMiles = 3959;
        $lat = '23.810332';
        $lng = '90.4125181';
        $distance = 5; //max distance in km


        $today = date('Y-m-d H:i:s');
        $top_services = \DB::table('services')->selectRaw("
            services.*,top_services.*, ( $earthRadiusMiles * acos( cos( radians($lat) ) * cos( radians( services.lat ) ) 
            * cos( radians( services.lng ) - radians($lng) ) + sin( radians($lat) ) *
            sin(radians(services.lat)) ) ) AS distance, lat, lng
        ")
            ->join('top_services', 'services.id', '=', 'top_services.service_id')
            ->where('services.city', 'dhaka')
            ->whereDate('top_services.end_date', '>=', $today)
            ->havingRaw("distance < $distance")
            ->inRandomOrder()
            ->limit(2)
            ->get();

        $top_services = $top_services->sortByDesc("star");
        //$services = $a->merge($b);

        //$services = $services->paginate(5);

        dd($top_services->toArray());
    }
}
