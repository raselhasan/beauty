<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Validator;
use App\Helpers;

use App\User;
use App\Service;
use App\Category;


class ServiceController extends ApiController
{
	public function index($user)
	{
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data, 'service');

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		return $this->sendResponse($user_data->services, 'All Services for this user');
	}

	public function show($id)
	{
		$service = Service::find($id);

		if (!$service) {
			return $this->sendError('Data not found', [], 404);
		}

		$message = 'Service for id: ' . $id;

		return $this->sendResponse($service, $message);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'description' => 'required',
		]);


		if($validator->fails()){
			return $this->sendError('Validation Error.', $validator->errors(), 409);       
		}

		$category = Category::find($request->category_id);
		
		if (! Category::where('id', $request->category_id)->exists()) {
			return $this->sendError('Category not found.', [], 404); 
		}

		foreach (json_decode($request->sub_category) as $subcat) {
			if(!$category->hasSubCategory($subcat)){
				return $this->sendError('Sub Category does not exist for this Category', ['id' => $subcat], 406); 
			}
		}

		$service_exists = Service::where('user_id', auth()->user()->id)->where('title', $request->title)->exists();

		if ($service_exists) {
			return $this->sendError('Service already exists for this user', ['user_id' => auth()->user()->id], 404);
		}
		
		$request['user_id'] = auth()->user()->id;

		$service = Service::create($request->all());

		$success = [
			'service' => $service,
		];

		return $this->sendResponse($success, 'Service created successfully.');
	}
}
