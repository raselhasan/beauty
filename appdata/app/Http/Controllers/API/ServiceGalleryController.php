<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Validator;
use App\Helpers;
use App\Service;
use App\ServiceGalleryTab;

class ServiceGalleryController extends ApiController
{
    public function serviceTabs($service)
    {
    	$validation = Helpers::checkUser($service);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['service_id' => $service], $validation['code']);
    	}

    	$service_data = Service::find($service);

    	if (!$service_data->serviceGalleryTabs()->exists()) {
    		return $this->sendError('Gallery tab not found for this service', ['service_id' => $service], 404);
    	}
    	
    	return $this->sendResponse($service_data->serviceGalleryTabs, 'Tabs for this service');
    }

    public function serviceTabStore(Request$request, $service)
    {
    	$req_validation = Validator::make($request->all(), [
			'tab' => 'required',
		]);

		if($req_validation->fails()){
			return $this->sendError('Validation Error.', $req_validation->errors(), 409);       
		}

		$validation = Helpers::checkUser($service);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
    	}
    	
    	$tab_exists = ServiceGalleryTab::where('tab', $request->tab)->where('service_id', $service)->exists();

    	if ($tab_exists) {
    		return $this->sendError('Type already exists for this service', ['service_id' => $service], 406);
    	}

    	$request['service_id'] = $service;

    	$tab = ServiceGalleryTab::create($request->all());

    	return $this->sendResponse($tab, 'Tab Created for this service');
    }

    public function serviceTabById($service, $tab)
    {
    	$validation = Helpers::checkUser($service);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['service_id' => $service], $validation['code']);
    	}

    	$service_data = Service::find($service);

    	if (!$service_data->serviceGalleryTabs()->exists()) {
    		return $this->sendError('No Gallery tab found for this service', ['service_id' => $service], 404);
    	}

    	if (!$service_data->serviceGalleryTabs->contains($tab)) {
    		return $this->sendError('This gallery tab not found for this service', ['service_id' => $service], 404);
    	}

    	return $this->sendResponse($service_data->serviceGalleryTabs()->where('id', $tab)->first(), 'Tab for this service by id');
    }

    public function serviceTabGallery($service, $tab)
    {
    	$validation = Helpers::checkUser($service);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['service_id' => $service], $validation['code']);
    	}

    	$service_data = Service::find($service);

    	if (!$service_data->serviceGalleryTabs()->exists()) {
    		return $this->sendError('No Gallery tab found for this service', ['service_id' => $service], 404);
    	}

    	if (!$service_data->serviceGalleryTabs->contains($tab)) {
    		return $this->sendError('This gallery tab not found for this service', ['service_id' => $service], 404);
    	}

    	$gallery_tab = $service_data->serviceGalleryTabs()->where('id', $tab)->first();

    	if (!$gallery_tab->serviceGalleries()->exists()) {
    		return $this->sendError('No Gallery found for this service gallery tab', ['service_id' => $service, 'tab_id' => $tab], 404);
    	}
    	
    	return $this->sendResponse($gallery_tab->serviceGalleries, 'Tabs for this service');
    }

    public function serviceTabGalleryById($service, $tab, $gallery)
    {
    	$validation = Helpers::checkUser($service);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['service_id' => $service], $validation['code']);
    	}

    	$service_data = Service::find($service);

    	if (!$service_data->serviceGalleryTabs()->exists()) {
    		return $this->sendError('No Gallery tab found for this service', ['service_id' => $service], 404);
    	}

    	if (!$service_data->serviceGalleryTabs->contains($tab)) {
    		return $this->sendError('This gallery tab not found for this service', ['service_id' => $service], 404);
    	}

    	$gallery_tab = $service_data->serviceGalleryTabs()->where('id', $tab)->first();

    	if (!$gallery_tab->serviceGalleries()->exists()) {
    		return $this->sendError('No Gallery found for this service gallery tab', ['service_id' => $service, 'tab_id' => $tab], 404);
    	}
    	
    	if (!$gallery_tab->serviceGalleries->contains($gallery)) {
    		return $this->sendError('This gallery tab not found for this service gallery tab', ['service_id' => $service, 'tab_id' => $tab, 'gallery_id' => $gallery], 404);
    	}
    	return $this->sendResponse($gallery_tab->serviceGalleries->where('id', $gallery)->first(), 'Tabs for this service');
    }
}
