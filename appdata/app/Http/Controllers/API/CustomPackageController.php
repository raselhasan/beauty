<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\CustomPackage;
use App\Http\Controllers\ApiController;
use App\Service;
use Validator;
use App\Helpers;

class CustomPackageController extends ApiController
{
    public function index($service)
    {
		$validation = Helpers::checkUser($service);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }
        
        $customPackageData = CustomPackage::where('user_id', auth()->id())->with('types')->get();

        return $this->sendResponse($customPackageData, 'Custom Package Created');
    }

    public function create(Request $request, $service)
    {
        $types = gettype($request->types) == 'string' ? json_decode($request->types) : $request->types;
        
        $req_validation = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'time' => 'required|numeric',
        ]);

        if($req_validation->fails()){
            return $this->sendError('Validation Error.', $req_validation->errors(), 409);       
        }
		$validation = Helpers::checkUser($service);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }
        
        $customPackage = CustomPackage::create([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price,
            'time' => $request->time,
            'user_id' => auth()->id(),
        ]);

        $service = Service::find($service);
        foreach($types as $id){
            $exists = $service->types->contains($id);
            if($service && $exists){
                $customPackage->types()->attach($id);
            }
        }

        return $this->sendResponse($customPackage, 'Package Created Successful.');
    }

    public function update(Request $request, $service, $custom)
    {
        $types = gettype($request->types) == 'string' ? json_decode($request->types) : $request->types;
        
        $req_validation = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'time' => 'required|numeric',
        ]);

        if($req_validation->fails()){
            return $this->sendError('Validation Error.', $req_validation->errors(), 409);       
        }
		$validation = Helpers::checkUser($service);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $customPackageData = CustomPackage::where('user_id', auth()->id())->where('id', $custom)->with('types')->get()->first();

        if (empty($customPackageData)) {
            return $this->sendError('Custom package not found',[
                'package_id' => $custom
            ],404);
        }
        $customPackageData->fill($request->all());

        if (!$customPackageData->isDirty()) {
            return $this->sendError('Must need to change data to update',[
                'package_id' => $custom
            ],406);
        }
        $customPackageData->save();

        return $this->sendResponse($customPackageData, 'Package Created Successful.');
    }

    public function show($service, $custom)
    {
		$validation = Helpers::checkUser($service);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }
        
        $customPackageData = CustomPackage::where('user_id', auth()->id())->where('id', $custom)->with('types')->get()->first();

        if (empty($customPackageData)) {
            return $this->sendError('Custom package not found',[
                'package_id' => $custom
            ],404);
        }
        return $this->sendResponse($customPackageData, 'Custom package data');
    }

    public function delete($service, $custom)
    {
		$validation = Helpers::checkUser($service);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }
        
        $customPackageData = CustomPackage::where('user_id', auth()->id())->where('id', $custom)->with('types')->get()->first();

        if (empty($customPackageData)) {
            return $this->sendError('Custom package not found',[
                'package_id' => $custom
            ],404);
        }
        
        $customPackageData->delete();
        
        return $this->sendResponse($customPackageData, 'Custom Package removed.');
    }

    public function typedelete($service, $custom, $type = null)
    {
		$validation = Helpers::checkUser($service);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }
        
        $customPackageData = CustomPackage::where('user_id', auth()->id())->where('id', $custom)->with('types')->get()->first();

        if (empty($customPackageData)) {
            return $this->sendError('Custom package not found',[
                'package_id' => $custom
            ],404);
        }
        if ($type) {
            if($customPackageData->types->contains($type)){
                $customPackageData->types()->detach($type);
                return $this->sendResponse($customPackageData->types, 'Type removed from this Custom Package ');
            }
            return $this->sendError('Package Type does not exists.',[
                'package_id' => $custom,
                'type_id' => $type
            ],404);
        }

        $customPackageData->types()->detach();
        
        return $this->sendResponse($customPackageData->types, 'All Types removed from this Custom Package ');
    }
}
