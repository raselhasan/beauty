<?php

namespace App\Http\Controllers\API;

use App\Type;
use Validator;
use App\Worker;
use App\Booking;
use App\Helpers;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\TypeWorkerResponse;

class ServiceTypeController extends ApiController
{
    public function types($service)
    {
        // $validation = Helpers::checkUser($service);

        // if ($validation) {
        // 	return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        // }

        $types = Service::find($service)->types->sortByDesc('id')->values();

        if (count($types) == 0) {
            return $this->sendError('No types available for this service', ['id' => $service], 404);
        }

        return $this->sendResponse($types, 'Types for services id : ' . $service);
    }

    public function type($service, $type)
    {
        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found.', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }
        return $this->sendResponse($type_data, 'Type for services id : ' . $service);
    }

    public function typeStore(Request $request, $service)
    {
        $req_validation = Validator::make($request->all(), [
            'name' => 'required|regex:/(^([a-zA-z ]+)(\d+)?$)/u',
            'price' => 'required|numeric',
            'time' => 'required|numeric',
        ]);

        if ($req_validation->fails()) {
            return $this->sendError('Validation Error.', $req_validation->errors(), 409);
        }

        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $type_exists = Type::where('name', $request->name)->where('service_id', $service)->exists();

        if ($type_exists) {
            return $this->sendError('Type already exists for this service', ['service_id' => $service], 406);
        }

        $request['service_id'] = $service;

        $type = Type::create($request->all());

        return $this->sendResponse($type, 'Service created successfully.');
    }

    public function typeDelete($service, $type)
    {
        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }

        $type_data->delete();

        return $this->sendResponse($type_data, 'Type Deleted');
    }

    public function typeUpdate(Request $request, $service, $type)
    {
        $req_validation = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'time' => 'required|numeric',
        ]);

        if ($req_validation->fails()) {
            return $this->sendError('Validation Error.', $req_validation->errors(), 409);
        }

        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }

        $type_data->fill($request->all());

        if (!$type_data->isDirty()) {
            return $this->sendError('Must need to change any value to update', $request->all(), 406);
        }

        $type_data->save();
        return $this->sendResponse($type_data, 'Type Successfully Updated.');
    }

    public function typeGetWorkers($service, $type)
    {
        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }

        $response = Type::where("id", $type)
            ->with(
                [
                    'workers.workingDays',
                    'workers.leaves',
                    'workers.bookings',
                    'workers.types' => function ($q) use ($service) {
                        $q->where('service_id', $service);
                    }
                ]
            )
            ->get()
            ->first();
        // dd($response);
        return $this->sendResponse(new TypeWorkerResponse($response), 'Workers for this Type');
    }
    public function storeBooking($service, $type, Request $request)
    {
        $service_data = Service::find($service);

        if (!$service_data) {
            return $this->sendError('Service not found.', ['id' => $service], 404);
        }

        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found.', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }
        $check_booking = Booking::where('user_id', $request->user_id)
            ->where('date', $request->date)
            ->whereTime('start', '<=', $request->start)
            ->whereTime('end', '>', $request->start)
            ->exists();

        if ($check_booking) {
            return $this->sendError(
                'In this time pereod you already book another service.',
                [
                    'date' => $request->date,
                    'start' => $request->start,
                    'end' => $request->end
                ],
                406
            );
        }
        $request['service_id'] = $service;
        $request['type_id'] = $type;
        $book = Booking::create($request->all());
        return $this->sendResponse($book, 'Booking successfull.');
    }
    public function getBooking(Service $service, Type $type)
    {
        $booking = $type->booking()->with(['user', 'worker'])->get();
        return $this->sendResponse($booking, 'Booking Data Found.');
    }
    public function getSingleBooking(Service $service, Type $type, Request $request)
    {
        $booking = $service->booking()
            ->with(['user', 'worker'])
            ->where('worker_id', $request->worker_id)
            ->where('date', $request->date)
            ->where('start', $request->start . ":00")
            ->first();
        return $this->sendResponse($booking, 'Booking Data Found.');
    }
    public function cancledBooking(Service $service, Type $type, Booking $booking)
    {
        $booking->cancle = 1;
        $booking->confirm = 0;
        $booking->save();
        $bookingAll = $type->booking()->with(['user', 'worker'])->get();
        return $this->sendResponse($bookingAll, 'Booking Cancled Successfully.');
    }
    public function confirmedBooking(Service $service, Type $type, Booking $booking)
    {
        $booking->cancle = 0;
        $booking->confirm = 1;
        $booking->save();
        $bookingAll = $type->booking()->with(['user', 'worker'])->get();
        return $this->sendResponse($bookingAll, 'Booking Confirmed Successfully.');
    }
    public function typeWorkers($service, $type)
    {
        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }

        return $this->sendResponse($type_data->workers, 'Workers for this Type');
    }

    public function typeWorkersAdd(Request $request, $service, $type)
    {
        $ids = $request->ids;

        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }
        foreach ($ids as $id) {
            $worker = Worker::find($id);
            if ($worker && !$type_data->workers()->where('worker_id', $id)->exists()) {
                $type_data->workers()->attach($worker);
            }
        }
        $type_data->save();

        return $this->sendResponse($type_data->workers, 'Workers added for this Type.');
    }

    public function typeWorkersDetach($service, $type, $worker)
    {


        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }

        $worker_data = Worker::find($worker);
        if ($worker_data && $type_data->workers()->where('worker_id', $worker)->exists()) {
            $type_data->workers()->detach($worker_data);
        }

        $type_data->save();

        return $this->sendResponse($type_data->workers, 'Workers added for this Type.');
    }

    public function typeWorkersById($service, $type, $worker)
    {
        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }

        if (!$type_data->workers->contains($worker)) {
            return $this->sendError('This worker does not exists for this service', [
                'service_id' => $service,
                'worker_id' => $worker,
            ], 404);
        }

        $worker_data = Worker::find($worker);

        if ($worker_data->user_id != auth()->user()->id) {
            return $this->sendError('You are not own this worker', [
                'worker_id' => $worker,
                'user_id' => auth()->user()->id,
            ], 404);
        }

        return $this->sendResponse($worker_data, 'Worker for this service');
    }

    public function typeWorkersStore(Request $request, $service, $type)
    {
        $req_validation = Validator::make($request->all(), [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email|unique:workers',
            'phone' => 'required|numeric',
        ]);

        if ($req_validation->fails()) {
            return $this->sendError('Validation Error.', $req_validation->errors(), 409);
        }

        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }

        $request['user_id'] = auth()->user()->id;

        $worker_data = Worker::create($request->all());

        $type_data->workers()->attach($worker_data->id);

        $type_data->save();

        return $this->sendResponse($worker_data, 'Worker created for this user');
    }

    public function typeWorkersUpdate(Request $request, $service, $type, $worker)
    {
        $req_validation = Validator::make($request->all(), [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email|unique:workers',
            'phone' => 'required|numeric',
        ]);

        if ($req_validation->fails()) {
            return $this->sendError('Validation Error.', $req_validation->errors(), 409);
        }

        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }

        $request['user_id'] = auth()->user()->id;

        $worker_data = Worker::find($worker);

        $worker_data->fill($request->all());

        if (!$worker_data->isDirty()) {
            return $this->sendError('Must need to change any data to update', [
                'type_id' => $type,
                'worker_id' => $worker,
                'service_id' => $service
            ], 406);
        }

        $worker_data->save();

        return $this->sendResponse($worker_data, 'Worker created for this user');
    }

    public function typeWorkersDelete($service, $type, $worker)
    {
        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $type_data = Type::find($type);

        if (!$type_data) {
            return $this->sendError('Type not found', ['id' => $type], 404);
        }

        if ($type_data->service_id != $service) {
            return $this->sendError('This type not found for this service', [
                'type_id' => $type,
                'service_id' => $service,
            ], 404);
        }

        $request['user_id'] = auth()->user()->id;

        $worker_data = Worker::find($worker);

        if (!$worker_data) {
            return $this->sendError('Worker does not exists', [
                'type_id' => $type,
                'worker_id' => $worker,
                'service_id' => $service
            ], 404);
        }

        $worker_data->delete();

        return $this->sendResponse($worker_data, 'Worker deleted');
    }
}
