<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Validator;

use App\Helpers;
use App\Service;
use App\Type;
use App\ServiceSchedule;

class ServiceScheduleController extends ApiController
{
	public function schedules($service)
	{
		$validation = Helpers::checkUser($service);

		if ($validation) {
			return $this->sendError($validation['message'], ['service_id' => $service], $validation['code']);
		}

		return $this->sendResponse(Service::find($service)->serviceSchedules, 'Schedules for this service');
	}

	public function schedulesById($service, $schedule)
	{
		$validation = Helpers::checkUser($service);

		if ($validation) {
			return $this->sendError($validation['message'], ['service_id' => $service], $validation['code']);
		}

		if (!Service::find($service)->serviceSchedules->contains($schedule)) {
			return $this->sendError('This Schedule not found for this Service', ['service_id' => $service, 'schedule_id' => $schedule], 404);
		}

		return $this->sendResponse(ServiceSchedule::find($schedule), 'Schedule for this service');
	}

	public function schedulesStore(Request $request, $service)
	{
		$req_validation = Validator::make($request->all(), [
			'start' => 'required|date_format:H:i',
			'end' => 'required|date_format:H:i|after:start',
			'type_id' => 'required',
			'worker_id' => 'required',
		]);

		if($req_validation->fails()){
			return $this->sendError('Validation Error.', $req_validation->errors(), 409);       
		}

		$validation = Helpers::checkUser($service);

		if ($validation) {
			return $this->sendError($validation['message'], ['service_id' => $service], $validation['code']);
		}

		if (!Type::where('service_id', $service)->where('id', $request->type_id)->exists()) {
			return $this->sendError('this service does not have this type', ['type_id' => $type, 'service_id' => $service], 404);
		}

		if (!auth()->user()->workers->contains($request->worker_id)) {
			return $this->sendError('You dont have this worker.', ['worker_id' => $request->worker_id], 404);
		}

		if (ServiceSchedule::where([
			['start', $request->start],
			['end', $request->end],
			['worker_id', $request->worker_id],
			['type_id', $request->type_id],
			['service_id', $service]
		])->exists()) {
			return $this->sendError('This Schedule already exists for this service', [
				'type_id' => $request->type_id,
				'worker_id' => $request->worker_id,
				'service_id' => $service,
			], 406);
		}
		$request['service_id'] = $service;

		$serviceSchedule = ServiceSchedule::create($request->all());

		return $this->sendResponse($serviceSchedule, 'Service schedule created');
	}

	public function schedulesUpdate(Request $request, $service, $schedule)
	{
		$req_validation = Validator::make($request->all(), [
			'start' => 'required|date_format:H:i',
			'end' => 'required|date_format:H:i|after:start',
			'type_id' => 'required',
			'worker_id' => 'required',
		]);

		if($req_validation->fails()){
			return $this->sendError('Validation Error.', $req_validation->errors(), 409);       
		}

		$validation = Helpers::checkUser($service);

		if ($validation) {
			return $this->sendError($validation['message'], ['service_id' => $service], $validation['code']);
		}

		if (!Type::where('service_id', $service)->where('id', $request->type_id)->exists()) {
			return $this->sendError('this service does not have this type', ['type_id' => $type, 'service_id' => $service], 404);
		}

		if (!auth()->user()->workers->contains($request->worker_id)) {
			return $this->sendError('You dont have this worker.', ['worker_id' => $request->worker_id], 404);
		}

		if (!ServiceSchedule::where('id', $schedule)->exists()) {
			return $this->sendError('This Schedule Not exists for this service', [
				'type_id' => $request->type_id,
				'worker_id' => $request->worker_id,
				'service_id' => $service,
				'schedule_id' => $schedule
			], 406);
		}

		$request['service_id'] = $service;
		
		$serviceSchedule = ServiceSchedule::find($schedule);

		$serviceSchedule->fill($request->all());

		if (!$serviceSchedule->isDirty()) {
			return $this->sendError('Must need to change any filed to update', ['request_data' => $request->all()], 406);
		}

		$serviceSchedule->save();

		return $this->sendResponse($serviceSchedule, 'Service schedule updated');
	}

	public function schedulesDelete($service, $schedule)
	{

		$validation = Helpers::checkUser($service);

		if ($validation) {
			return $this->sendError($validation['message'], ['service_id' => $service], $validation['code']);
		}

		if (!Service::find($service)->serviceSchedules->contains($schedule)) {
			return $this->sendError('Schedule does not exists for this service', ['service_id' => $service, 'schedule_id' => $schedule], 406);
		}

		$serviceSchedule = ServiceSchedule::find($schedule);

		if (!$serviceSchedule) {
			return $this->sendError('This Schedule Not exists for this service', [
				'service_id' => $service,
				'schedule_id' => $schedule
			], 406);
		}

		$serviceSchedule->delete();

		return $this->sendResponse($serviceSchedule, 'Service schedule deleted');
	}
}
