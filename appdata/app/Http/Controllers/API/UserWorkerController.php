<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Validator;
use App\Helpers;

use App\User;
use App\Worker;
use App\Leave;
use App\WorkingDay;
use Carbon\Carbon;

class UserWorkerController extends ApiController
{
    public function workers($user)
	{
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data, 'worker');

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		return $this->sendResponse($user_data->workers->sortByDesc('id')->values(), 'Workers for this user');
	}

	public function workerById($user, $worker)
	{
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}

		return $this->sendResponse($worker_data, 'Workers for this user');
	}
	public function workerDays(Request $request, $user, $worker)
	{
		$date = Carbon::parse($request->date)->format('Y-m-d');
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}

		if (!$worker_data->workingDays()->exists()) {
			return $this->sendError('Working Days does not exist for this worker');
		}

		$workingDays = $worker_data
		->workingDays()
		->where(function($q) use($date) {
			return $q->where('end_repeat', $date)
					->orWhere('end_repeat', null);
		})
		->where('day', $request->day)
		->orderBy('end_repeat', 'DESC')
		->first();
		return $this->sendResponse($workingDays, 'working days for this worker');
	}
		
	public function workerDaysAll(Request $request, $user)
	{
		$dayC = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
		$date = [];
		foreach($request->date as $dt) {
			$date[] = Carbon::parse($dt)->format('Y-m-d');
		}
		$user_data = User::find($user);
		
		$usererror = Helpers::checkUserExists($user, $user_data);
		
		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}
		
		$worker_data = $user_data->workers;

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'user_id' => $user
			], 404);
		}
		$wkddata = [];
		$i = 0;
		foreach($worker_data as $worker) {
			$wkddata[$worker->id] = Worker::find($worker->id)
			->workingDays()
			->where(function($q) use($date) {
				return $q->whereIn('end_repeat', $date)
						->orWhere('end_repeat', null);
			})
			->get()->toArray();
		}
		// return $wkddata;
		$rtnData = [];
		$tmpk = 0;
		$tmpk1 = 0;
		foreach($wkddata as $key => $data) {
			$rtnData[$tmpk]['worker_id'] = $key;
			for($i = 0; $i < count($data); $i++) {
				for($j = $i + 1; $j < count($data); $j++) {
					if($data[$j]['day'] == $data[$i]['day']) {
						$rtnData[$tmpk]['data'][$tmpk1]['day'] = $data[$i]['day'];
						if($data[$j]['end_repeat'] != null) {
							$rtnData[$tmpk]['data'][$tmpk1]['id'] = $data[$j]['id'];
							$rtnData[$tmpk]['data'][$tmpk1]['start'] = Carbon::parse($data[$j]['start'])->format('H:i');
							$rtnData[$tmpk]['data'][$tmpk1++]['end'] = Carbon::parse($data[$j]['end'])->format('H:i');
						}else {
							$rtnData[$tmpk]['data'][$tmpk1]['id'] = $data[$i]['id'];
							$rtnData[$tmpk]['data'][$tmpk1]['start'] = Carbon::parse($data[$i]['start'])->format('H:i');
							$rtnData[$tmpk]['data'][$tmpk1++]['end'] = Carbon::parse($data[$i]['end'])->format('H:i');
						}
					}
				}
				if(!$this->array_recursive_search_key_map($data[$i]['day'], $rtnData[$tmpk])) {
					$rtnData[$tmpk]['data'][$tmpk1]['day'] = $data[$i]['day'];
					$rtnData[$tmpk]['data'][$tmpk1]['id'] = $data[$i]['id'];
					$rtnData[$tmpk]['data'][$tmpk1]['start'] = Carbon::parse($data[$i]['start'])->format('H:i');
					$rtnData[$tmpk]['data'][$tmpk1++]['end'] = Carbon::parse($data[$i]['end'])->format('H:i');
				}
			}
			foreach($dayC as $day) {
				if(!$this->array_recursive_search_key_map($day, $rtnData[$tmpk])) {
					$rtnData[$tmpk]['data'][$tmpk1]['day'] = $day;
					$rtnData[$tmpk]['data'][$tmpk1]['id'] = '';
					$rtnData[$tmpk]['data'][$tmpk1]['start'] = '';
					$rtnData[$tmpk]['data'][$tmpk1++]['end'] = '';
				}
			}
			$tmpk1 = 0;
			$tmpk++;
		}
		// return $rtnData;
		return $this->sendResponse($rtnData, 'working days for this worker');
	}

	public function workerDaysWeek(Request $request, $user, $worker)
	{
		$dayC = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
		$date = [];
		foreach($request->date as $dt) {
			$date[] = Carbon::parse($dt)->format('Y-m-d');
		}
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = $user_data->workers;

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'user_id' => $user
			], 404);
		}
		$wkddata = [];
		$i = 0;
		
		$wkddata[$worker] = Worker::find($worker)
		->workingDays()
		->where(function($q) use($date) {
			return $q->whereIn('end_repeat', $date)
					->orWhere('end_repeat', null);
		})
		->get()->toArray();
		
		// return $wkddata;
		$rtnData = [];
		$tmpk = 0;
		$tmpk1 = 0;
		foreach($wkddata as $key => $data) {
			$rtnData[$tmpk]['worker_id'] = $key;
			for($i = 0; $i < count($data); $i++) {
				for($j = $i + 1; $j < count($data); $j++) {
					if($data[$j]['day'] == $data[$i]['day']) {
						$rtnData[$tmpk]['data'][$tmpk1]['day'] = $data[$i]['day'];
						if($data[$j]['end_repeat'] != null) {
							$rtnData[$tmpk]['data'][$tmpk1]['id'] = $data[$j]['id'];
							$rtnData[$tmpk]['data'][$tmpk1]['start'] = Carbon::parse($data[$j]['start'])->format('H:i');
							$rtnData[$tmpk]['data'][$tmpk1++]['end'] = Carbon::parse($data[$j]['end'])->format('H:i');
						}else {
							$rtnData[$tmpk]['data'][$tmpk1]['id'] = $data[$i]['id'];
							$rtnData[$tmpk]['data'][$tmpk1]['start'] = Carbon::parse($data[$i]['start'])->format('H:i');
							$rtnData[$tmpk]['data'][$tmpk1++]['end'] = Carbon::parse($data[$i]['end'])->format('H:i');
						}
					}
				}
				if(!$this->array_recursive_search_key_map($data[$i]['day'], $rtnData[$tmpk])) {
					$rtnData[$tmpk]['data'][$tmpk1]['day'] = $data[$i]['day'];
					$rtnData[$tmpk]['data'][$tmpk1]['id'] = $data[$i]['id'];
					$rtnData[$tmpk]['data'][$tmpk1]['start'] = Carbon::parse($data[$i]['start'])->format('H:i');
					$rtnData[$tmpk]['data'][$tmpk1++]['end'] = Carbon::parse($data[$i]['end'])->format('H:i');
				}
			}
			foreach($dayC as $day) {
				if(!$this->array_recursive_search_key_map($day, $rtnData[$tmpk])) {
					$rtnData[$tmpk]['data'][$tmpk1]['day'] = $day;
					$rtnData[$tmpk]['data'][$tmpk1]['id'] = '';
					$rtnData[$tmpk]['data'][$tmpk1]['start'] = '';
					$rtnData[$tmpk]['data'][$tmpk1++]['end'] = '';
				}
			}
			$tmpk1 = 0;
			$tmpk++;
		}
		return $this->sendResponse($rtnData, 'working days for this worker');
	}
	
	public function addWorkerWorkingDay(Request $request, $user, $worker)
	{
		if($request->repeat) {
			$request['end_repeat'] = null;	
		}else {
			$request['end_repeat'] = Carbon::parse($request->date)->format('Y-m-d');
		}
		unset($request["date"]);
		unset($request["worker"]);
		unset($request["index"]);
		
		$user_data = User::find($user);
		
		$usererror = Helpers::checkUserExists($user, $user_data);
		
		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}
		
		$worker_data = $user_data->workers;
		
		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'user_id' => $user
			], 404);
		}
		
		if (WorkingDay::where('worker_id', $worker)->where('day', $request->day)->exists()) {
			if($request->repeat == 0){
				$workerDayData = WorkingDay::where('worker_id', $worker)->where('end_repeat', $request->end_repeat)->first();
				if ($workerDayData) {
					$workerDayData->start = $request->start;
					$workerDayData->end = $request->end;
					$workerDayData->save();
					return $this->sendResponse($workerDayData, 'Woker Day saved.');
				} else {
					WorkingDay::create($request->all());
					return $this->sendResponse($workerDayData, 'Woker Day saved.');
				}
			} else {
				$workingDayData = WorkingDay::where('worker_id', $worker)->whereNull('end_repeat')->where('day', $request->day)->first();
				if ($workingDayData) {
					$workingDayData->start = $request->start;
					$workingDayData->end = $request->end;
					$workingDayData->save();
					return $this->sendResponse($workingDayData, 'Woker Day saved.');
				} else {
					WorkingDay::create($request->all());
					return $this->sendResponse($workingDayData, 'Woker Day saved.');
				}
			}
		} else {
			$worker_day_added = WorkingDay::create( $request->all());
			return $this->sendResponse($worker_day_added, 'Woker Day saved.');
		}
	}
	public function addWorker(Request $request, $user)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required|regex:/(^([a-zA-z ]+)(\d+)?$)/u',
			'surname' => 'required|regex:/(^([a-zA-z ]+)(\d+)?$)/u',
			'email' => 'required|email|unique:workers',
			'phone' => 'required|numeric|unique:workers'
			]);

			if($validator->fails()){
				return $this->sendError('Validation Error.', $validator->errors(), 409);
			}

			$user_data = User::find($user);

			$usererror = Helpers::checkUserExists($user, $user_data);

			if ($usererror) {
				return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
			}
			$request['user_id'] = $user;

			$worker_data = Worker::create($request->all());

			return $this->sendResponse($worker_data, 'Worker created');
		}

		public function workerUpdate(Request $request, $user, $worker)
		{
			$validator = Validator::make($request->all(), [
				'name' => 'required|regex:/(^([a-zA-z ]+)(\d+)?$)/u',
				'surname' => 'required|regex:/(^([a-zA-z ]+)(\d+)?$)/u',
				'email' => 'required|email',
				'phone' => 'required|numeric'
			]);

			if($validator->fails()){
				return $this->sendError('Validation Error.', $validator->errors(), 409);
			}
			$user_data = User::find($user);

			$usererror = Helpers::checkUserExists($user, $user_data);

			if ($usererror) {
				return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
			}

			$worker_data = $user_data->workers()->where('id', $worker)->first();

			if (!$worker_data) {
				return $this->sendError('Worker not found for this user', [
					'worker_id' => $worker,
					'user_id' => $user
				], 404);
			}

			if(Worker::where('email', $request->email)->where('id', '!=', $worker)->exists()){
				return $this->sendError('Email already used by another worker', ['worker_id' => $worker, 'email' => $request->email], 406);
			}
			if(Worker::where('phone', $request->phone)->where('id', '!=', $worker)->exists()){
				return $this->sendError('Phone Number already used by another worker', ['worker_id' => $worker, 'phone' => $request->phone], 406);
			}
			$worker_data->fill($request->all());

			if (!$worker_data->isDirty()) {
				return $this->sendError('Must need to update any filed', ['request_data' => $request->all()], 406);
			}

			$worker_data->save();

			return $this->sendResponse($worker_data, 'Worker updated');
		}


	public function workerDelete($user, $worker)
	{
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}
		$worker_data->delete();

		return $this->sendResponse($worker_data, 'Worker deleted');
	}

	public function workerDaysStore(Request $request, $user, $worker)
	{
		$req_validation = Validator::make($request->all(), [
            'repeat' => 'required|integer|min:0|max:1',
            'end_repeat' => 'required|date_format:Y-m-d|after:today',
        ]);

        if($req_validation->fails()){
            return $this->sendError('Validation Error.', $req_validation->errors(), 409);
        }
        $days = ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
        if (!in_array($request->day, $days)) {
        	return $this->sendError('Invalid Days format', ['format' => $days] , 406);
        }

		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}

		$request['worker_id'] = $worker;

		if(WorkingDay::where('worker_id', $worker)->where('day', $request->day)->exists()){
			return $this->sendError('This Day already exists for this worker', ['Day' => $request->day, 'worker_id' => $worker], 406);
		}

		$workingDays = WorkingDay::create($request->all());

		return $this->sendResponse($workingDays, 'Working days creates');

	}

	public function workerDaysUpdate(Request $request, $user, $worker, $day)
	{
		$req_validation = Validator::make($request->all(), [
            'repeat' => 'required|integer|min:0|max:1',
            'end_repeat' => 'required|date_format:Y-m-d|after:today',
        ]);

        if($req_validation->fails()){
            return $this->sendError('Validation Error.', $req_validation->errors(), 409);
        }
        $days = ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
        if (!in_array($request->day, $days)) {
        	return $this->sendError('Invalid Days format', ['format' => $days] , 406);
        }

		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}

		if(!WorkingDay::where('worker_id', $worker)->where('day', $request->day)->exists()){
			return $this->sendError('This Day Not exists for this worker', ['Day' => $request->day, 'worker_id' => $worker], 406);
		}

		$working_day = WorkingDay::find($day);

		$working_day->fill($request->all());

		if (!$working_day->isDirty()) {
			return $this->sendError('Must need to update any filed', ['request_data' => $request->all()], 406);
		}

		$working_day->save();

		return $this->sendResponse($working_day, 'Working days updated');
	}

	public function workerDaysdelete($user, $worker, $day)
	{
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}

		if (!$worker_data->workingDays()->exists()) {
			return $this->sendError('Working Days does not exist for this worker');
		}

		$working_day = $worker_data->workingDays()->where('id', $day)->first();
		if (!$working_day) {
			return $this->sendError('Working Day not found for this worker', [
				'worker_id' => $worker,
				'working_day_id' => $day
			], 404);
		}

		$working_day->delete();

		return $this->sendResponse($working_day, 'working day deleted ');
	}

	public function workerDaysById($user, $worker, $day)
	{
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}

		if (!$worker_data->workingDays()->exists()) {
			return $this->sendError('Working Days does not exist for this worker');
		}

		$working_day = $worker_data->workingDays()->where('id', $day)->first();
		if (!$working_day) {
			return $this->sendError('Working Day not found for this worker', [
				'worker_id' => $worker,
				'working_day_id' => $day
			], 404);
		}
		return $this->sendResponse($working_day, 'working days for this worker');
	}

	public function leaveDays($user, $worker)
	{
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}

		if (!$worker_data->leaves()->exists()) {
			return $this->sendError('Leaves Days does not exist for this worker');
		}

		return $this->sendResponse($worker_data->leaves, 'Leave days for this worker');
	}
	public function leaveDaysStroe(Request $request, $user, $worker)
	{
		$request['date'] = Carbon::parse($request->date)->format('Y-m-d');
		$validator = Validator::make($request->all(), [
			'date' => 'required|date',
			'start' => 'required',
			'end' => 'required|after:start',
		]);

		if($validator->fails()){
			return $this->sendError('Validation Error.', $validator->errors(), 409);
		}
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = Worker::find($worker);

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', ['worker_id' => $worker, 'user_id' => $user], 404);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}

		$request['worker_id'] = $worker;

		$leave_day = Leave::create($request->all());

		return $this->sendResponse($leave_day, 'Worker Leave created');
	}
	public function leaveDaysById($user, $worker, $day)
	{
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}

		if (!$worker_data->leaves()->exists()) {
			return $this->sendError('Leave Day does not exist for this worker');
		}

		$leave_day = $worker_data->leaves()->where('id', $day)->first();
		if (!$leave_day) {
			return $this->sendError('Leave Day not found for this worker', [
				'worker_id' => $worker,
				'leave_day_id' => $day
			], 404);
		}
		return $this->sendResponse($leave_day, 'Leave day for this worker');
	}

	public function leaveDaysUpdate(Request $request, $user, $worker, $leave)
	{
		$request['date'] = Carbon::parse($request->date)->format('Y-m-d');
		$validator = Validator::make($request->all(), [
			'date' => 'required|date',
			'start' => 'required',
			'end' => 'required|after:start',
		]);

		if($validator->fails()){
			return $this->sendError('Validation Error.', $validator->errors(), 409);
		}

		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = Worker::find($worker);

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', ['worker_id' => $worker, 'user_id' => $user], 404);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}

		$leave_data = Leave::find($leave);

		if ($leave_data->worker_id != $worker) {
			return $this->sendError('Leave does not exist for this worker', [
				'leave_id' => $leave,
				'worker_id' => $worker
			], 404);
		}

		$leave_data->fill($request->all());

		if (!$leave_data->isDirty()) {
			return $this->sendError('Must need to change any data to update', [
				'request_data' => $request->all(),
			], 406);
		}

		$leave_data->save();

		return $this->sendResponse($leave_data, 'Leave data updated for this Worker');
	}

	public function leaveDaysDelete($user, $worker, $leave)
	{
		$user_data = User::find($user);

		$usererror = Helpers::checkUserExists($user, $user_data);

		if ($usererror) {
			return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
		}

		$worker_data = Worker::find($worker);

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', ['worker_id' => $worker, 'user_id' => $user], 404);
		}

		$worker_data = $user_data->workers()->where('id', $worker)->first();

		if (!$worker_data) {
			return $this->sendError('Worker not found for this user', [
				'worker_id' => $worker,
				'user_id' => $user
			], 404);
		}

		$leave_data = Leave::find($leave);

		if (!$leave_data) {
			return $this->sendError('Leave data not found', ['leave_id' => $leave,
				'worker_id' => $worker], 404);
		}

		if ($leave_data->worker_id != $worker) {
			return $this->sendError('Leave does not exist for this worker', [
				'leave_id' => $leave,
				'worker_id' => $worker
			], 404);
		}

		$leave_data->delete();

		return $this->sendResponse($leave_data, 'Leave data deleted');
	}
	private function array_recursive_search_key_map($needle, $haystack) {
		foreach($haystack as $first_level_key=>$value) {
			if ($needle === $value) {
				return array($first_level_key);
			} elseif (is_array($value)) {
				$callback = $this->array_recursive_search_key_map($needle, $value);
				if ($callback) {
					return array_merge(array($first_level_key), $callback);
				}
			}
		}
		return false;
	}
}
