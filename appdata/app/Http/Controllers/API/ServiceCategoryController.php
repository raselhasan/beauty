<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Helpers;

use App\Service;
use App\SubCategory;

class ServiceCategoryController extends ApiController
{
    public function category($id)
    {
    	$validation = Helpers::checkUser($id);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['id' => $id], $validation['code']);
    	}
    	
    	$category = Service::find($id)->category;

    	return $this->sendResponse($category, 'category for services id : '. $id);
    }

    public function subcategory($id)
    {
    	$validation = Helpers::checkUser($id);

    	if ($validation) {
    		return $this->sendError($validation['message'], ['id' => $id], $validation['code']);
    	}

    	$subids = json_decode(Service::find($id)->sub_category);

    	$subcategories = SubCategory::findMany($subids);

    	return $this->sendResponse($subcategories, 'Sub category for services id : '. $id);

    }

    public function subcategoryById($service, $subcategory)
    {
        $validation = Helpers::checkUser($service);

        if ($validation) {
            return $this->sendError($validation['message'], ['id' => $service], $validation['code']);
        }

        $subids = json_decode(Service::find($service)->sub_category);

        if (!in_array($subcategory, $subids)) {
            return $this->sendError('Category does not have this sub category', 
                ['service_id' => $service, 'sub_category' => $subcategory], 404);
        }
        $subcategories = SubCategory::find($subcategory);


        return $this->sendResponse($subcategories, 'Sub category for services id : '. $service);
    }
}
