<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Helpers;

use App\User;
class UserGalleryController extends ApiController
{
    public function galleries($user)
    {
    	$user_data = User::find($user);

    	$usererror = Helpers::checkUserExists($user, $user_data);

    	if ($usererror) {
    		return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
    	}

    	return $this->sendResponse($user_data->galleries, 'Galleries for this user');
    }

    public function galleriesById($user, $gallery)
    {
    	$user_data = User::find($user);

    	$usererror = Helpers::checkUserExists($user, $user_data);

    	if ($usererror) {
    		return $this->sendError($usererror['data'], $usererror['message'], $usererror['code']);
    	}
    	if (!$user_data->galleries->contains($gallery)) {
    		return $this->sendError('Gallery not found for this user', ['user_id' => $user, 'gallery_id' => $gallery], 404);
    	}

    	$gallery_data = $user_data->galleries()->where('id', $gallery)->first();
    	if (!$gallery_data) {
    		return $this->sendError('Gallery not found', ['user_id' => $user, 'gallery_id' => $gallery], 404);
    	}
    	return $this->sendResponse($gallery_data, 'Galleries for this user');
    }
}
