<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use Carbon\Carbon;
use App\Payment;
use PDF;
use App\ProfilePayment;
use App\VipPackage;
use App\AddShowingPackage;
use App\AddShowingPayment;
class PaymentListController extends Controller
{
    public function index()
    {
    	$services = Service::where('user_id',auth()->user()->id)->where('active',1)->with('payment')->get()->toArray();
        $profile_payments = ProfilePayment::where('user_id',auth()->user()->id)->with('vipPackage')->first()->toArray();
        $add_controll = AddShowingPayment::where('user_id',auth()->user()->id)->with('AddShowingPackage')->first()->toArray();
    	return view('pages.payment-list',compact('services','profile_payments','add_controll'));
    }

    public function paymentHistory()
    {
    	$payments = Payment::where('user_id',Auth()->user()->id)->get()->groupBy('order_id')->toArray();
        $profile_payments = ProfilePayment::where('user_id',Auth()->user()->id)->with('user','vipPackage')->orderBy('id','DESC')->get()->toArray();

        $AddShowingPayment = AddShowingPayment::where('user_id',Auth()->user()->id)->with('user','AddShowingPackage')->orderBy('id','DESC')->get()->toArray();

    	//dd($AddShowingPayment);
    	return view('pages.payment-history',compact('payments','profile_payments','AddShowingPayment'));
    }

    public function paymentDownload(Request $request)
    {
    	$ids = explode(' ',trim($request->ids));
    	$payments = Payment::whereIn('id',$ids)->get()->toArray();
    	$pdf = PDF::loadView('pdf/payment-pdf',['payments'=>$payments]);
        return $pdf->download('invoice.pdf');
    }
}
