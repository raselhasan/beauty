<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Category;
use App\SubCategory;
use App\District;
use DB;
use Session;
use App\User;
class SearchServiceController extends Controller
{
    public function index(Request $request)
    {

        $ip = '203.78.146.6'; // your ip address here
    	$item['lat'] = $request->lat;
    	$item['lng'] = $request->lng;
        $item['back'] = 0;
        if($request->city){
            $item['city'] = $request->city;
            $item['back'] = 1;
        }else{
            $getCity = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
            $item['city'] = $getCity['city'];
        }
    	$item['title'] = $request->title; 
    	$item['distance'] = $request->distance;
    	$item['price'] = $request->price;
    	$item['page'] = $request->page;

    	// http://localhost/beauty/services?lat=23.7834386&lng=90.3729968&city=Comilla&title=fa&salons=1&personal=1&distance=10&price=0,100&category=1&sub_category%5B%5D=2&sub_category%5B%5D=3&sub_category%5B%5D=1&page=1
    	$item['salons'] = $request->salons;
    	$item['personal'] = $request->personal;
    	$item['category'] = '';
    	if($request->category){
    		$getCategory = Category::find($request->category);
    		$item['category'] = $getCategory->name;
    	}
    	$item['sub_category'] = '';

        if($request->sub_category){
            $sub_categories = SubCategory::where('category_id',$request->category)->get();
            if(count($sub_categories) > 0){
                foreach ($sub_categories as $key => $sub_cat) {
                    $checked = '';
                    if(in_array($sub_cat->id,$request->sub_category)){
                        $checked = 'checked';
                    }
                    $item['sub_category'] .='<div class="ctm-option-ch"><label class="ctm-con mar-0">'.$sub_cat->name.'
                        <input type="checkbox" class="my-checkbox" name="sub_category[]" value="'.$sub_cat->id.'"'.$checked.' onclick="getServices(0)">
                        <span class="checkmark"></span>
                    </label></div>';

                }
            }
        }
        $item['page'] = $request->page;
    	$service['categories'] = Category::all();
    	$service['cities'] = District::all();

        $item['recently_viewed'] = [];
        if(Session::has('recently_viewed')){
           $item['recently_viewed'] = Service::whereIn('id',Session::get('recently_viewed'))->get(); 
        }
        //dd($item['recently_viewed']);
        return view('pages.blog',compact('service','item'));
	}

    public function getUserCity()
    {
        $ip = '203.78.146.6'; // your ip address here
        $getCity = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
        return $getCity['city'];
    }

	public function indexData(Request $request)
	{
		$earthRadiusMiles = 3959;
        $lat = $request->lat;
        $lng = $request->lng;
        $distance = $request->distance;
        $city = $request->city;
        $title = $request->title;
        $category = $request->category;
        $sub_category = $request->sub_category;
        $personal = $request->personal;
        $salons = $request->salons;
        if($sub_category[0] == ''){
           $sub_category = []; 
        }
        $today = date('Y-m-d H:i:s');
        $price = $request->price;
        $min = 0;
        $max = 0;
        if($price){
        	$exp = explode(",",$price);
        	$min = $exp[0];
        	$max = $exp[1];
    	}
    	if($min == 'NaN'){
    		$min = 0;
    	}

        $selectDistance =
        '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
        '* cos( radians( lat ) ) ' .
        '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
        '+ sin( radians(' . $lat . ') ) ' .
        '* sin( radians( lat ) ) ) )';

        $top_services = \DB::table('services')->selectRaw("
            services.*,services.id as main_id,top_services.*,users.name,users.surname
        ")
        ->when($lat != null, function($q) use ($lat,$lng,$selectDistance,$distance) {
            return $q->selectRaw("{$selectDistance} AS distance")
                   ->whereRaw("{$selectDistance} < ?", $distance);
        })
        ->join('top_services','services.id','=','top_services.service_id')
        ->join('users','services.user_id','=','users.id')
        ->when($city != null, function($q) use($city) {
            return $q->where('services.city',$city);
        })
        ->where('services.top_service',1)
        ->whereDate('top_services.end_date', '>=', $today)
        ->inRandomOrder()
        ->limit(10)
        ->get();
        $top_services = $top_services->sortByDesc("star");

        $count_top_services = count($top_services);


        $services = Service::when($title != null, function($q) use ($title) {
            return $q->where('title', 'LIKE',"%{$title}%");
        })
        ->when($lat != null, function($q) use ($lat,$lng,$selectDistance,$distance) {
            return $q->selectRaw("*, {$selectDistance} AS distance")
                   ->whereRaw("{$selectDistance} < ?", $distance);
        })
        ->where('services.top_service',0)
        ->when($category != null, function($q) use($category) {
            return $q->where('category_id', $category);
        })
        ->when(count($sub_category) > 0, function($q) use($sub_category) {
            return $q->whereJsonContains('sub_category', $sub_category);
        })
        ->when($city != null, function($q) use($city) {
            return $q->where('city',$city);
        })
        
        ->with(["types" => function($q) use ($min, $max) {
            $q->when($max != null, function($iq) use ($min,$max) {
                return $iq->whereBetween('types.price',[$min,$max]);
            });
        },
        "user" => function($q) use ($personal,$salons) {
            $q->when($personal != null, function($iq) use ($personal) {
                return $iq->where('users.account_type',1);
            })
            ->when($salons != null, function($iq) use ($salons) {
                return $iq->where('users.account_type',2);
            });
        }])
        ->paginate(25);

        $count_services = count($services);
        $total_services = $count_top_services + $count_services;
        $divider = $total_services / 2;
        $take_service = $divider;
        $take_profile = $divider;
        if (strpos($divider,'.') !== false) {
            $take_service = $take_service + 0.5;
            $take_profile = $take_profile - 0.5;
        }
        $serviceData = $this->takeVipServices($take_service,$lat,$lng,$distance);
        $profileData = $this->takeVipProfile($take_profile,$lat,$lng,$distance);
        $allVip = $serviceData->merge($profileData);
        $allVip = $allVip->sortBy('distance');
        return view('pages.services-data',compact('services','top_services','allVip'));	

	}

	public function paginationData(Request $request)
	{
		$earthRadiusMiles = 3959;
        $lat = $request->lat;
        $lng = $request->lng;
        $distance = $request->distance;
        $city = $request->city;
        $title = $request->title;
        $category = $request->category;
        $sub_category = $request->sub_category;
        if($sub_category[0] == ''){
           $sub_category = []; 
        }
        $today = date('Y-m-d H:i:s');
        $price = $request->price;
        $min = 0;
        $max = 0;
        if($price){
            $exp = explode(",",$price);
            $min = $exp[0];
            $max = $exp[1];
        }
        if($min == 'NaN'){
            $min = 0;
        }
        $personal = $request->personal;
        $salons = $request->salons;

		$selectDistance =
        '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
        '* cos( radians( lat ) ) ' .
        '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
        '+ sin( radians(' . $lat . ') ) ' .
        '* sin( radians( lat ) ) ) )';

        $services = Service::when($title != null, function($q) use ($title) {
            return $q->where('title', 'LIKE',"%{$title}%");
        })
        ->when($lat != null, function($q) use ($lat,$lng,$selectDistance,$distance) {
            return $q->selectRaw("*, {$selectDistance} AS distance")
                   ->whereRaw("{$selectDistance} < ?", $distance);
        })
        ->where('services.top_service',0)
        ->when($category != null, function($q) use($category) {
            return $q->where('category_id', $category);
        })
        ->when(count($sub_category) > 0, function($q) use($sub_category) {
            return $q->whereJsonContains('sub_category', $sub_category);
        })
        ->when($city != null, function($q) use($city) {
            return $q->where('city',$city);
        })
        ->with(["types" => function($q) use ($min, $max) {
            $q->when($max != null, function($iq) use ($min,$max) {
                return $iq->whereBetween('types.price',[$min,$max]);
            });
        },
        "user" => function($q) use ($personal,$salons) {
            $q->when($personal != null, function($iq) use ($personal) {
                return $iq->where('users.account_type',1);
            })
            ->when($salons != null, function($iq) use ($salons) {
                return $iq->where('users.account_type',2);
            });
        }])
        ->paginate(25);

        $total_services = count($services);
        $divider = $total_services / 2;
        $take_service = $divider;
        $take_profile = $divider;
        if (strpos($divider,'.') !== false) {
            $take_service = $take_service + 0.5;
            $take_profile = $take_profile - 0.5;
        }
        $serviceData = $this->takeVipServices($take_service,$lat,$lng,$distance);
        $profileData = $this->takeVipProfile($take_profile,$lat,$lng,$distance);
        $allVip = $serviceData->merge($profileData);
        $allVip = $allVip->sortBy('distance');

        return view('pages.top-service',compact('services','allVip'));
	}

    public function takeVipServices($take_service,$lat,$lng,$distance)
    {  
        $today = date('Y-m-d H:i:s');
        if($lat ==''){
            $ip = '203.78.146.6'; // your ip address here
            $getUserLocation = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
            $lat = $getUserLocation['lat'];
            $lng = $getUserLocation['lon'];
        }

        $selectDistance =
        '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
        '* cos( radians( lat ) ) ' .
        '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
        '+ sin( radians(' . $lat . ') ) ' .
        '* sin( radians( lat ) ) ) )';

        $vip_services = \DB::table('services')->selectRaw("
            services.*,services.id as main_id,vip_services.*,users.name,users.surname
        ")
        ->when($lat != null, function($q) use ($lat,$lng,$selectDistance,$distance) {
            return $q->selectRaw("{$selectDistance} AS distance")
                   ->whereRaw("{$selectDistance} < ?", $distance);
        })
        ->join('vip_services','services.id','=','vip_services.service_id')
        ->join('users','services.user_id','=','users.id')
        ->where('services.vip_service',1)
        ->whereDate('vip_services.end_date', '>=', $today)
        ->inRandomOrder()
        ->limit($take_service)
        ->get();
        if(count($vip_services) == $take_service){
            return $vip_services;
        }elseif ($distance >= 100) {
            return $vip_services;

        }else{
            return $this->takeVipServices($take_service,$lat,$lng,$distance+10);
        }
    }

    public function takeVipProfile($take_profile,$lat,$lng,$distance)
    {
        $today = date('Y-m-d H:i:s');
        if($lat ==''){
            $ip = '203.78.146.6'; // your ip address here
            $getUserLocation = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
            $lat = $getUserLocation['lat'];
            $lng = $getUserLocation['lon'];
        }

        $selectDistance =
        '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
        '* cos( radians( user_lat ) ) ' .
        '* cos( radians( user_lng ) - radians(' . $lng . ') ) ' .
        '+ sin( radians(' . $lat . ') ) ' .
        '* sin( radians( user_lat ) ) ) )';

        $vip_profile = \DB::table('users')->selectRaw("
            users.*,profile_payments.id as payment_id,profile_payments.start_date,profile_payments.end_date
        ")
        ->when($lat != null, function($q) use ($lat,$lng,$selectDistance,$distance) {
            return $q->selectRaw("{$selectDistance} AS distance")
                   ->whereRaw("{$selectDistance} < ?", $distance);
        })
        ->join('profile_payments','users.id','=','profile_payments.user_id')
        ->where('users.vip_profile',1)
        ->whereDate('profile_payments.end_date', '>=', $today)
        ->inRandomOrder()
        ->limit($take_profile)
        ->get();
        if(count($vip_profile) == $take_profile){
            return $vip_profile;
        }elseif ($distance >= 100) {
            return $vip_profile;

        }else{
            return $this->takeVipProfile($take_profile,$lat,$lng,$distance+10);
        }
    }

    public function indexVipData(Request $request)
    {
        $services = $this->takeVipServices(8,$request->lat,$request->lng,$request->distance);
        $profiles = $this->takeVipProfile(8,$request->lat,$request->lng,$request->distance);
        $services = $services->sortBy('distance');
        $profiles = $profiles->sortBy('distance');
        return view('pages.vip-profile-service',compact('services','profiles'));

    }
    










	public function getSubCategory(Request $request)
	{
		$sub_categories = SubCategory::where('category_id',$request->category_id)->get();
		$html = '';
		if(count($sub_categories) > 0){
			foreach ($sub_categories as $key => $sub_cat) {
				$html .='<div class="ctm-option-ch"><label class="ctm-con mar-0">'.$sub_cat->name.'
					<input type="checkbox" name="sub_category[]" value="'.$sub_cat->id.'" onclick="getServices(0)">
					<span class="checkmark"></span>
				</label></div>';

			}
		}
		return $html;
	}

	public function test()
	{
		$title = "maiore";
        $category = 5;
        $sub_category = [20];
        $city = "Dhaka";
        $min = 10;
        $max = 35;
        $lat = '23.810332';
        $lng = '90.4125181';
        $distance = '10';
        $selectDistance =
        '( 3959 * acos( cos( radians(' . $lat . ') ) ' .
        '* cos( radians( lat ) ) ' .
        '* cos( radians( lng ) - radians(' . $lng . ') ) ' .
        '+ sin( radians(' . $lat . ') ) ' .
        '* sin( radians( lat ) ) ) )';

        $data = Service::when($title != null, function($q) use ($title) {
            return $q->where('title', 'LIKE',"%{$title}%");
        })
        ->selectRaw("*, {$selectDistance} AS distance")
        ->whereRaw("{$selectDistance} < ?", $distance)
        ->when($category != null, function($q) use($category) {
            return $q->where('category_id', $category);
        })
        ->when(count($sub_category) > 0, function($q) use($sub_category) {
            return $q->whereJsonContains('sub_category', $sub_category);
        })
        ->when($city != null, function($q) use($city) {
            return $q->where('city',$city);
        })
        ->with(["types" => function($q) use ($min, $max) {
            $q->when($max != null, function($iq) use ($min,$max) {
                return $iq->whereBetween('types.price',[$min,$max]);
            });
        }])
        ->paginate(15);
        dd($data->toArray());

	}

    public function profileList(Request $request)
    {
        $name = $request->profile_name;
        if($name == ''){
            $data['users'] = User::orderBy(DB::raw('RAND()'))->paginate(12);
            
        }else{
            $data['users'] = User::where('name','LIKE',"%{$name}%")
            ->orWhere('surname','LIKE',"%{$name}%")
            ->orWhere('nickname','LIKE',"%{$name}%")
            ->paginate(12);
        }
        $data['profile_name'] = $name;
        $service = Service::where('vip_service',1)->orderBy(DB::raw('RAND()'))->limit(3)->get();
        $profile = User::where('vip_profile',1)->orderBy(DB::raw('RAND()'))->limit(2)->get();
        $data['vip_item'] = $service->merge($profile);
        //dd($data['vip_item']->toArray());
        return view('pages.profile-list',compact('data'));
    }

    public function profileAndService(Request $request){

        $serviceData = $this->takeVipServices(4,$request->lat,$request->lng,5);
        $profileData = $this->takeVipProfile(4,$request->lat,$request->lng,5);
        $profiles = '';
        $services = '';
        if(count($profileData) > 0)
        {
            foreach ($profileData as $key => $profile) {
                $img = asset('assets/img/services/'.$profile->profile_image);
                $url = url("public-profile").'/'.$profile->id;
                $profiles .='<div class="blog">
                <div class="blog-img">
                  <img src="'.$img.'">
                </div>
                <div class="blog-des">
                  <div class="blog-txt">
                   '.substr($profile->bio,0,85).'...
                  </div>
                  <div class="blog-dta star">
                    <span class="date"><a href="'.$url.'">View profile</a></span>
                  </div>
                </div>
              </div>';
            }
        }
        if(count($serviceData) > 0)
        {
            foreach ($serviceData as $key => $service) {
                $img = asset('assets/img/services/'.$service->image);
                $url = url("about-model").'/'.$service->main_id;
                $services .='<div class="one-content">
                  <div class="single-content">
                    <div class="content-img-p">
                      <div class="content-img">
                        <img src="'.$img.'">
                      </div>
                    </div>
                  </div>
                  <div class="content-des">
                    <h5><a href="'.$url.'" class="url-color">'.$service->title.'</a></h5>
                  </div>
                  <div class="content-tlt">
                    '.substr($service->description,0,120).'...
                  </div>

                </div>';
            }
        }
        return response()->json(['profiles'=>$profiles,'services'=>$services]);
    }


}
