<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:admin');
    }
    public function registration(Request $request)
    {
        // dd($req->all());
        $checkUser = User::where('email', $request->email)->exists();
        if ($checkUser) {
            return response()->json(['error' => 'This email already have one account.'], 200);
        }
        $user = new User;
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->nickname = $request->nickname;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->account_type = $request->account_type;
        if($request->account_type == 1){
            $user->show_name = '["3"]';
        }
        $user->save();

        $headers = [
            'Accept' => 'application/json'
        ];
        $client = new \GuzzleHttp\Client([
            'headers' => $headers
        ]);
        $url = url('/') . '/api/login';
        $myBody = [];
        $myBody['email'] = $request->email;
        $myBody['password'] = $request->password;

        try {
            $response = $client->post($url,  ['form_params' => $myBody]);
        } catch (Exception $e) {
            //throw $th;
            return response()->json(['error' => 'Username or Password not match.'], 200);
        }
        $data = json_decode($response->getBody()->getContents());

        if ($data->success) {
            // Attempt to log the admin in
            if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
                // if successful, then redirect to their intend locati

                $request->session()->put('api_token', $data->data->token);
                // return redirect()->route('user.profile');
                return response()->json(['user' => Auth::user()], 200);
            }
        }
    }
}
