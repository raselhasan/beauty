<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function __construct()
    {
        $this->middleware('guest:web')->except('logout');
    }
    public function login(Request $request)
    {
        $headers = [
            'Accept' => 'application/json'
        ];
        $client = new \GuzzleHttp\Client([
            'headers' => $headers
        ]);
        $url = url('/') . '/api/login';
        $myBody = [];
        $myBody['email'] = $request->email;
        $myBody['password'] = $request->password;

        try {
            $response = $client->post($url,  ['form_params' => $myBody]);
        } catch (Exception $e) {
            //throw $th;
            return response()->json(['error' => 'Username or Password not match.'], 200);
        }
        $data = json_decode($response->getBody()->getContents());

        if ($data->success) {
            // Attempt to log the admin in
            if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
                // if successful, then redirect to their intend locati

                $request->session()->put('api_token', $data->data->token);
                // return redirect()->route('user.profile');
                return response()->json(['user' => Auth::user()], 200);
            }
        }

        // if unsuccessful, then redirect back to login page with form data
        return response()->json(['error' => 'Username or Password not match.'], 200);
    }
    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        return $this->loggedOut($request) ?: redirect()->route('index');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('web');
    }
}
