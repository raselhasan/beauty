<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use Session;
class LocalizationController extends Controller
{
    public function index(Request $request,$locale) {
      Session::put('locale', $locale);
      return back();
   }
}
