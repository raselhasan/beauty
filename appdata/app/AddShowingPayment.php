<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddShowingPayment extends Model
{
    public function user()
    {
    	return $this->hasOne(User::class,'id','user_id');
    }
    public function AddShowingPackage()
    {
    	return $this->hasOne(AddShowingPackage::class,'id','package_id');
    }
}
