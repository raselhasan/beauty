<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkingDay extends Model
{
	protected $guarded = [];
    public function workerSchedules()
    {
    	return $this->hasMany(WorkerSchedule::class);
    }

	public function worker()
	{
		return $this->belongsTo(Worker::class);
	}
}
