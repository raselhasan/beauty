<?php 

namespace App;

use App\Service;

class Helpers
{
	public static function checkUser($id)
	{
		if (!Service::where('id', $id)->exists()) {
			return [
				'message' => 'Service not found',
				'code' => 404
			];
		}

		if (!auth()->user()->services->contains($id)) {
			return [
				'message' => 'You does not own this service',
				'code' => 401
			];
		}

		return null;
	}

	public static function checkUserExists($user, $user_data, $tp = null)
	{
		if (auth()->user()->id != $user) {
			return [
				'data' =>'Unauthorized', 
				'message' => 'Requested user is not same for authenticated user', 
				'code' => 401
			];
		}

		if (!$user_data) {
			return [
				'data' =>'User not found.', 
				'message' => ['user_id' => $user], 
				'code' => 404
			];
		}

		if (!$user_data->services()->exists() && $tp == 'service') {
			return  [
				'data' =>'No Service found.', 
				'message' => ['user_id' => $user], 
				'code' => 404
			];
		}

		if (!$user_data->workers()->exists() && $tp == 'worker') {
			return  [
				'data' =>'No Worker found.', 
				'message' => ['user_id' => $user], 
				'code' => 404
			];
		}

		return null;
	}
}

?>