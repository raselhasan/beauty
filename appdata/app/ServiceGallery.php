<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceGallery extends Model
{
    public function serviceGalleryTab()
    {
        return $this->belongsTo(ServiceGalleryTab::class);
    }
}
