<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfilePayment extends Model
{
    public function user()
    {
    	return $this->hasOne(User::class,'id','user_id');
    }
    public function vipPackage()
    {
    	return $this->hasOne(VipPackage::class,'id','package_id');
    }
}
