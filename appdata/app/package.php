<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public function duration()
    {
    	return $this->HasMany('App\Duration','package_id','id');
    }
    public function star()
    {
    	return $this->HasOne('App\Star','package_id','id');
    }
}
