<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceGalleryTab extends Model
{
	protected $guarded = [];
	
	public function service()
	{
		return $this->belongsTo(Service::class);
	}
    public function serviceGalleries()
    {
        return $this->hasMany(ServiceGallery::class);
    }
    
}
