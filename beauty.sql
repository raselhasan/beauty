-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2019 at 08:12 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beauty`
--

-- --------------------------------------------------------

--
-- Table structure for table `alt_service_img_type`
--

CREATE TABLE `alt_service_img_type` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'category 1', '2019-12-28 07:52:48', '2019-12-28 07:52:48'),
(2, 'category 2', '2019-12-28 07:52:48', '2019-12-28 07:52:48');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `picture`, `created_at`, `updated_at`) VALUES
(4, '1575203885_0.jpg', '2019-12-01 06:38:06', '2019-12-01 06:38:06'),
(5, '1575203886_1.jpg', '2019-12-01 06:38:06', '2019-12-01 06:38:06'),
(6, '1575203886_2.png', '2019-12-01 06:38:06', '2019-12-01 06:38:06'),
(7, '1575203886_3.jpg', '2019-12-01 06:38:06', '2019-12-01 06:38:06'),
(8, '1575203886_4.jpg', '2019-12-01 06:38:06', '2019-12-01 06:38:06'),
(9, '1575203886_5.jpg', '2019-12-01 06:38:06', '2019-12-01 06:38:06'),
(10, '1575203886_6.jpg', '2019-12-01 06:38:06', '2019-12-01 06:38:06'),
(11, '1575203886_7.jpg', '2019-12-01 06:38:06', '2019-12-01 06:38:06'),
(12, '1575203886_8.jpg', '2019-12-01 06:38:06', '2019-12-01 06:38:06'),
(13, '1575203886_9.jpg', '2019-12-01 06:38:06', '2019-12-01 06:38:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_21_085426_create_galleries_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` longtext,
  `img` varchar(255) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `sub_category` text,
  `address` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `user_id`, `title`, `description`, `img`, `category`, `sub_category`, `address`, `created_at`, `updated_at`) VALUES
(1, 1, 'Service One', '<p><span style=\"text-align: justify;\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</span><br></p>', 'profile_pic1577279381.jpg', 1, '[\"1\"]', NULL, '2019-12-25 00:45:00', '2019-12-29 07:07:28'),
(2, 1, 'Service two', '<p><span style=\"text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</span><br></p>', 'profile_pic1577281982.jpg', NULL, NULL, NULL, '2019-12-25 07:51:52', '2019-12-25 07:53:05'),
(3, 1, 'new-service', NULL, NULL, 1, '1', NULL, '2019-12-25 08:04:26', '2019-12-29 00:07:12');

-- --------------------------------------------------------

--
-- Table structure for table `service_galleries`
--

CREATE TABLE `service_galleries` (
  `id` int(11) NOT NULL,
  `service_gallery_tab_id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_galleries`
--

INSERT INTO `service_galleries` (`id`, `service_gallery_tab_id`, `img`, `created_at`, `updated_at`) VALUES
(1, 1, 'srvc_glry1577645529.jpg', '2019-12-29 12:52:10', '2019-12-29 12:52:10'),
(3, 2, 'srvc_glry1577645630.jpg', '2019-12-29 12:53:52', '2019-12-29 12:53:52'),
(4, 2, 'srvc_glry1577646449.jpg', '2019-12-29 13:07:30', '2019-12-29 13:07:30');

-- --------------------------------------------------------

--
-- Table structure for table `service_gallery_tab`
--

CREATE TABLE `service_gallery_tab` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `tab` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_gallery_tab`
--

INSERT INTO `service_gallery_tab` (`id`, `service_id`, `tab`, `created_at`, `updated_at`) VALUES
(1, 1, 'One', '2019-12-29 09:22:27', '2019-12-29 09:22:27'),
(2, 1, 'two', '2019-12-29 09:25:18', '2019-12-29 09:25:18');

-- --------------------------------------------------------

--
-- Table structure for table `service_list`
--

CREATE TABLE `service_list` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'sub category 1', '2019-12-28 07:53:46', '2019-12-28 07:53:46'),
(2, 1, 'sub category 2', '2019-12-28 07:53:46', '2019-12-28 07:53:46'),
(3, 2, 'sub category 3', '2019-12-28 07:53:46', '2019-12-28 07:53:46'),
(4, 2, 'sub category 4', '2019-12-28 07:53:46', '2019-12-28 07:53:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` text COLLATE utf8mb4_unicode_ci,
  `twitter` text COLLATE utf8mb4_unicode_ci,
  `gmail` text COLLATE utf8mb4_unicode_ci,
  `linkedin` text COLLATE utf8mb4_unicode_ci,
  `pinterest` text COLLATE utf8mb4_unicode_ci,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `nickname`, `email`, `phone`, `address`, `facebook`, `twitter`, `gmail`, `linkedin`, `pinterest`, `bio`, `email_verified_at`, `password`, `remember_token`, `profile_image`, `created_at`, `updated_at`) VALUES
(1, 'Ali', 'Noman', 'noman', 'n@gmail.com', '252352526', 'Dhaka, Bangladesh', 'https://www.facebook.com/alinoman.bd', 'https://www.facebook.com/alinoman.bd', 'https://www.facebook.com/alinoman.bd', 'https://www.facebook.com/alinoman.bd', 'https://www.facebook.com/alinoman.bd', 'asf asf asfasf saf asfas fas fasf saf', NULL, '$2y$10$9lEQqrfnKnq9L49CCdNXUuD6asSu5vhG2bSHLXfJNeaWKfDyWGPRu', NULL, 'profile_pic1577266510.jpg', '2019-11-19 09:59:26', '2019-12-25 03:35:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_cover`
--

CREATE TABLE `user_cover` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=not_active; 1=active;',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_cover`
--

INSERT INTO `user_cover` (`id`, `user_id`, `img`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'cover_img1574854338.jpg', 1, '2019-11-27 05:32:18', '2019-11-28 05:20:34'),
(2, 1, 'cover_img1574855167.jpg', 0, '2019-11-27 05:46:07', '2019-11-27 05:46:07'),
(3, 1, 'cover_img1574855934.jpg', 0, '2019-11-27 05:58:54', '2019-11-28 05:20:34'),
(4, 1, 'cover_img1574918485.jpg', 1, '2019-11-27 23:21:25', '2019-11-28 05:20:34'),
(6, 1, 'cover_img1574923045.jpg', 1, '2019-11-28 00:37:25', '2019-11-28 05:20:34'),
(7, 1, 'cover_img1574923061.jpg', 0, '2019-11-28 00:37:41', '2019-11-28 00:37:41'),
(8, 1, 'cover_img1574923085.jpg', 1, '2019-11-28 00:38:05', '2019-11-28 05:20:34'),
(9, 1, 'cover_img1574924319.jpg', 1, '2019-11-28 00:58:39', '2019-11-28 05:20:34'),
(10, 1, 'cover_img1574925631.jpg', 0, '2019-11-28 01:20:31', '2019-11-28 01:20:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alt_service_img_type`
--
ALTER TABLE `alt_service_img_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_galleries`
--
ALTER TABLE `service_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_gallery_tab`
--
ALTER TABLE `service_gallery_tab`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_list`
--
ALTER TABLE `service_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_cover`
--
ALTER TABLE `user_cover`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alt_service_img_type`
--
ALTER TABLE `alt_service_img_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `service_galleries`
--
ALTER TABLE `service_galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `service_gallery_tab`
--
ALTER TABLE `service_gallery_tab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service_list`
--
ALTER TABLE `service_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_cover`
--
ALTER TABLE `user_cover`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
