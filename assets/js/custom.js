$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
  },
});
var swiper = new Swiper(".ctm-container-4", {
  spaceBetween: 30,
  effect: "fade",
  autoplay: {
    delay: 3500,
    disableOnInteraction: false,
  },
});
$(function () {
  $(document).on("click", ".ctm-select-txt", function () {
    $(".ctm-select-txt").removeClass("active-ctm-s");
    $(this).addClass("active-ctm-s");
    $(".ctm-select-txt").each(function () {
      if ($(this).hasClass("active-ctm-s")) {
        if ($(this).hasClass("ctm-slct-hide")) {
          $(this).removeClass("ctm-slct-hide");
          $(this).parent().find(".ctm-option-box").hide();
        } else {
          $(this).addClass("ctm-slct-hide");
          $(this).parent().find(".ctm-option-box").show();
        }
      } else {
        $(this).removeClass("ctm-slct-hide");
        $(this).parent().find(".ctm-option-box").hide();
      }
    });
  });
  $(document).on("click", ".ctm-option", function () {
    var option_val = $(this).text();
    $(this).parents().find(".active-ctm-s .select-txt").text(option_val);
    $(".ctm-option-box").hide();
    $(".ctm-select .ctm-select-txt").removeClass("ctm-slct-hide");
    // console.log(option_val);
  });
  $(document).on(
    "change",
    '.ctm-option-ch input[type="checkbox"]',
    function () {
      if ($(this).prop("checked")) {
        $(this).parents(".ctm-option-ch").addClass("option-bg");
      } else {
        $(this).parents(".ctm-option-ch").removeClass("option-bg");
      }
    }
  );

  $(document).on("click", ".pay-acc", function () {
    $(".payment-option .pay-acc").removeClass("pay-selected");
    $(this).addClass("pay-selected");
    // console.log(option_val);
  });
  $("#coverImgC").on("hidden.bs.modal", function () {
    $("#coverImgC .image-checkbox").each(function () {
      if (!$(this).hasClass("prv-has")) {
        $(this).removeClass("image-checkbox-checked");
        $(this).find("input").prop("checked", false);
      }
    });
  });
  $(".image-checkbox").each(function () {
    if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
      $(this).addClass("image-checkbox-checked");
    } else {
      $(this).removeClass("image-checkbox-checked");
    }
  });
  $(document).on("click", ".image-checkbox", function (e) {
    $(this).toggleClass("image-checkbox-checked");
    var $checkbox = $(this).find('input[type="checkbox"]');
    $checkbox.prop("checked", !$checkbox.prop("checked"));

    e.preventDefault();
  });

  $(".dr-l").on("click", function () {
    if ($(this).hasClass("dr-in")) {
      $(this).parent().find(".dr-content").slideUp(400);
      $(this).removeClass("dr-in");
    } else {
      $(this).parents().find(".dr-content").slideUp(400);
      $(this).parents().find(".dr-l").removeClass("dr-in");
      $(this).parent().find(".dr-content").slideDown(400);
      $(this).addClass("dr-in");
    }
  });
  // $(".search-ic").on("click", function() {
  //   $(".search-box").fadeIn(400);
  // });
  // $(".s-close").on("click", function() {
  //   $(".search-box").fadeOut(400);
  // });

  $(".line-h span").on("click", function () {
    $(".header-filter .form-sh").slideToggle(400);
    if ($(this).find("i").hasClass("rotate-ar")) {
      $(this).find("i").removeClass("rotate-ar");
    } else {
      $(this).find("i").addClass("rotate-ar");
    }

    if ($(".plg_assign").val() == 0) {
      $("#single_range").wRunner({
        step: 1,
        type: "single",

        limits: {
          minLimit: 0,

          maxLimit: 100,
        },
        singleValue: $("#distance").val(),

        roots: document.body,

        divisionsCount: 0,

        valueNoteDisplay: true,

        theme: "default",

        direction: "horizontal",
      });
      var price = $("#price").val();
      var low = price.split(",")[0];
      var high = price.split(",")[1];
      $("#multi_range").wRunner({
        step: 5,
        type: "range",

        limits: {
          minLimit: 0,

          maxLimit: 100,
        },

        rangeValue: {
          minValue: low,
          maxValue: high,
        },

        roots: document.body,

        divisionsCount: 0,

        valueNoteDisplay: true,

        theme: "default",

        direction: "horizontal",
      });
    }
    $(".plg_assign").val(1);
  });

  $("#RName").keyup(function () {
    if ($(this).val().trim().length == 0) {
      $(this).css({ "border-color": "red" });
    } else {
      $(this).css({ "border-color": "#495057" });
    }
  });
  $("#REmail").keyup(function () {
    if ($(this).val().trim().length == 0) {
      $(this).css({ "border-color": "red" });
    } else if (!validateEmail($(this).val().trim())) {
      $(this).css({ "border-color": "red" });
    } else {
      $(this).css({ "border-color": "#495057" });
    }
  });
  $("#RPassword").keyup(function () {
    if ($(this).val().trim().length == 0) {
      $(this).css({ "border-color": "red" });
    } else {
      $(this).css({ "border-color": "#495057" });
    }
  });
  $("#RNickname").keyup(function () {
    if ($(this).val().trim().length == 0) {
      $(this).css({ "border-color": "red" });
    } else {
      $(this).css({ "border-color": "#495057" });
    }
  });
  $("#LEmail").keyup(function () {
    if ($(this).val().trim().length == 0) {
      $(this).css({ "border-color": "red" });
    } else if (!validateEmail($(this).val().trim())) {
      $(this).css({ "border-color": "red" });
    } else {
      $(this).css({ "border-color": "#495057" });
    }
  });
  $("#LPassword").keyup(function () {
    if ($(this).val().trim().length == 0) {
      $(this).css({ "border-color": "red" });
    } else {
      $(this).css({ "border-color": "#495057" });
    }
  });
  $("#serviceDescription").summernote({
    placeholder: "Service description",
    dialogsInBody: true,
    dialogsFade: false,
    minHeight: 100,
    airMode: false,
    toolbar: [
      ["style", ["style"]],
      [
        "font",
        [
          "bold",
          "italic",
          "underline",
          "superscript",
          "subscript",
          "strikethrough",
          "clear",
        ],
      ],
      ["fontname", ["fontname"]],
      ["fontsize", ["fontsize"]], // Still buggy
      ["color", ["color"]],
      ["para", ["ul", "ol", "paragraph"]],
      ["height", ["height"]],
      ["table", ["table"]],
      ["insert", ["link", "picture", "video", "hr"]],
    ],

    // fontSize
    fontSizes: [
      "8",
      "9",
      "10",
      "11",
      "12",
      "14",
      "18",
      "24",
      "36",
      "40",
      "48",
      "60",
      "72",
    ],
  });
  $(document).on("click", '[ctm-slt-n="cat_name"] .ctm-option', function (
    event
  ) {
    var cat_id = $(event.target).attr("ctm-otn-v");
    $('[ctm-slt-n="cat_name"] .ctm-option').removeAttr("ctm-select");
    $(event.target).attr("ctm-select", "true");
    // console.log(cat_id);
    $.ajax({
      url: window.change_service_sub_category,
      method: "POST",
      data: {
        cat_id: cat_id,
      },
      // contentType: false,
      // cache: false,
      // processData: false,
      success: function (res) {
        $('[ctm-slt-n="sub_cat_name"] .ctm-option-box').html(res);
      },
    });
  });
  $(document).on("click", '[ctm-slt-n="sub_cat_name"] .ctm-option', function (
    event
  ) {
    var sub_cat_id = $(event.target).attr("ctm-otn-v");
    $('[ctm-slt-n="sub_cat_name"] .ctm-option').removeAttr("ctm-select");
    $(event.target).attr("ctm-select", "true");
    // console.log(cat_id);
    $.ajax({
      url: window.change_service_inner_category,
      method: "POST",
      data: {
        sub_cat_id: sub_cat_id,
      },
      // contentType: false,
      // cache: false,
      // processData: false,
      success: function (res) {
        $('[ctm-slt-n="inner_cat_name"] .ctm-option-box').html(res);
      },
    });
  });
  // $(document).on("click", '[ctm-slt-n="sub_cat_name"] .ctm-option', function(
  //   event
  // ) {
  //   $('[ctm-slt-n="sub_cat_name"] .ctm-option').removeAttr("ctm-select");
  //   $(event.target).attr("ctm-select", "true");
  // });
  $(document).on(
    "click",
    '[ctm-slt-n="service_gallery_tab"] .ctm-option',
    function (event) {
      $('[ctm-slt-n="service_gallery_tab"] .ctm-option').removeAttr(
        "ctm-select"
      );
      $(event.target).attr("ctm-select", "true");
    }
  );
  //mobile menu

  $(".hs-menubar").hsMenu();

  //mobile menu

  $(".show-sub-m").hover(
    function (event) {
      $(".sub-menu", this).slideDown(200);
      var relX = event.pageX - (event.pageX - $(this).offset().left);
      var win_w = $(window).width();
      var main_m_w = $(".main-menu1").width();
      var lf_w = (win_w - main_m_w) / 2;
      var sub_w = $(this).find(".sub-menu").width();
      var right_w = win_w - relX;
      var n_left = right_w - lf_w;
      var left_sub;
      if (sub_w > main_m_w) {
        $(this).find(".sub-menu").width(main_m_w);
        // sub_w = main_m_w-40;
      }
      if (n_left < sub_w) {
        left_sub = n_left + 80 - sub_w;
        $(this)
          .find(".sub-menu")
          .css("left", left_sub + "px");
      } else {
        $(this).find(".sub-menu").css("left", "0");
      }
      // console.log(left_sub);
      // console.log(relX);
      // console.log(win_w);
      // console.log(right_w);
    },
    function () {
      $(".sub-menu", this).hide();
    }
  );

  var a = 0,
    sub_m_width,
    x,
    inn,
    y;
  $(".sub-menu").each(function (i, v) {
    sub_m_width = 0;
    x = 0;
    a++;
    inn = 0;
    $(this).addClass("sub-menu" + a);
    $(".sub-menu" + a + " .inner-menu").each(function (i, v) {
      x = $(".inner-menu").width();
      sub_m_width = sub_m_width + x;
      inn++;
    });
    var main_m_w = $(".main-menu1").width();
    // console.log(inn);
    if (inn >= 5 || main_m_w < sub_m_width) {
      y = $(".main-menu1").width() - 40;
      $(".sub-menu" + a).width(y);
    } else {
      $(".sub-menu" + a).width(sub_m_width);
    }
  });

  $(window).resize(function () {
    a = 0;
    $(".sub-menu").each(function (i, v) {
      sub_m_width = 0;
      x = 0;
      a++;
      inn = 0;
      $(".sub-menu" + a + " .inner-menu").each(function (i, v) {
        x = $(".inner-menu").width();
        sub_m_width = sub_m_width + x;
        inn++;
      });
      // console.log(a);
      var main_m_w = $(".main-menu1").width();
      if (inn >= 4 || main_m_w < sub_m_width) {
        y = $(".main-menu1").width() - 40;
        $(".sub-menu" + a).width(y);
      } else {
        $(".sub-menu" + a).width(sub_m_width);
      }
    });
  });

  $(".sub-menu").hover(
    function (event) {
      $(this).parent().find(".nav-link").css("color", "#E25B37");
    },
    function () {
      $(this).parent().find(".nav-link").css("color", "#fff");
    }
  );
  $(".multi-menu .nav-link").hover(
    function (event) {
      $(this).css("color", "#E25B37");
    },
    function () {
      $(this).css("color", "#fff");
    }
  );

  $(document).on("click", ".nav-close", function () {
    $(".hs-navigation").removeClass("open");
    $(".dim-overlay").css("display", "none");
  });
});
$(document).on("click", ".setting-btn", function () {
  $(".switch-acnt-box").toggle();
});
$(document).on("click", ".lan-flag", function () {
  $(".lan-pop-up").toggleClass("d-none");
});
function searchByTitle(e, val) {
  if (val.length) {
    $(e.target).parents(".ss").find(".ss-src-result").removeClass("d-none");
  } else {
    $(e.target).parents(".ss").find(".ss-src-result").addClass("d-none");
  }
}
$(document).on("click", function (e) {
  // if (
  //   !$(e.target).hasClass("search-ic") &&
  //   $(e.target).parents(".search-ic").length === 0 &&
  //   $(e.target).parents(".search-box").length === 0
  // ) {
  //   $(".search-box").fadeOut(400);
  // }
  if (
    !$(e.target).hasClass("dr-l") &&
    $(e.target).parents(".dr-l").length === 0 &&
    $(e.target).parents(".dr-content").length === 0
  ) {
    $(".dr-content").slideUp(400);
    $(".dr-l").removeClass("dr-in");
  }
  if (
    !$(e.target).hasClass("lan-flag") &&
    $(e.target).parents(".lan-flag").length === 0
  ) {
    $(".lan-pop-up").addClass("d-none");
  }
  if ($(e.target).parents(".ss").length === 0) {
    $(".ss-src-result").addClass("d-none");
  }

  if (
    !$(e.target).hasClass("setting-btn") &&
    $(e.target).parents(".setting-btn").length === 0 &&
    $(e.target).parents(".switch-acnt-box").length === 0
  ) {
    $(".switch-acnt-box").hide();
  }

  if (
    !$(e.target).hasClass("ctm-select-txt") &&
    $(e.target).parents(".ctm-select-txt").length === 0 &&
    $(e.target).parents(".ctm-option-box").length === 0
  ) {
    $(".ctm-option-box").hide();
    $(".ctm-select-txt").removeClass("ctm-slct-hide");
  }
});
$(document).on("hidden.bs.modal", function () {
  if ($(".modal.show").length) {
    $("body").addClass("modal-open");
  }
});
$(document).on("hide.bs.modal", "#modal", function () {
  $("#main_img").val("");
});

function validateEmail(sEmail) {
  var filter = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if (filter.test(sEmail)) {
    return true;
  } else {
    return false;
  }
}

function validateUserRes(e) {
  e.preventDefault();
  var name = $("#RName").val().trim(),
    email = $("#REmail").val().trim(),
    password = $("#RPassword").val().trim(),
    account_type = $(".account_type:checked").val(),
    surname = $("#RSurname").val().trim(),
    nickname = $("#RNickname").val().trim(),
    error = 0;

  if (name.length == 0) {
    $("#RName").css({ "border-color": "red" });
    error++;
  }
  if (nickname.length == 0) {
    $("#RNickname").css({ "border-color": "red" });
    error++;
  }
  if (email.length == 0) {
    $("#REmail").css({ "border-color": "red" });
    error++;
  } else if (!validateEmail(email)) {
    $("#REmail").css({ "border-color": "red" });
    error++;
  }
  if (password.length == 0) {
    $("#RPassword").css({ "border-color": "red" });
    error++;
  }
  if (error == 0) {
    $("#RName").css({ "border-color": "#495057" });
    $("#REmail").css({ "border-color": "#495057" });
    $("#RPassword").css({ "border-color": "#495057" });
    $.ajax({
      url: window.registration,
      method: "POST",
      data: {
        name: name,
        surname: surname,
        nickname: nickname,
        email: email,
        password: password,
        account_type: account_type,
      },
      success: function (res) {
        if (res.error) {
          $(".reg-error").removeClass("d-none").html(res.error);
        } else {
          Vue.prototype.$user = res.user;
          $.event.trigger({
            type: "loginComplete",
            user: res.user,
          });

          $(".reg-error").addClass("d-none");
          $("#registrationPopUp").modal("hide");
          if (window.service) {
            $(".usr-main-p").removeClass("d-none");
            $(".setting-btn").removeClass("d-none");
            $(".lg-btn").addClass("d-none");
          } else {
            window.location.href = window.profile;
          }
        }
      },
      error: function (err) {
        console.log(err);
      },
    });
  } else {
  }
}
function validateUserLogin(e) {
  e.preventDefault();
  (email = $("#LEmail").val().trim()),
    (password = $("#LPassword").val().trim()),
    (error = 0);

  if (email.length == 0) {
    $("#LEmail").css({ "border-color": "red" });
    error++;
  } else if (!validateEmail(email)) {
    $("#LEmail").css({ "border-color": "red" });
    error++;
  }
  if (password.length == 0) {
    $("#LPassword").css({ "border-color": "red" });
    error++;
  }
  if (error == 0) {
    $("#LEmail").css({ "border-color": "#495057" });
    $("#LPassword").css({ "border-color": "#495057" });
    $.ajax({
      url: window.login,
      method: "POST",
      data: {
        email: email,
        password: password,
      },
      success: function (res) {
        if (res.error) {
          $(".login-error").removeClass("d-none").html(res.error);
        } else {
          Vue.prototype.$user = res.user;
          // Vue.$emit("loginComplete", res.user);
          $.event.trigger({
            type: "loginComplete",
            user: res.user,
          });

          $(".login-error").addClass("d-none");
          $("#loginPopUp").modal("hide");
          if (window.service) {
            $(".usr-main-p").removeClass("d-none");
            $(".setting-btn").removeClass("d-none");
            $(".lg-btn").addClass("d-none");
          } else {
            window.location.href = window.profile;
          }
        }
      },
      error: function (err) {
        console.log(err);
      },
    });
  } else {
  }
}

// gallery
function uploadGallery() {
  var formData = new FormData();
  var totalfiles = $("#file-upload").prop("files").length;
  // console.log(totalfiles);
  for (var i = 0; i < totalfiles; i++) {
    formData.append("pic" + i, $("#file-upload").prop("files")[i]);
  }
  formData.append("total", totalfiles);
  $.ajax({
    url: window.gallery_up,
    method: "POST",
    data: formData,
    contentType: false,
    cache: false,
    processData: false,
    success: function (response) {
      $("#productGalleryV").prepend(response);
    },
  });
}

function removeImg(e, id) {
  if (confirm("Are you sure?")) {
    var _this = $(e.target);
    $.ajax({
      url: window.gallery_remove,
      method: "POST",
      data: {
        id: id,
      },
      success: function (response) {
        // console.log(response);
        _this.parent().remove();
      },
    });
  } else {
    e.preventDefault();
  }
}

function showPicEditBox() {
  $("#proImgC").modal("show");
}
function hidePicEditBox() {
  $("#proImgC").modal("hide");
  $("#coverImgC").modal("hide");
  $("#proInfoC").modal("hide");
  $("#serviceTD").modal("hide");
  $("#serviceImg").modal("hide");
  $("#serviceGalleryImg").modal("hide");
  $("#serviceGlryTab").modal("hide");
}
function changeCoverImage(e) {
  $("#coverImgC").modal("show");
}
function changeServiceImage(e) {
  $("#serviceImg").modal("show");
}
function changeServiceGalleryImage(e) {
  var actv_tab = $(".gallery .active-tag").html().trim();
  $('[ctm-slt-n="service_gallery_tab"] .ctm-option').each(function () {
    if ($(this).html().trim() == actv_tab) {
      $(this).attr("ctm-select", "true");
      $('[ctm-slt-n="service_gallery_tab"] #service_gallery_tab_a').text(
        actv_tab
      );
    } else {
      $(this).removeAttr("ctm-select");
      if (actv_tab == "all") {
        $('[ctm-slt-n="service_gallery_tab"] #service_gallery_tab_a').text(
          "chose..."
        );
      }
    }
  });
  $("#serviceGalleryImg").modal("show");
}
function addServiceGalleryTab(e) {
  $("#serviceGlryTab").modal("show");
}
function proInfoC(e) {
  $.ajax({
    url: window.get_profile_info,
    method: "POST",
    data: {},
    success: function (res) {
      $("#name").val(res.name);
      $("#surname").val(res.surname);
      $("#nickname").val(res.nickname);
      $("#email").val(res.email);
      $("#phone").val(res.phone);
      $("#autoAddress").val(res.address);
      $("#facebook").val(res.facebook);
      $("#twitter").val(res.twitter);
      $("#gmail").val(res.gmail);
      $("#linkedin").val(res.linkedin);
      $("#pinterest").val(res.pinterest);
      $("#bio").val(res.bio);
      $("#lat").val(res.user_lat);
      $("#long").val(res.user_lng);
      $("#city").val(res.user_city);
      if (res.account_type == 1) {
        $(".sls").attr("checked", true);
      } else {
        $(".cmp").attr("checked", true);
      }
      if (jQuery.inArray("1", res.show_name) !== -1) {
        $(".sh1").attr("checked", true);
      }
      if (jQuery.inArray("2", res.show_name) !== -1) {
        $(".sh2").attr("checked", true);
      }
      if (jQuery.inArray("3", res.show_name) !== -1) {
        $(".sh3").attr("checked", true);
      }
    },
  });
  $("#proInfoC").modal("show");
}
function serviceTD(e) {
  $("#serviceTD").modal("show");
}

window.addEventListener("DOMContentLoaded", function () {
  var image = document.getElementById("image");
  var input = document.getElementById("main_img");
  var $modal = $("#modal");
  var croppers;
  $('[data-toggle="tooltip"]').tooltip();
  input.addEventListener("change", function (e) {
    var files = e.target.files;
    var done = function (url) {
      image.src = url;
      $modal.modal({
        backdrop: "static",
        keyboard: false,
      });
    };
    var reader;
    var file;
    var url;
    if (files && files.length > 0) {
      file = files[0];
      if (URL) {
        done(URL.createObjectURL(file));
      } else if (FileReader) {
        reader = new FileReader();
        reader.onload = function (e) {
          done(reader.result);
        };
        reader.readAsDataURL(file);
      }
    }
  });
  $modal
    .on("shown.bs.modal", function () {
      cropper = new Cropper(image, {
        aspectRatio: 1 / 1,
        viewMode: 3,
        autoCropArea: 1,
      });
    })
    .on("hidden.bs.modal", function () {
      cropper.destroy();
      cropper = null;
    });
  document.getElementById("crop").addEventListener("click", function () {
    var initialAvatarURL;
    var canvas;
    $modal.modal("hide");
    if (cropper) {
      canvas = cropper.getCroppedCanvas({
        width: 500,
        height: 500,
      });
      canvas.toBlob(function (blob) {
        var formData = new FormData();
        formData.append("avatar", blob);
        // $('.loading-dd').show();
        $.ajax(window.profile_single_tmp_img, {
          method: "POST",
          data: formData,
          processData: false,
          contentType: false,
          xhr: function () {
            var xhr = new XMLHttpRequest();
            xhr.upload.onprogress = function (e) {
              var percent = "0";
              if (e.lengthComputable) {
                percent = Math.round((e.loaded / e.total) * 100);
              }
            };

            return xhr;
          },
          success: function (res) {
            // console.log(res);
            $("#files").html(res);
          },
        });
      });
    }
  });
});
window.addEventListener("DOMContentLoaded", function () {
  var image = document.getElementById("server_image");
  var input = document.getElementById("server_img_u");
  var $modal = $("#serverUploadImgM");
  var croppers;
  $('[data-toggle="tooltip"]').tooltip();
  input.addEventListener("change", function (e) {
    var files = e.target.files;
    var done = function (url) {
      image.src = url;
      $modal.modal({
        backdrop: "static",
        keyboard: false,
      });
    };
    var reader;
    var file;
    var url;
    if (files && files.length > 0) {
      file = files[0];
      if (URL) {
        done(URL.createObjectURL(file));
      } else if (FileReader) {
        reader = new FileReader();
        reader.onload = function (e) {
          done(reader.result);
        };
        reader.readAsDataURL(file);
      }
    }
  });
  $modal
    .on("shown.bs.modal", function () {
      cropper = new Cropper(image, {
        aspectRatio: 1 / 1,
        viewMode: 3,
        autoCropArea: 1,
      });
    })
    .on("hidden.bs.modal", function () {
      cropper.destroy();
      cropper = null;
    });
  document
    .getElementById("crop_service_img")
    .addEventListener("click", function () {
      var initialAvatarURL;
      var canvas;
      $modal.modal("hide");
      if (cropper) {
        canvas = cropper.getCroppedCanvas({
          width: 500,
          height: 500,
        });
        canvas.toBlob(function (blob) {
          var formData = new FormData();
          formData.append("avatar", blob);
          // $('.loading-dd').show();
          $.ajax(window.service_tmp_img, {
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            xhr: function () {
              var xhr = new XMLHttpRequest();
              xhr.upload.onprogress = function (e) {
                var percent = "0";
                if (e.lengthComputable) {
                  percent = Math.round((e.loaded / e.total) * 100);
                }
              };

              return xhr;
            },
            success: function (res) {
              // console.log(res);
              $("#service_files").html(res);
            },
          });
        });
      }
    });
});
$(document).on("click", ".remove", function () {
  var img_name = this.id;
  $(this).closest("span.pip").remove();
  $.ajax({
    method: "POST",
    url: window.profile_single_tmp_remove,
    data: {
      img_name: img_name,
    },
    success: function (result) {},
  });
});
$(document).on("click", ".service-remove", function () {
  var img_name = this.id;
  $(this).closest("span.pip").remove();
  $.ajax({
    method: "POST",
    url: window.service_single_tmp_remove,
    data: {
      img_name: img_name,
    },
    success: function (result) {},
  });
});
$(document).on("click", ".service-gallery-remove", function () {
  var img_name = this.id;
  $(this).closest("span.pip").remove();
  $.ajax({
    method: "POST",
    url: window.service_gallery_single_tmp_remove,
    data: {
      img_name: img_name,
    },
    success: function (result) {},
  });
});
window.addEventListener("DOMContentLoaded", function () {
  var image = document.getElementById("c_image");
  var input = document.getElementById("cover_img");
  var $modal = $("#modal_cover");
  var croppers;
  $('[data-toggle="tooltip"]').tooltip();
  input.addEventListener("change", function (e) {
    var files = e.target.files;
    var done = function (url) {
      image.src = url;
      $modal.modal({
        backdrop: "static",
        keyboard: false,
      });
    };
    var reader;
    var file;
    var url;
    if (files && files.length > 0) {
      file = files[0];
      if (URL) {
        done(URL.createObjectURL(file));
      } else if (FileReader) {
        reader = new FileReader();
        reader.onload = function (e) {
          done(reader.result);
        };
        reader.readAsDataURL(file);
      }
    }
  });
  $modal
    .on("shown.bs.modal", function () {
      cropper = new Cropper(image, {
        aspectRatio: 8 / 5,
        viewMode: 3,
        autoCropArea: 1,
      });
    })
    .on("hidden.bs.modal", function () {
      cropper.destroy();
      cropper = null;

      $("#cover_img").val("");
    });
  document.getElementById("c_crop").addEventListener("click", function () {
    var initialAvatarURL;
    var canvas;
    $modal.modal("hide");
    if (cropper) {
      canvas = cropper.getCroppedCanvas({
        width: 800,
        height: 500,
      });
      canvas.toBlob(function (blob) {
        var formData = new FormData();
        formData.append("avatar", blob);
        // $('.loading-dd').show();
        $.ajax(window.cover_img_upload, {
          method: "POST",
          data: formData,
          processData: false,
          contentType: false,
          xhr: function () {
            var xhr = new XMLHttpRequest();
            xhr.upload.onprogress = function (e) {
              var percent = "0";
              if (e.lengthComputable) {
                percent = Math.round((e.loaded / e.total) * 100);
              }
            };

            return xhr;
          },
          success: function (res) {
            // console.log(res);
            $("#cover_all_area").prepend(res);
          },
          error: function (res) {
            console.log(res);
          },
        });
      });
    }
  });
});
window.addEventListener("DOMContentLoaded", function () {
  var image = document.getElementById("service_gallery_image");
  var input = document.getElementById("gallery_img_u");
  var $modal = $("#serviceGalleryImgU");
  var croppers;
  $('[data-toggle="tooltip"]').tooltip();
  input.addEventListener("change", function (e) {
    var files = e.target.files;
    var done = function (url) {
      image.src = url;
      $modal.modal({
        backdrop: "static",
        keyboard: false,
      });
    };
    var reader;
    var file;
    var url;
    if (files && files.length > 0) {
      file = files[0];
      if (URL) {
        done(URL.createObjectURL(file));
      } else if (FileReader) {
        reader = new FileReader();
        reader.onload = function (e) {
          done(reader.result);
        };
        reader.readAsDataURL(file);
      }
    }
  });
  $modal
    .on("shown.bs.modal", function () {
      cropper = new Cropper(image, {
        aspectRatio: 1 / 1,
        viewMode: 3,
        autoCropArea: 1,
      });
    })
    .on("hidden.bs.modal", function () {
      cropper.destroy();
      cropper = null;

      $("#cover_img").val("");
    });
  document
    .getElementById("crop_service_gallery_img")
    .addEventListener("click", function () {
      var initialAvatarURL;
      var canvas;
      $modal.modal("hide");
      if (cropper) {
        canvas = cropper.getCroppedCanvas({
          width: 500,
          height: 500,
        });
        canvas.toBlob(function (blob) {
          var formData = new FormData();
          formData.append("avatar", blob);
          // $('.loading-dd').show();
          $.ajax(window.service_gallery_img_upload, {
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            xhr: function () {
              var xhr = new XMLHttpRequest();
              xhr.upload.onprogress = function (e) {
                var percent = "0";
                if (e.lengthComputable) {
                  percent = Math.round((e.loaded / e.total) * 100);
                }
              };

              return xhr;
            },
            success: function (res) {
              // console.log(res);
              $("#service_gallery").prepend(res);
            },
            error: function (res) {
              console.log(res);
            },
          });
        });
      }
    });
});
function deleteCoverPic(e, id) {
  if (confirm("Are you sure?")) {
    var _this = $(e.target);
    $.ajax({
      url: window.cover_pic_remove,
      method: "POST",
      data: {
        id: id,
      },
      success: function (response) {
        _this.parent().remove();
      },
    });
  } else {
    e.preventDefault();
  }
}
$("#proInfoC #name").keyup(function () {
  if ($(this).val().trim().length == 0) {
    $(this).css({ "border-color": "red" });
  } else {
    $(this).css({ "border-color": "#495057" });
  }
});
$("#proInfoC #surname").keyup(function () {
  if ($(this).val().trim().length == 0) {
    $(this).css({ "border-color": "red" });
  } else {
    $(this).css({ "border-color": "#495057" });
  }
});
$("#proInfoC #nickname").keyup(function () {
  if ($(this).val().trim().length == 0) {
    $(this).css({ "border-color": "red" });
  } else {
    $(this).css({ "border-color": "#495057" });
  }
});

$(document).on("submit", "#profile_info_form", function (event) {
  event.preventDefault();
  var error = 0;
  if ($("#proInfoC #name").val().trim().length == 0) {
    error++;
    $("#proInfoC #name").css({ "border-color": "red" });
  } else {
    $("#proInfoC #name").css({ "border-color": "#495057" });
  }
  if ($("#proInfoC #surname").val().trim().length == 0) {
    error++;
    $("#proInfoC #surname").css({ "border-color": "red" });
  } else {
    $("#proInfoC #surname").css({ "border-color": "#495057" });
  }
  if ($("#proInfoC #nickname").val().trim().length == 0) {
    error++;
    $("#proInfoC #nickname").css({ "border-color": "red" });
  } else {
    $("#proInfoC #nickname").css({ "border-color": "#495057" });
  }

  if (error == 0) {
    var form = $(this)[0];
    var formdata = new FormData(form);
    $.ajax({
      url: window.insert_profile_info,
      method: "POST",
      data: formdata,
      // dataType:'JSON',
      contentType: false,
      cache: false,
      processData: false,
      success: function (res) {
        $("#proInfoC").modal("hide");
        // console.log(res);
        $(".userPro-section .userPro-tlt h3").html(
          res.name +
            " " +
            res.surname +
            (res.nickname ? " (" + res.nickname + ")" : "")
        );
        $(".userPro-section .pro-email").html(
          '<i class="fa fa-envelope"></i>' + (res.email ? res.email : "")
        );
        $(".userPro-section .pro-phn").html(
          '<i class="fas fa-mobile-alt"></i>' + (res.phone ? res.phone : "")
        );
        $(".userPro-section .pro-cn").html(
          '<i class="fas fa-map-marker-alt"></i>' +
            (res.address ? res.address : "")
        );
        if (res.facebook) {
          $(".userPro-section .social .fb").attr(
            "href",
            res.facebook ? res.facebook : "javascript:void(0)"
          );
          $(".userPro-section .social .fb").parent().removeClass("d-none");
        } else {
          $(".userPro-section .social .fb").parent().addClass("d-none");
        }
        if (res.twitter) {
          $(".userPro-section .social .tw").attr(
            "href",
            res.twitter ? res.twitter : "javascript:void(0)"
          );
          $(".userPro-section .social .tw").parent().removeClass("d-none");
        } else {
          $(".userPro-section .social .tw").parent().addClass("d-none");
        }
        if (res.gmail) {
          $(".userPro-section .social .go").attr(
            "href",
            res.gmail ? res.gmail : "javascript:void(0)"
          );
          $(".userPro-section .social .go").parent().removeClass("d-none");
        } else {
          $(".userPro-section .social .go").parent().addClass("d-none");
        }
        if (res.linkedin) {
          $(".userPro-section .social .lin").attr(
            "href",
            res.linkedin ? res.linkedin : "javascript:void(0)"
          );
          $(".userPro-section .social .lin").parent().removeClass("d-none");
        } else {
          $(".userPro-section .social .lin").parent().addClass("d-none");
        }
        if (res.pinterest) {
          $(".userPro-section .social .pin").attr(
            "href",
            res.pinterest ? res.pinterest : "javascript:void(0)"
          );
          $(".userPro-section .social .pin").parent().removeClass("d-none");
        } else {
          $(".userPro-section .social .pin").parent().addClass("d-none");
        }

        var makeName = "";
        if (jQuery.inArray("1", res.show_name) !== -1) {
          makeName += res.name + " ";
        }
        if (jQuery.inArray("2", res.show_name) !== -1) {
          makeName += res.surname + " ";
        }
        if (jQuery.inArray("3", res.show_name) !== -1) {
          if (makeName == "") {
            makeName = res.nickname;
          } else {
            makeName += "(" + res.nickname + ")";
          }
        }
        makeName = makeName.trim();

        $(".setName").text(makeName);

        $(".userPro-section .bio-txtP").html(res.bio ? res.bio : "");
        var x = document.getElementById("snackbar");
        x.style.background = "#7d0000";
        x.innerHTML = "Profile Information Successfully Inserted.";
        x.className = "show";
        setTimeout(function () {
          x.className = x.className.replace("show", "");
        }, 7000);
      },
    });
  } else {
    var x = document.getElementById("snackbar");
    x.style.background = "#7d0000";
    x.innerHTML = "Please Insert Required Information.";
    x.className = "show";
    setTimeout(function () {
      x.className = x.className.replace("show", "");
    }, 7000);
  }
});
$(document).on("submit", "#upload_service_gallery_info", function (event) {
  event.preventDefault();
  var srvc_glry_tab_id = $(
    '[ctm-slt-n="service_gallery_tab"] [ctm-select]'
  ).attr("ctm-otn-v");
  if (srvc_glry_tab_id) {
    $.ajax({
      url: window.add_service_gallery_image,
      method: "POST",
      data: {
        id: srvc_glry_tab_id,
      },
      success: function (res) {
        $("#serviceGalleryImg").modal("hide");

        $(".gallery-items-row").append(
          '<div class="item-column  col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2"><div class="single-content gallery-item" data-gallery-tag="' +
            res.tab +
            '">' +
            '<a href="https://images.pexels.com/photos/716411/pexels-photo-716411.jpeg?auto=compress&amp;cs=tinysrgb&amp;h=350" data-fancybox="gallery">' +
            '<div class="content-img-p">' +
            '<div class="content-img">' +
            '<img src="' +
            window.base_url +
            "/" +
            res.path +
            '" alt="img">' +
            "</div>" +
            "</div>" +
            "</a>" +
            "</div></div>"
        );

        var x = document.getElementById("snackbar");
        x.style.background = "#7d0000";
        x.innerHTML = "Service Gallery Image Successfully Added.";
        x.className = "show";
        setTimeout(function () {
          x.className = x.className.replace("show", "");
        }, 7000);
      },
    });
  } else {
    var x = document.getElementById("snackbar");
    x.style.background = "#7d0000";
    x.innerHTML = "Please Select Required Information.";
    x.className = "show";
    setTimeout(function () {
      x.className = x.className.replace("show", "");
    }, 7000);
  }
});

$(document).on("submit", "#change_service_details", function (event) {
  event.preventDefault();
  var form = $(this)[0];
  var formdata = new FormData(form);
  $.ajax({
    url: window.change_service_details,
    method: "POST",
    data: formdata,
    contentType: false,
    cache: false,
    processData: false,
    success: function (res) {
      $(".service-title").text(res.title);
      $(".service-description").html(res.description);
      $("#serviceTD").modal("hide");
      var x = document.getElementById("snackbar");
      x.style.background = "#7d0000";
      x.innerHTML = "Service Details Successfully Inserted.";
      x.className = "show";
      setTimeout(function () {
        x.className = x.className.replace("show", "");
      }, 7000);
    },
  });
});
function Confirm(title, msg, $true, $false, $link) {
  var $content =
    "<div class='dialog-ovelay'>" +
    "<div class='dialog'><header>" +
    " <h3> " +
    title +
    " </h3> " +
    "<i class='fa-close'>✖</i>" +
    "</header>" +
    "<div class='dialog-msg'>" +
    " <p> " +
    msg +
    " </p> " +
    "</div>" +
    "<footer class='edit-box-area'>" +
    "<div class='controls edit-box'>" +
    " <button class='btn btn-success doAction'>" +
    $true +
    "</button> " +
    " <button class='p-btn btn cancelAction'>" +
    $false +
    "</button> " +
    "</div>" +
    "</footer>" +
    "</div>" +
    "</div>";
  // console.log($link);
  $("body").prepend($content).addClass("ovrf-hdn");
  $(".doAction").click(function () {
    window.open($link, "_blank");
    $(this)
      .parents(".dialog-ovelay")
      .fadeOut(500, function () {
        $("body").removeClass("ovrf-hdn");
        $(this).remove();
      });
  });
  $(".cancelAction, .fa-close").click(function () {
    $(this)
      .parents(".dialog-ovelay")
      .fadeOut(500, function () {
        $("body").removeClass("ovrf-hdn");
        $(this).remove();
      });
  });
}
function editServiceCategory(e) {
  $('[ctm-slt-n="cat_name"] .ctm-select-txt').removeAttr("onclick");
  $('[ctm-slt-n="sub_cat_name"] .ctm-select-txt').removeAttr("onclick");
  $('[ctm-slt-n="inner_cat_name"] .ctm-select-txt').removeAttr("onclick");
  $(".save-cat-btn i").removeClass("fa-edit").addClass("fa-save");
  $(".save-cat-btn .edit-info-btn")
    .addClass("ctm-btn-save")
    .attr("onclick", "saveServiceCategory(event)");
}
function saveServiceCategory(e) {
  var cat_id = $('[ctm-slt-n="cat_name"] [ctm-select]').attr("ctm-otn-v");
  var sub_cat_id = $('[ctm-slt-n="sub_cat_name"] [ctm-select]').attr(
    "ctm-otn-v"
  );
  var inner_cat_id = [];
  $(".srvc-inner-cat:checked").each(function () {
    inner_cat_id.push(this.value);
  });
  var id = $('[name="service_id_h"]').val();
  if (cat_id && sub_cat_id && inner_cat_id.length != 0) {
    $.ajax({
      url: window.save_service_category,
      method: "POST",
      data: {
        cat_id: cat_id,
        sub_cat_id: sub_cat_id,
        inner_cat_id: inner_cat_id,
        id: id,
      },
      success: function (res) {
        // $('[ctm-slt-n="sub_cat_name"] .ctm-option-box').html(res);
        // $('[ctm-slt-n="sub_cat_name"] .ctm-select-txt').removeAttr("onclick");
        $('[ctm-slt-n="cat_name"] .ctm-select-txt').attr(
          "onclick",
          "event.stopPropagation()"
        );
        $('[ctm-slt-n="sub_cat_name"] .ctm-select-txt').attr(
          "onclick",
          "event.stopPropagation()"
        );
        $('[ctm-slt-n="inner_cat_name"] .ctm-select-txt').attr(
          "onclick",
          "event.stopPropagation()"
        );
        $(".save-cat-btn i").addClass("fa-edit").removeClass("fa-save");
        $(".save-cat-btn .edit-info-btn")
          .removeClass("ctm-btn-save")
          .attr("onclick", "editServiceCategory(event)");
        var x = document.getElementById("snackbar");
        x.style.background = "#7d0000";
        x.innerHTML = "Service Category Successfully Change.";
        x.className = "show";
        setTimeout(function () {
          x.className = x.className.replace("show", "");
        }, 7000);
      },
    });
  } else {
    var x = document.getElementById("snackbar");
    x.style.background = "#7d0000";
    x.innerHTML = "Please Choose Category and Sub Category.";
    x.className = "show";
    setTimeout(function () {
      x.className = x.className.replace("show", "");
    }, 7000);
  }
}
$(document).on("submit", "#change_service_glry_tab", function (event) {
  event.preventDefault();
  var form = $(this)[0];
  var formdata = new FormData(form);
  var empty = $("#change_service_glry_tab")
    .find("#glryTabName")
    .filter(function () {
      return this.value === "";
    });
  if (empty.length) {
    var x = document.getElementById("snackbar");
    x.style.background = "#7d0000";
    x.innerHTML = "Please Give Gallery Tab Name.";
    x.className = "show";
    setTimeout(function () {
      x.className = x.className.replace("show", "");
    }, 7000);
  } else {
    $.ajax({
      url: window.change_service_glry_tab,
      method: "POST",
      data: formdata,
      contentType: false,
      cache: false,
      processData: false,
      success: function (res) {
        $(".gallery ul .add-tab-li").before(
          '<li class="nav-item active">' +
            '<a class="nav-link" href="javascript:void(0)" data-images-toggle="' +
            res +
            '">' +
            res +
            "</a>" +
            "</li>"
        );
        $("#serviceGlryTab").modal("hide");
        var x = document.getElementById("snackbar");
        x.style.background = "#7d0000";
        x.innerHTML = "Add Gallery Tab Successfully.";
        x.className = "show";
        setTimeout(function () {
          x.className = x.className.replace("show", "");
        }, 7000);
      },
    });
  }
});
